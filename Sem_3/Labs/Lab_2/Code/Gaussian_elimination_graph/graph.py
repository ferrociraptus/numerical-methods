from matplotlib import pyplot as plt
import numpy as np
from decimal import *

# result_1 = [{}]
# result_2 = [{}]
# result_3 = [{}]

# with open("matrixs/calculation_result_1.py", "r") as file:
#     exec("\n".join(file.readlines()))
#

from matrixs.calculation_result_1 import *


def draw_first_plot():
    fig, ax = plt.subplots()

    err = [(np.linalg.norm(res["X"] - res["X_calculated"]) / np.linalg.norm(res["X_calculated"])) for res in result_1]

    ax.plot([10 ** i for i in range(1, len(result_1) + 1)], err, label="Gaussian_elimination_method", linewidth=2)

    ax.grid(True)
    ax.set_xscale('log')
    ax.set_yscale('log')

    plt.ylabel("error")
    plt.xlabel("condition number")
    plt.title("Error to condition value")

    ax.legend(loc="upper right")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig('Error_to_cond_val.png', dpi=100)


def draw_second_plot():
    fig, ax = plt.subplots()
    err_1 = [(np.linalg.norm(res["X"] - res["X_calculated"]) / np.linalg.norm(res["X_calculated"])) for res in result_2]
    err_2 = [(np.linalg.norm(res["X"] - res["X_calculated"]) / np.linalg.norm(res["X_calculated"])) for res in result_3]


    ax.plot([np.linalg.norm(res["err"]) / np.linalg.norm(res["B"]) for res in result_2], err_1,
            label="Gaussian_elimination_method_cond_1", linewidth=2)
    ax.plot([np.linalg.norm(res["err"]) / np.linalg.norm(res["B"]) for res in result_2],
            [err/np.linalg.norm(res["A"])/Decimal(np.linalg.norm(res["A"].astype(np.float64)))
             for err, res in zip(err_1, result_2)],
            label=r"cond_1: $\frac{||\delta x||}{||x||*||A||*||A^{-1}||}$",
            linestyle="--")

    ax.plot([np.linalg.norm(res["err"]) / np.linalg.norm(res["B"]) for res in result_3], err_2,
            label="Gaussian_elimination_method_cond_10", linewidth=2)
    ax.plot([np.linalg.norm(res["err"]) / np.linalg.norm(res["B"]) for res in result_2],
            [err / np.linalg.norm(res["A"]) / Decimal(np.linalg.norm(np.linalg.inv(res["A"].astype(np.float64))))
             for err, res in zip(err_1, result_2)],
            label=r"cond_10: $\frac{||\delta x||}{||x||*||A||*||A^{-1}||}$",
            linestyle="--")


    ax.grid(True)
    ax.set_xscale('log')
    ax.set_yscale('log')

    plt.xlabel(r"$\frac{\delta \beta}{\beta}$")
    plt.ylabel("error")
    plt.title("Error to user error")

    ax.legend(loc="upper right")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig('Error_to_user_err_val.png', dpi=100)


draw_first_plot()
draw_second_plot()
plt.show()
