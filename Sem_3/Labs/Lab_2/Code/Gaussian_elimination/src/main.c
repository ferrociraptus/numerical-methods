#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <ctype.h>

#define MATRIX_SIZE 10
#define TESTING_ITERATIONS 19
#define SIGMA_ITERATIONS 17

typedef struct{
	double a_matrix[MATRIX_SIZE][MATRIX_SIZE];
	double x_vector[MATRIX_SIZE];
	double x_calculated[MATRIX_SIZE];
	double b_vector[MATRIX_SIZE];
	double err_vector[MATRIX_SIZE];
	double cond_val;
}DataSerializationStruct;

void fill_matrix(double matrix[MATRIX_SIZE][MATRIX_SIZE], int range_min, int range_max){
	for (int i = 0; i < MATRIX_SIZE; i ++)
		for (int k = 0; k < MATRIX_SIZE; k++)
			matrix[i][k] = rand() % (range_max + range_min) - range_min;
}

void fill_vector(double* vector, int range_min, int range_max){
	for (int i = 0; i < MATRIX_SIZE; i ++)
		vector[i] = rand() % (range_max + range_min) - range_min;
}

double* dot_matrix_with_vector(double matrix[MATRIX_SIZE][MATRIX_SIZE], double vector[MATRIX_SIZE]){
	double* result = (double*) malloc(sizeof(double) * MATRIX_SIZE);
	memset(result, 0, sizeof(double)*MATRIX_SIZE);
	
	for (int row = 0; row < MATRIX_SIZE; row++)
		for (int col = 0; col < MATRIX_SIZE; col++)
			result[row] += vector[col] * matrix[row][col];
		
	return result;
}

double* dot_matrix_with_vector_save(double* result, double matrix[MATRIX_SIZE][MATRIX_SIZE], double vector[MATRIX_SIZE]){
	memset(result, 0, sizeof(double) * MATRIX_SIZE);
	for (int row = 0; row < MATRIX_SIZE; row++)
		for (int col = 0; col < MATRIX_SIZE; col++)
			result[row] += vector[col] * matrix[row][col];
		
	return result;
}

void init_printing_to_python_code(FILE* stream){
	fprintf(stream, "from decimal import *\n");
	fprintf(stream, "import numpy as np\n");
}

void print_matrix_to_python_code(FILE* stream, const char* field_name, double matrix[MATRIX_SIZE][MATRIX_SIZE]){
	fprintf(stream, "%s = np.array([", field_name);
	for (unsigned row = 0; row < MATRIX_SIZE; row++){
		fprintf(stream, "[");
		for (unsigned col = 0; col < MATRIX_SIZE-1; col++)
			fprintf(stream, "Decimal(\"%.40lf\"),", matrix[row][col]);
		fprintf(stream, "Decimal(\"%.50lf\")", matrix[row][MATRIX_SIZE - 1]);
		fprintf(stream, "]");
		
		if (row < MATRIX_SIZE-1)
			fprintf(stream, ",");
	}
	fprintf(stream, "], dtype=np.dtype(Decimal))\n");
}

void print_vector_to_python_code(FILE* stream, const char* field_name, double vector[MATRIX_SIZE]){
	fprintf(stream, "%s = np.array([", field_name);
	for (unsigned col = 0; col < MATRIX_SIZE-1; col++){
		fprintf(stream, "Decimal(\"%.50lf\")", vector[col]);
		if (col < MATRIX_SIZE-1)
			fprintf(stream, ",");
	}
	fprintf(stream, "], dtype=np.dtype(Decimal))\n");
}

void print_data_to_python_code(FILE* stream, const char* field_name, DataSerializationStruct* str){
	char buf[100] = {0};
	fprintf(stream, "%s = {}\n", field_name);
	
	sprintf(buf, "%s[\"A\"]", field_name);
	print_matrix_to_python_code(stream, buf, str->a_matrix );
	
	sprintf(buf, "%s[\"B\"]", field_name);
	print_vector_to_python_code(stream, buf, str->b_vector);
	
	sprintf(buf, "%s[\"X\"]", field_name);
	print_vector_to_python_code(stream, buf, str->x_vector);
	
	sprintf(buf, "%s[\"X_calculated\"]", field_name);
	print_vector_to_python_code(stream, buf, str->x_calculated);
	
	sprintf(buf, "%s[\"err\"]", field_name);
	print_vector_to_python_code(stream, buf, str->err_vector);
	
	fprintf(stream, "%s[\"condition_number\"] = %.40lf\n", field_name, str->cond_val);
}

void add_value_to_vector(double vector[MATRIX_SIZE], double val){
	for (unsigned i = 0; i < MATRIX_SIZE; i++)
		vector[i] += val;
}

void matrix_from_file(FILE* file, double matrix[MATRIX_SIZE][MATRIX_SIZE]){
	char ch_buf = getc(file);
	while(!isdigit(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
	ungetc(ch_buf, file);
	
	for (unsigned row = 0; row < MATRIX_SIZE; row++){
		for (unsigned col = 0; col < MATRIX_SIZE; col++){
			double val;
			fscanf(file, "%lf", &val);
			matrix[row][col] = val;
			
			ch_buf = getc(file);
			while(isspace(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
// 			while((isdigit(ch_buf) || ch_buf == '.' || ch_buf == '-') && ch_buf != EOF) ch_buf = getc(file);
			while((!isdigit(ch_buf) && ch_buf != '-') && ch_buf != EOF) ch_buf = getc(file);
			ungetc(ch_buf, file);
		}
	}
}

void copy_matrix(double matrix_buf[MATRIX_SIZE][MATRIX_SIZE], double matrix[MATRIX_SIZE][MATRIX_SIZE]){
	for (int i = 0; i < MATRIX_SIZE; i++)
		memcpy(matrix_buf[i], matrix[i], sizeof(double)*MATRIX_SIZE);
}

void add_vector_to_vector(double vector_1[MATRIX_SIZE], double vector_2[MATRIX_SIZE]
){
	for (unsigned i = 0; i < MATRIX_SIZE; i++)
		vector_1[i] += vector_2[i];
}

double* gaussian_elimination_method(double matrix[MATRIX_SIZE][MATRIX_SIZE],
									double matrix_extending[MATRIX_SIZE]){
	unsigned matrix_size = MATRIX_SIZE;
	
	double matrix_copy[MATRIX_SIZE][MATRIX_SIZE];
	double matrix_extending_copy[MATRIX_SIZE];
	double* solve = (double*)malloc(sizeof(double)*MATRIX_SIZE);
	
	for (unsigned i = 0; i < matrix_size; i++){
		memcpy(matrix_copy[i], matrix[i], sizeof(double)*matrix_size);
	}
	memcpy(matrix_extending_copy, matrix_extending, sizeof(double)*matrix_size);
	
	// forward elimination
	for (unsigned iter = 0; iter < matrix_size - 1; iter++){
		for (unsigned row_num = iter + 1; row_num < matrix_size; row_num++){
			double supply_coefficient = matrix_copy[row_num][iter] / matrix_copy[iter][iter];
			for (unsigned el_num = iter; el_num < matrix_size; el_num++)
				matrix_copy[row_num][el_num] -= supply_coefficient * matrix_copy[iter][el_num];
			
			matrix_extending_copy[row_num] -= supply_coefficient * matrix_extending_copy[iter];
		}
	}
	
	// backward elimination
	for (int iter = MATRIX_SIZE - 1; iter >= 0; iter--){
		solve[iter] = matrix_extending_copy[iter];
		for (unsigned el_num = MATRIX_SIZE -1; el_num > iter; el_num--)
			solve[iter] -= solve[el_num]*matrix_copy[iter][el_num];
		solve[iter] /= matrix_copy[iter][iter];
	}
	
	return solve;
}

int main(){
	srand(time(NULL));
// 	double A[MATRIX_SIZE][MATRIX_SIZE] = 
// 			{{81,92,44,48,51,28,98,90,31,25},	\
// 			{43,50,78,88,37,53,6,8,56,3},		\
// 			{56,65,54,4,55,18,27,13,61,100},	\
// 			{86,81,61,25,44,28,92,76,66,8},		\
// 			{7,58,94,92,77,15,81,95,18,6},		\
// 			{79,74,91,94,47,49,68,35,72,94},	\
// 			{36,49,59,59,38,12,89,23,33,85},	\
// 			{17,25,62,57,29,30,4,26,83,24},		\
// 			{70,40,37,40,1,69,43,49,81,26},		\
// 			{54,67,91,68,22,19,16,85,35,45}};
// 	
// 	double B[MATRIX_SIZE] = {23292, 17250, 19009, 24364, 17509, 28391, 17095, 16208, 23542, 19004};
// 	
// 	double X[MATRIX_SIZE] = {75, 1, 14, 23, 41, 76, 28, 55, 82, 38};
	
	FILE* python_code = fopen("matrixs/calculation_result_1.py", "w");
	
	init_printing_to_python_code(python_code);
	fprintf(python_code, "result_1 = [None for i in range(%d)]\n", TESTING_ITERATIONS);

	DataSerializationStruct result;
	for (int i = 1; i < TESTING_ITERATIONS+1; i++){
		char filename[100] = {'\0'};
		char str[100] = {0};
		
		sprintf(filename, "matrixs/matrix_%d", i);
		FILE* file = fopen(filename, "r");
		
	
		matrix_from_file(file, result.a_matrix);
		
		fill_vector(result.x_vector, 1,  100);
		
		dot_matrix_with_vector_save(result.b_vector,  result.a_matrix, result.x_vector);
		
		double* solving = gaussian_elimination_method( result.a_matrix, result.b_vector);
		memcpy(result.x_calculated, solving, sizeof(double)* MATRIX_SIZE);
		free(solving);
		
		result.cond_val = pow(10, i);
		
		sprintf(str, "result_1[%d]", i - 1);
		print_data_to_python_code(python_code, str, &result);

		fclose(file);
	}
	fclose(python_code);
	
	
	//second_result
	python_code = fopen("matrixs/calculation_result_1.py", "a");
	
// 	init_printing_to_python_code(python_code);
	fprintf(python_code, "result_2 = [None for i in range(%d)]\n", SIGMA_ITERATIONS);
	FILE* file = fopen("matrixs/matrix_1", "r");
	matrix_from_file(file,  result.a_matrix);
	fclose(file);
	fill_vector(result.x_vector, 0, 100);
	dot_matrix_with_vector_save(result.b_vector, result.a_matrix, result.x_vector);
	for (int i = 0; i < SIGMA_ITERATIONS; i++){

		char str[100] = {0};
		
		memset(result.err_vector, 0, sizeof(double)*MATRIX_SIZE);
		add_value_to_vector(result.err_vector, pow(10, -i));
		
		dot_matrix_with_vector_save(result.b_vector, result.a_matrix, result.x_vector);
		add_vector_to_vector(result.b_vector, result.err_vector);
		
		double* solving = gaussian_elimination_method(result.a_matrix, result.b_vector);
		memcpy(result.x_calculated, solving, sizeof(double)* MATRIX_SIZE);
		free(solving);
		
		result.cond_val = pow(10, i);
		
		sprintf(str, "result_2[%d]", i);
		print_data_to_python_code(python_code, str, &result);

	}
	fclose(python_code);
	
	
	//third_result
	python_code = fopen("matrixs/calculation_result_1.py", "a");
	
// 	init_printing_to_python_code(python_code);
	fprintf(python_code, "result_3 = [None for i in range(%d)]\n", SIGMA_ITERATIONS);
	file = fopen("matrixs/matrix_2", "r");
	matrix_from_file(file, result.a_matrix);
	fclose(file);
	dot_matrix_with_vector_save(result.b_vector, result.a_matrix, result.x_vector);
	for (int i = 0; i < SIGMA_ITERATIONS; i++){

		char str[100] = {0};
		
		memset(result.err_vector, 0, sizeof(double)*MATRIX_SIZE);
		add_value_to_vector(result.err_vector, pow(10, -i));
		
		add_vector_to_vector(result.b_vector, result.err_vector);
		dot_matrix_with_vector_save(result.b_vector, result.a_matrix, result.x_vector);
		
		double* solving = gaussian_elimination_method(result.a_matrix, result.b_vector);
		memcpy(result.x_calculated, solving, sizeof(double)* MATRIX_SIZE);
		free(solving);
		
		result.cond_val = pow(10, i);
		
		sprintf(str, "result_3[%d]", i);
		print_data_to_python_code(python_code, str, &result);

	}
	fclose(python_code);
}
