import numpy as np
from Condition_matrix_creation import condition_number_matirx_gen
from scipy.stats import ortho_group
from random import randint


def self_value_matrix_gen():
    with open("matrix/self_values", "w") as meta:
        for iter in range(100):
            with open(f"matrix/matrix_{iter}", "w") as file:
                matrix_O = ortho_group.rvs(dim=10)
                lambda_0 = 1
                self_values = [lambda_0]
                prev = lambda_0
                for i in range(9):
                    self_values.append(prev := prev * (((iter if iter != 49 else 50) + 1) / 50)) #игнорирование отделимости == 1

                self_values_matrix = np.diag(self_values)
                matrix_A = np.linalg.inv(matrix_O) @ self_values_matrix @ matrix_O
                print(10, file=file)
                print(matrix_A, file=file)
                print("\n", self_values, file=file)

                print(f"matrix/matrix_{iter}: {self_values}\n", file=meta)


# matrix_gen()
self_value_matrix_gen()
