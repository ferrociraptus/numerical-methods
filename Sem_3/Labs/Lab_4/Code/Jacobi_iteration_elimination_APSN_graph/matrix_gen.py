import numpy as np
from Condition_matrix_creation import condition_number_matirx_gen
from scipy.stats import ortho_group
from random import randint


def diagonal_check_dominance(mat: np.array) -> bool:
    mat = np.abs(mat)
    for i in range(mat.shape[0]):
        row_sum = np.sum(mat[i])
        if row_sum - mat[i][i] >= mat[i][i]:
            return False
    return True


def check(mat: np.array) -> bool:
    C = mat - np.diag(mat.diagonal())
    for row_i, mat_val in enumerate(mat.diagonal()):
        C[row_i] /= mat_val

    v, w = np.linalg.eig(C)
    for val in v:
        if np.abs(val) >= 1:
            return False
    return True


# for i in range(0, 20):
#     with open(f"matrix/matrix_{i}", "w") as file:
#         matrix = condition_number_matirx_gen(10 ** i, 10)
#         while not (diagonal_check_dominance(matrix) and check(matrix)):
#             matrix = condition_number_matirx_gen(10 ** i, 10)
#         print(np.linalg.det(matrix))
#         print(matrix, file=file)

def matrix_gen():
    for iter in range(0, 20):
        with open(f"matrix/matrix_{iter}", "w") as file:
            matrix = (np.random.rand(10, 10) - 0.5) * 100
            matrix = matrix - np.diag(matrix.diagonal())
            matrix_diag = np.array([np.sum(np.abs(matrix[i])) + 10 ** iter for i in range(10)])

            if iter == 0:
                matrix_diag[0] += 1

            matrix += np.diag(matrix_diag)

            print(10, file=file)
            print(matrix, file=file)


def multi_size_matrix_gen():
    for iter in range(2, 1000):
        with open(f"matrix/matrix_size{iter}", "w") as file:
            matrix = (np.random.rand(iter, iter)) * 10
            matrix = matrix - np.diag(matrix.diagonal())
            matrix_diag = np.array([np.sum(np.abs(matrix[i])) + 100 for i in range(iter)])

            if iter == 0:
                matrix_diag[0] += 1

            matrix += np.diag(matrix_diag)

            print(iter, file=file)
            print(matrix, file=file)


def self_value_matrix_gen():
    with open("matrix/self_values", "w") as meta:
        for iter in range(100):
            with open(f"matrix/matrix_{iter}", "w") as file:
                matrix_O = ortho_group.rvs(dim=10)
                lambda_0 = 1
                self_values = [lambda_0]
                prev = lambda_0
                for i in range(9):
                    self_values.append(prev := prev * (((iter if iter != 49 else 50) + 1) / 50))

                self_values_matrix = np.diag(self_values)
                matrix_A = np.linalg.inv(matrix_O) @ self_values_matrix @ matrix_O
                print(10, file=file)
                print(matrix_A, file=file)
                print("\n", self_values, file=file)

                print(f"matrix/matrix_{iter}: {self_values}\n", file=meta)


def self_value_matrix_gen():
    with open("matrix/self_values", "w") as meta:
        for iter in range(100):
            with open(f"matrix/matrix_{iter}", "w") as file:
                matrix_O = ortho_group.rvs(dim=10)
                lambda_0 = 1
                self_values = [lambda_0]
                prev = lambda_0
                for i in range(9):
                    self_values.append(prev := prev * (((iter if iter != 49 else 50) + 1) / 50))

                self_values_matrix = np.diag(self_values)
                matrix_A = np.linalg.inv(matrix_O) @ self_values_matrix @ matrix_O
                print(10, file=file)
                print(matrix_A, file=file)
                print("\n", self_values, file=file)

                print(f"matrix/matrix_{iter}: {self_values}\n", file=meta)


# matrix_gen()
self_value_matrix_gen()
