from matplotlib import pyplot as plt
import numpy as np
from matrix.calculation_result_1 import *
from decimal import *


def separability_to_iterations():
    fig, ax = plt.subplots()

    ax.plot([(i + 1) / 50 for i in range(98)], [val["iterations_amount"] for val in result_1],
            label=r"Jacobi elimination method $\epsilon^{-16}$", linewidth=2)

    ax.plot([(i + 1) / 50 for i in range(98)], [val["iterations_amount"] for val in result_2],
            label=r"Jacobi elimination method $\epsilon^{-8}$", linewidth=2)

    ax.grid(True)
    # ax.set_xscale('log')
    # ax.set_yscale('log')

    plt.xlabel(r"Separability value")
    plt.ylabel("Iterations amount")
    plt.title("Separability value to iterations amount")

    ax.legend(loc="lower right")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig('separability_value_to_iterations.png', dpi=100)


def iterations_to_accuracy():
    fig, ax = plt.subplots()

    ax.plot([10 ** -i for i in range(len(result_5))],
            [val["iterations_amount"] for val in result_5],
            label=r"Jacobi elimination method $\frac{\lambda_{n}}{\lambda_{n+1}} = 1$", linewidth=2)

    ax.plot([10 ** -i for i in range(len(result_6))],
            [val["iterations_amount"] for val in result_6],
            label=r"Jacobi elimination method $\frac{\lambda_{n}}{\lambda_{n+1}} = 1.96$", linewidth=2)

    ax.grid(True)
    ax.set_xscale('log')
    # ax.set_yscale('log')

    plt.xlabel(r"Accuracy")
    plt.ylabel("Iterations")
    plt.title(r"Iterations amount to accuracy")

    ax.legend(loc="upper right")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig('iterations_to_accuracy.png', dpi=100)


def accuracy_to_iterations():
    fig, ax = plt.subplots()

    ax.plot(list(range(result_3["iterations_amount"])),
            [np.linalg.norm(result_3["lambdas"] - iteration) / np.linalg.norm(result_3["lambdas"]) for iteration in
             result_3["iterations"]],
            label=r"Jacobi elimination method $\frac{\lambda_{n}}{\lambda_{n+1}} = 1$", linewidth=2)

    ax.plot(list(range(result_4["iterations_amount"])),
            [np.linalg.norm(result_4["lambdas"] - iteration) / np.linalg.norm(result_4["lambdas"]) for iteration in
             result_4["iterations"]],
            label=r"Jacobi elimination method $\frac{\lambda_{n}}{\lambda_{n+1}} = 1.96$", linewidth=2)

    ax.grid(True)
    # ax.set_xscale('log')
    # ax.set_yscale('log')

    plt.xlabel(r"Iteration")
    plt.ylabel("Accuracy amount")
    plt.title(r"Accuracy value to iterations amount $\epsilon = 10^{-16}$")

    ax.legend(loc="upper right")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig('accuracy_value_to_iterations.png', dpi=100)


separability_to_iterations()
iterations_to_accuracy()
accuracy_to_iterations()

plt.show()
