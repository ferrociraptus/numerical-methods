#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <ctype.h>

#define MATRIX_SIZE 10
#define TESTING_ITERATIONS 98
#define ITERATIONS 1000
#define EPS 1e-16

#define sign(val) ((signbit((val))) ? (-1) : (1))


typedef struct{
	double a_matrix[MATRIX_SIZE][MATRIX_SIZE];
	double lambda_vector[MATRIX_SIZE];
	double lambda_calculated[MATRIX_SIZE];
	double err_vector[MATRIX_SIZE];
}DataSerializationStruct;

int cmp(const void* val1, const void* val2){
	if ((*((double*)val1)) > (*((double*)val2)))
		return 1;
	else if ((*((double*)val1)) == (*((double*)val2)))
		return 0;
	else
		return -1;
}


double* dot_matrix_with_vector(double matrix[MATRIX_SIZE][MATRIX_SIZE], double vector[MATRIX_SIZE]){
	double* result = (double*) malloc(sizeof(double) * MATRIX_SIZE);
	memset(result, 0, sizeof(double)*MATRIX_SIZE);
	
	for (int row = 0; row < MATRIX_SIZE; row++)
		for (int col = 0; col < MATRIX_SIZE; col++)
			result[row] += vector[col] * matrix[row][col];
		
	return result;
}

double* dot_matrix_with_vector_save(double* result, double matrix[MATRIX_SIZE][MATRIX_SIZE], double vector[MATRIX_SIZE]){
	memset(result, 0, sizeof(double) * MATRIX_SIZE);
	for (int row = 0; row < MATRIX_SIZE; row++)
		for (int col = 0; col < MATRIX_SIZE; col++)
			result[row] += vector[col] * matrix[row][col];
		
	return result;
}

void init_printing_to_python_code(FILE* stream){
	fprintf(stream, "from decimal import *\n");
	fprintf(stream, "import numpy as np\n");
}

void print_matrix(FILE* stream, const char* field_name, double matrix[MATRIX_SIZE][MATRIX_SIZE]){
	fprintf(stream, "%s = ", field_name);
	for (unsigned row = 0; row < MATRIX_SIZE; row++){
		fprintf(stream, "[");
		for (unsigned col = 0; col < MATRIX_SIZE-1; col++)
			fprintf(stream, "%.15lf ", matrix[row][col]);
		fprintf(stream, "%.15lf]\n", matrix[row][MATRIX_SIZE - 1]);
		
		if (row < MATRIX_SIZE-1)
			fprintf(stream, ",");
	}
// 	fprintf(stream, "\n");
}

void print_matrix_to_python_code(FILE* stream, const char* field_name, double matrix[MATRIX_SIZE][MATRIX_SIZE]){
	fprintf(stream, "%s = np.array([", field_name);
	for (unsigned row = 0; row < MATRIX_SIZE; row++){
		fprintf(stream, "[");
		for (unsigned col = 0; col < MATRIX_SIZE-1; col++)
			fprintf(stream, "Decimal(\"%.20lf\"),", matrix[row][col]);
		fprintf(stream, "Decimal(\"%.20lf\")", matrix[row][MATRIX_SIZE - 1]);
		fprintf(stream, "]");
		
		if (row < MATRIX_SIZE-1)
			fprintf(stream, ",");
	}
	fprintf(stream, "], dtype=np.dtype(Decimal))\n");
}

void print_vector_to_python_code(FILE* stream, const char* field_name, double vector[MATRIX_SIZE]){
	fprintf(stream, "%s = np.array([", field_name);
	for (unsigned col = 0; col < MATRIX_SIZE-1; col++){
		fprintf(stream, "Decimal(\"%.20lf\")", vector[col]);
		if (col < MATRIX_SIZE-1)
			fprintf(stream, ",");
	}
	fprintf(stream, "], dtype=np.dtype(Decimal))\n");
}

void print_vector(FILE* stream, const char* field_name, double vector[MATRIX_SIZE]){
	fprintf(stream, "%s = [", field_name);
	for (unsigned col = 0; col < MATRIX_SIZE; col++){
		fprintf(stream, "%.10lf", vector[col]);
		if (col < MATRIX_SIZE-1)
			fprintf(stream, ",");
	}
	fprintf(stream, "]\n");
}

void print_data_to_python_code(FILE* stream, const char* field_name, DataSerializationStruct* str){
	
	char buf[100] = {0};
	fprintf(stream, "%s = {}\n", field_name);
	
	sprintf(buf, "%s[\"A\"]", field_name);
	print_matrix_to_python_code(stream, buf, str->a_matrix );
	
	sprintf(buf, "%s[\"lambdas_calculated\"]", field_name);
	print_vector_to_python_code(stream, buf, str->lambda_calculated);
	
	sprintf(buf, "%s[\"lambdas\"]", field_name);
	print_vector_to_python_code(stream, buf, str->lambda_vector);
	
	sprintf(buf, "%s[\"err\"]", field_name);
	print_vector_to_python_code(stream, buf, str->err_vector);
}

void add_value_to_vector(double vector[MATRIX_SIZE], double val){
	for (unsigned i = 0; i < MATRIX_SIZE; i++)
		vector[i] += val;
}

void matrix_from_file(FILE* file, double matrix[MATRIX_SIZE][MATRIX_SIZE]){
	int size;
	fscanf(file, "%d", &size);
	
	char ch_buf = getc(file);
	while(!isdigit(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
	ungetc(ch_buf, file);
	
	
	for (unsigned row = 0; row < MATRIX_SIZE; row++){
		for (unsigned col = 0; col < MATRIX_SIZE; col++){
			double val;
			fscanf(file, "%lf", &val);
			matrix[row][col] = val;
			
			ch_buf = getc(file);
			while(isspace(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
			while((!isdigit(ch_buf) && ch_buf != '-') && ch_buf != EOF) ch_buf = getc(file);
			ungetc(ch_buf, file);
		}
	}
}

void init_values_from_file(FILE* file, DataSerializationStruct* values){
	int size;
	fscanf(file, "%d", &size);
	
	char ch_buf = getc(file);
	while(!isdigit(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
	ungetc(ch_buf, file);
	
	
	for (unsigned row = 0; row < MATRIX_SIZE; row++){
		for (unsigned col = 0; col < MATRIX_SIZE; col++){
			double val;
			fscanf(file, "%lf", &val);
			values->a_matrix[row][col] = val;
			
			ch_buf = getc(file);
			while(isspace(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
			while((!isdigit(ch_buf) && ch_buf != '-') && ch_buf != EOF) ch_buf = getc(file);
			ungetc(ch_buf, file);
		}
	}
	
	ch_buf = getc(file);
	while(!isdigit(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
	ungetc(ch_buf, file);
	
	for (unsigned col = 0; col < MATRIX_SIZE; col++){
		double val;
		fscanf(file, "%lf", &val);
		values->lambda_vector[col] = val;
		
		ch_buf = getc(file);
		while(isspace(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
		while((!isdigit(ch_buf) && ch_buf != '-') && ch_buf != EOF) ch_buf = getc(file);
		ungetc(ch_buf, file);
	}
}


void copy_matrix(double matrix_to[MATRIX_SIZE][MATRIX_SIZE], double matrix_from[MATRIX_SIZE][MATRIX_SIZE]){
	for (int i = 0; i < MATRIX_SIZE; i++)
		memcpy(matrix_to[i], matrix_from[i], sizeof(double)*MATRIX_SIZE);
}

void dot_matrix_on_matrix_save(double result_matrix[MATRIX_SIZE][MATRIX_SIZE],double first_matrix[MATRIX_SIZE][MATRIX_SIZE], double second_matrix[MATRIX_SIZE][MATRIX_SIZE]){
	double ans_matrix[MATRIX_SIZE][MATRIX_SIZE];
	memset(ans_matrix, 0, sizeof(double) * MATRIX_SIZE * MATRIX_SIZE);
	
	for (int row = 0; row < MATRIX_SIZE; row++){
		for (int column = 0; column < MATRIX_SIZE; column++){
			for (int el = 0; el < MATRIX_SIZE; el++){
				ans_matrix[row][column] += first_matrix[row][el] * second_matrix[el][column]; 
			}
		}
	}
	
	memcpy(result_matrix, ans_matrix, sizeof(double) * MATRIX_SIZE * MATRIX_SIZE);
}

void dot_matrix_on_matrix(double first_matrix[MATRIX_SIZE][MATRIX_SIZE], double second_matrix[MATRIX_SIZE][MATRIX_SIZE]){
	double result_matrix[MATRIX_SIZE][MATRIX_SIZE];
	memset(result_matrix, 0, sizeof(double) * MATRIX_SIZE * MATRIX_SIZE);
	
	for (int row = 0; row < MATRIX_SIZE; row++){
		for (int column = 0; column < MATRIX_SIZE; column++){
			for (int el = 0; el < MATRIX_SIZE; el++){
				result_matrix[row][column] += first_matrix[row][el] * second_matrix[column][el]; 
			}
		}
	}
	
	memcpy(first_matrix, result_matrix, sizeof(double) * MATRIX_SIZE * MATRIX_SIZE);
}

void transpose_matrix(double matrix[MATRIX_SIZE][MATRIX_SIZE], double result[MATRIX_SIZE][MATRIX_SIZE]){
	double ans[MATRIX_SIZE][MATRIX_SIZE] = {0};
	for (int i = 0; i < MATRIX_SIZE; i++){
		for (int el = i; el < MATRIX_SIZE; el++){
			ans[i][el] = matrix[el][i];
			ans[el][i] = matrix[i][el];
		}
	}
	memcpy(result, ans, sizeof(double)*MATRIX_SIZE*MATRIX_SIZE);
}

unsigned find_self_values_my_jacobi_rotation_method(double A[MATRIX_SIZE][MATRIX_SIZE], double eps, double self_values[MATRIX_SIZE], double T[MATRIX_SIZE][MATRIX_SIZE]){
	memset(self_values, 0, sizeof(double) * MATRIX_SIZE);
	for (int i = 0; i < MATRIX_SIZE; i++){
		memset(T[i],0,sizeof(double)*MATRIX_SIZE);
		T[i][i] = 1;
	}
	
	int fix_i = 0, fix_j = 0, prev_i;
	double A_copy[MATRIX_SIZE][MATRIX_SIZE] = {0};
	copy_matrix(A_copy, A);
	double max_el = A_copy[0][1];
	unsigned couter = 0;
	while(1){
		max_el = A_copy[0][1];
		fix_i = 0;
		fix_j = 1;
		
		for (int j = 0; j < MATRIX_SIZE; j++){ // row
			for (int i = 0; i < MATRIX_SIZE; i++){ // column
				if (i != j){
					if (max_el < fabs(A_copy[i][j])){
						max_el = fabs(A_copy[i][j]);
						fix_i = i;
						fix_j = j;
					}
				}
			}
		}

		if (fabs(max_el) < eps)
			break;
		
		double c,s;
		double p = 2 * A_copy[fix_i][fix_j];
		double q = A_copy[fix_i][fix_i] - A_copy[fix_j][fix_j];
		double d = sqrt(pow(p,2) + pow(q, 2));
		if (fabs(q) <= eps){ // q == 0
			c = s = 1.0/sqrt(2);
		}
		else{
			double r = fabs(q)/(2*d);
			c = sqrt(0.5 + r);
			if (fabs(p / q) >= 1e-10)
				s = sign(p*q) * sqrt(0.5 - r);
			else
				s = fabs(p)*sign(p*q)/(2*d*c);
		}
		double iter_T[MATRIX_SIZE][MATRIX_SIZE] = {0};
		double iter_T_transpose[MATRIX_SIZE][MATRIX_SIZE];
		for (int i = 0; i < MATRIX_SIZE; i++){
			memset(iter_T[i],0,sizeof(double)*MATRIX_SIZE);
			iter_T[i][i] = 1;
		}

		
		iter_T[fix_i][fix_i] = c;
		iter_T[fix_j][fix_i] = s;
		iter_T[fix_j][fix_j] = c;
		iter_T[fix_i][fix_j] = -s;
		
		transpose_matrix(iter_T, iter_T_transpose);
		dot_matrix_on_matrix_save(A_copy, iter_T_transpose, A_copy);
		dot_matrix_on_matrix_save(A_copy, A_copy, iter_T);
		
		A_copy[fix_i][fix_j] = A_copy[fix_j][fix_i] = 0;
		dot_matrix_on_matrix(T, iter_T);
		
		couter++;
	}
	for (int i = 0; i < MATRIX_SIZE; i++){
		self_values[i] = A_copy[i][i];
	}
	return couter;
}

unsigned find_self_values_my_jacobi_rotation_method_with_iterations(
	double A[MATRIX_SIZE][MATRIX_SIZE],
	double eps,
	double self_values[MATRIX_SIZE],
	double T[MATRIX_SIZE][MATRIX_SIZE],
	FILE* file,
	const char* var_name){
	
	
	fprintf(file, "%s=[]\n", var_name);
	
	memset(self_values, 0, sizeof(double) * MATRIX_SIZE);
	for (int i = 0; i < MATRIX_SIZE; i++){
		memset(T[i],0,sizeof(double)*MATRIX_SIZE);
		T[i][i] = 1;
	}
	
	int fix_i = 0, fix_j = 0, prev_i;
	double A_copy[MATRIX_SIZE][MATRIX_SIZE] = {0};
	copy_matrix(A_copy, A);
	double max_el = A_copy[0][1];
	unsigned couter = 0;
	while(1){
		max_el = A_copy[0][1];
		fix_i = 0;
		fix_j = 1;
		
		for (int j = 0; j < MATRIX_SIZE; j++){ // row
			for (int i = 0; i < MATRIX_SIZE; i++){ // column
				if (i != j){
					if (max_el < fabs(A_copy[i][j])){
						max_el = fabs(A_copy[i][j]);
						fix_i = i;
						fix_j = j;
					}
				}
			}
		}

		if (fabs(max_el) < eps)
			break;
		
		double c,s;
		double p = 2 * A_copy[fix_i][fix_j];
		double q = A_copy[fix_i][fix_i] - A_copy[fix_j][fix_j];
		double d = sqrt(pow(p,2) + pow(q, 2));
		if (fabs(q) <= eps){ // q == 0
			c = s = 1.0/sqrt(2);
		}
		else{
			double r = fabs(q)/(2*d);
			c = sqrt(0.5 + r);
			if (fabs(p / q) >= 1e-10)
				s = sign(p*q) * sqrt(0.5 - r);
			else
				s = fabs(p)*sign(p*q)/(2*d*c);
		}
		double iter_T[MATRIX_SIZE][MATRIX_SIZE] = {0};
		double iter_T_transpose[MATRIX_SIZE][MATRIX_SIZE];
		for (int i = 0; i < MATRIX_SIZE; i++){
			memset(iter_T[i],0,sizeof(double)*MATRIX_SIZE);
			iter_T[i][i] = 1;
		}

		
		iter_T[fix_i][fix_i] = c;
		iter_T[fix_j][fix_i] = s;
		iter_T[fix_j][fix_j] = c;
		iter_T[fix_i][fix_j] = -s;
		
		transpose_matrix(iter_T, iter_T_transpose);
		dot_matrix_on_matrix_save(A_copy, iter_T_transpose, A_copy);
		dot_matrix_on_matrix_save(A_copy, A_copy, iter_T);
		
		A_copy[fix_i][fix_j] = A_copy[fix_j][fix_i] = 0;
		dot_matrix_on_matrix(T, iter_T);
		
		couter++;
		
		for (int i = 0; i < MATRIX_SIZE; i++){
			self_values[i] = A_copy[i][i];
		}
		
		fprintf(file, "%s.append(None)\n", var_name);
		char str[100];
		sprintf(str, "%s[-1]", var_name);
		qsort(self_values, MATRIX_SIZE, sizeof(double), cmp);
		print_vector_to_python_code(file, str, self_values);
	}
	
	return couter;
}


int main(){
	srand(time(NULL));
	
	FILE* python_code = fopen("matrix/calculation_result_1.py", "w");
	
	init_printing_to_python_code(python_code);
	fprintf(python_code, "result_1 = [None for i in range(%d)]\n", TESTING_ITERATIONS);
	fprintf(python_code, "result_2 = [None for i in range(%d)]\n", TESTING_ITERATIONS);
	for (int i = 0; i < TESTING_ITERATIONS; i++){
// 		int i = 0;
		DataSerializationStruct result;

		char filename[100] = {'\0'};
		char str[100] = {0};
		char str2[100] = {0};
		double solving[MATRIX_SIZE] = {0};
		
		
		sprintf(filename, "matrix/matrix_%d", i);
		FILE* file = fopen(filename, "r");
		
		sprintf(str, "result_1[%d]", i);
		sprintf(str2, "result_2[%d]", i);
		
		init_values_from_file(file, &result);

		double T[MATRIX_SIZE][MATRIX_SIZE];
		
		int iterations = find_self_values_my_jacobi_rotation_method(result.a_matrix, EPS, result.lambda_calculated, T);
		
		qsort(result.lambda_calculated, MATRIX_SIZE, sizeof(double), cmp);
		
		
		print_matrix(stdout, "Input matrix", result.a_matrix);
		putchar('\n');
		print_vector(stdout, "Input lambdas" ,result.lambda_vector);
		putchar('\n');
		print_vector(stdout, "Lambdas" ,result.lambda_calculated);
		putchar('\n');
		print_matrix(stdout, "Self vectors", T);
		
		print_data_to_python_code(python_code, str, &result);
		fprintf(python_code, "%s[\"iterations_amount\"] = %d\n\n", str, iterations);
		

		//with small epsilon
		iterations = find_self_values_my_jacobi_rotation_method(result.a_matrix, 1e-8, result.lambda_calculated, T);
		
		qsort(result.lambda_calculated, MATRIX_SIZE, sizeof(double), cmp);
		
		
		print_matrix(stdout, "Input matrix", result.a_matrix);
		putchar('\n');
		print_vector(stdout, "Input lambdas" ,result.lambda_vector);
		putchar('\n');
		print_vector(stdout, "Lambdas" ,result.lambda_calculated);
		putchar('\n');
		print_matrix(stdout, "Self vectors", T);
		
		print_data_to_python_code(python_code, str2, &result);
		fprintf(python_code, "%s[\"iterations_amount\"] = %d\n\n", str2, iterations);
		
	}
// 	fclose(python_code);

	// second
	
// 	fprintf(python_code, "result_3 = [None for i in range(%d)]\n", TESTING_ITERATIONS);
// 	fprintf(python_code, "result_4 = [None for i in range(%d)]\n", TESTING_ITERATIONS);
	{
		int i = 49;
		DataSerializationStruct result;

		char filename[100] = {'\0'};
		double solving[MATRIX_SIZE] = {0};
		
		
		sprintf(filename, "matrix/matrix_%d", i);
		FILE* file = fopen(filename, "r");
		
		init_values_from_file(file, &result);
		fclose(file);

		double T[MATRIX_SIZE][MATRIX_SIZE];
		
		
		//with very small epsilon
		int iterations = find_self_values_my_jacobi_rotation_method_with_iterations(result.a_matrix, EPS, result.lambda_calculated, T, python_code, "iterations");
		
		qsort(result.lambda_calculated, MATRIX_SIZE, sizeof(double), cmp);
		
		
		print_vector(stdout, "Lambdas" ,result.lambda_calculated);
		
		print_data_to_python_code(python_code, "result_3", &result);
		fprintf(python_code, "%s[\"iterations_amount\"] = %d\n", "result_3", iterations);
		fprintf(python_code, "%s[\"iterations\"] = iterations\n\n", "result_3");
	}
	
	{
		int i = 97;
		DataSerializationStruct result;

		char filename[100] = {'\0'};
		double solving[MATRIX_SIZE] = {0};
		
		
		sprintf(filename, "matrix/matrix_%d", i);
		FILE* file = fopen(filename, "r");
		
		init_values_from_file(file, &result);
		fclose(file);

		double T[MATRIX_SIZE][MATRIX_SIZE];
		
		int iterations = find_self_values_my_jacobi_rotation_method_with_iterations(result.a_matrix, 1e-16, result.lambda_calculated, T, python_code, "iterations");
		
		qsort(result.lambda_calculated, MATRIX_SIZE, sizeof(double), cmp);
		
		print_vector(stdout, "Lambdas" ,result.lambda_calculated);
		
		print_data_to_python_code(python_code, "result_4", &result);
		fprintf(python_code, "%s[\"iterations_amount\"] = %d\n", "result_4", iterations);
		fprintf(python_code, "%s[\"iterations\"] = iterations\n\n", "result_4");
	}
	
	
	//third
	fprintf(python_code, "result_5 = [None for i in range(17)]\n");
	fprintf(python_code, "result_6 = [None for i in range(17)]\n");
	
	double eps = 1;
	for (int i = 0; i <= 16; i++, eps /= 10){
		DataSerializationStruct result;

		char filename[100] = {'\0'};
		char str[100] = {0};
		double solving[MATRIX_SIZE] = {0};
		
		
		sprintf(filename, "matrix/matrix_%d", 49);
		FILE* file = fopen(filename, "r");

		sprintf(str, "result_5[%d]", i);
		
		init_values_from_file(file, &result);
		fclose(file);

		double T[MATRIX_SIZE][MATRIX_SIZE];
		
		int iterations = find_self_values_my_jacobi_rotation_method(result.a_matrix, eps, result.lambda_calculated, T);
		
		qsort(result.lambda_calculated, MATRIX_SIZE, sizeof(double), cmp);
		
		print_vector(stdout, "Lambdas" ,result.lambda_calculated);
		
		print_data_to_python_code(python_code, str, &result);
		fprintf(python_code, "%s[\"iterations_amount\"] = %d\n\n", str, iterations);
	}
	
	eps = 1;
	for (int i = 0; i <= 16; i++, eps /= 10){
		DataSerializationStruct result;

		char filename[100] = {'\0'};
		char str[100] = {0};
		double solving[MATRIX_SIZE] = {0};
		
		
		sprintf(filename, "matrix/matrix_%d", 97);
		FILE* file = fopen(filename, "r");

		sprintf(str, "result_6[%d]", i);
		
		init_values_from_file(file, &result);
		fclose(file);

		double T[MATRIX_SIZE][MATRIX_SIZE];
		
		int iterations = find_self_values_my_jacobi_rotation_method(result.a_matrix, eps, result.lambda_calculated, T);
		
		qsort(result.lambda_calculated, MATRIX_SIZE, sizeof(double), cmp);
		
		print_vector(stdout, "Lambdas" ,result.lambda_calculated);
		
		print_data_to_python_code(python_code, str, &result);
		fprintf(python_code, "%s[\"iterations_amount\"] = %d\n\n", str, iterations);
	}
	fclose(python_code);
	printf("END!!!\n");
}
