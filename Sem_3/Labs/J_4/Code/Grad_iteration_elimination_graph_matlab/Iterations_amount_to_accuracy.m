calculation_result


accuracy = [];
iterations_amount = [];
for key = [0:11]
  map = result_1(int2str(key));
  accuracy(end+1) = map('eps');
  iterations_amount(end+1) = map('iterations_amount');
endfor

semilogx(accuracy, iterations_amount)
grid on
xlabel 'accuracy'
ylabel 'iterations amount'
title 'Iterations amount to accuracy'