calculation_result


accuracy = [];
norms = [];
for key = [0:11]
  map = result_1(int2str(key));
  accuracy(end+1) = map('eps');
  norms(end+1) = norm(map('A') * transpose(map('X_calculated')) - transpose(map('B')));
endfor

loglog(accuracy, norms)
grid on
xlabel 'accuracy'
ylabel 'norm'
title 'Discrepancy norm to accuracy'