from scipy import linalg as la
import numpy as np


# def condition_number_matirx_gen(cond_number, n=100):
#     n = abs(n)
#     size = (n, n)
#     cond_number = abs(cond_number)
#     log_cond_P = np.log(cond_number)
#     exp_vec = np.arange(-log_cond_P / 4., log_cond_P * (n + 1) / (4 * (n - 1)), log_cond_P / (2. * (n - 1)))[:n]
#     s = np.exp(exp_vec)
#     S = np.diag(s)
#     U, _ = la.qr((np.random.rand(*size) - 5.) * 200)
#     V, _ = la.qr((np.random.rand(*size) - 5.) * 200)
#     P = U.dot(S).dot(V.T)
#     P = P.dot(P.T)
#     return P

def condition_number_matirx_gen(cond_number, n=100):
    cond_number = abs(cond_number)
    n = abs(n)
    size = (n, n)
    # diag = (np.random.rand(n) * cond_number)
    # diag = np.array([i if i > 1 else i+1 for i in diag])
    # diag[0] = 1
    # diag[-1] = cond_number
    diag = np.linspace(1, cond_number, n)
    D = np.diag(diag)
    Q, R = np.linalg.qr((np.random.rand(*size) - 5.) * 200)

    A = Q @ D @ Q.T
    return A

# mat = condition_number_matirx_gen(100,10)
# # print(mat.diagonal())
# print(np.linalg.cond(mat))


