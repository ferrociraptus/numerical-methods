#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>
#include <math.h>

#define R 10


void ReadMatrix(FILE* fileID, double A[R][R])
{
	int i = 0, j = 0, counter = 0, flag = 1;
	char number[30] = { 0 };

	while (flag)
	{
		char c = fgetc(fileID);

		if (c == ';')
		{
			flag = 0;
		}

		if ((c != ' ') && (c != '\n'))
		{
			number[counter] = c;
			counter++;
		}
		else
		{
			number[counter] = '\0';
			A[i][j] = atof(number);
			//printf("%f", atof(number));
			counter = 0;

			if (c == ' ')
				j++;
			else
			{
				j = 0;
				i++;
			}
		}
	}
}

void ReadColumn(FILE* fileID, double arr[R])
{
	int i = 0, j = 0, counter = 0, flag = 1;
	char number[30] = { 0 };
	while (flag)
	{
		char c = fgetc(fileID);

		if (c == ';')
		{
			flag = 0;
		}
		if (c != '\n')
		{
			number[counter] = c;
			counter++;
		}
		else
		{
			number[counter] = '\0';
			arr[i] = atof(number);
		//	printf("%16.15lf\n", arr[i]);
			counter = 0;
			i++;
		}
	}
}

void PrintMatrix(double M[R][R])
{
	for (int i = 0; i < R; i++)
	{
		printf(" {  ");
		for (int j = 0; j < R; j++)
		{
			if (M[i][j] < 0)
			{
				printf("%16.15lf   ", M[i][j]);

			}
			else
			{
				printf(" %16.15lf   ", M[i][j]);
			}

			if (j == R - 1)
			{
				printf("}\n");
			}
		}
	}
}

void MatrixByVector(double A[R][R], double V[R], double D[R])
{
	for (int i = 0; i < R; i++)
	{
		D[i] = 0;
	}

	for (int i = 0; i < R; i++) {
		for (int j = 0; j < R; j++) {
			D[i] += A[i][j] * V[j];
		}
	}
}

void ShiftMatrix(double A[R][R], double val){
	for (int i = 0; i < R; i++)
		A[i][i] -= val;
}

double FindNorm(double M[R])
{
	double norm = 0;
	for (int i = 0; i < R; i++)
	{
		norm += M[i] * M[i];
	}
	norm = sqrt(norm);

	return (norm);
}

double Scalar(double V1[R], double V2[R])
{
	double scalar = 0;
	for (int i = 0; i < R; i++)
	{
		scalar += V1[i] * V2[i];
	}
	return scalar;
}


double getSecNormColumn(double column[R]) {
	double res = 0;
	for (int i = 0; i < R; i++)
		res += column[i] * column[i];
	return sqrt(res);
}


void getOrthonormalVec(double col[R], double res[R]) {
	double norm = getSecNormColumn(col);
	memcpy(res, col, sizeof(double)*R);
	if (norm == 0)
		return;
	for (int i = 0; i < R; i++)
		res[i] /= norm;
}


void getLUFactorization(double matrix_A[R][R], double matrix_L[R][R], double matrix_U[R][R]) {

	for (int k = 0; k < R; k++) {
		memset(matrix_L[k], 0, sizeof(double)*R);
		matrix_L[k][k] = 1;
	}

	for (int k = 0; k < R; k++) {
		memset(matrix_U[k], 0, sizeof(double)*R);
	}

	double temp_sum;

	//zeroth step

	for (int j = 0; j < R; j++)
		matrix_U[0][j] = matrix_A[0][j];
	for (int i = 1; i < R; i++)
		matrix_L[i][0] = matrix_A[i][0] / matrix_U[0][0];

	// other steps
	for (int m = 1; m < R; m++) {
		for (int j = m - 1; j < R; j++) {
			temp_sum = 0.0;
			for (int k = 0; k < m; k++)
				temp_sum += matrix_L[m][k] * matrix_U[k][j];
			matrix_U[m][j] = matrix_A[m][j] - temp_sum;
		}

		for (int i = m; i < R; i++) {
			temp_sum = 0.0;
			for (int k = 0; k < m; k++)
				temp_sum += matrix_L[i][k] * matrix_U[k][m];
			matrix_L[i][m] = (matrix_A[i][m] - temp_sum) / matrix_U[m][m];
		}
	}
}

void solveLUSystem(double matrix_L[R][R], double matrix_U[R][R], double column_B[R], double solution[R]) {
	double temp_column[R] = {0};
	double temp_sum = 0;

	/* 1 epoch */

	temp_column[0] = column_B[0];
	// other steps
	for (int i = 1; i < R; i++) {
		// calculate auxiliary sum
		for (int j = 0; j < i; j++)
			temp_sum += matrix_L[i][j] * temp_column[j];

		temp_column[i] = (column_B[i] - temp_sum);
		temp_sum = 0;
	}

	/* 2 epoch */

	solution[R - 1] = temp_column[R - 1] / matrix_U[R - 1][R - 1];
	// other steps
	for (int i = R - 2; i >= 0; i--) {
		for (int j = R - 1; j > i; j--)
			temp_sum += matrix_U[i][j] * solution[j];

		solution[i] = (temp_column[i] - temp_sum) / matrix_U[i][i];
		temp_sum = 0;
	}
}

void solveSystem(double matrix_A[R][R], double column_B[R], double res[R]) {
	double L[R][R] = {{0}};
	double U[R][R] = {{0}};
	getLUFactorization(matrix_A, L, U);
	solveLUSystem(U, L, column_B, res);
}


double reverseIterationsWithShift(double A[R][R], double start_lambda, double start_vec[R], double eps, double X[R]) {
	double x_k[R];
	getOrthonormalVec(start_vec, x_k);
	double x_k_1[R];
	double delta[R];

	double lambda_k = start_lambda, lambda_k_1, temp;
	do {
		memcpy(x_k_1, x_k, sizeof(double) * R);
		lambda_k_1 = lambda_k;
		ShiftMatrix(A, lambda_k_1);
		solveSystem(A, x_k_1, x_k);

		if (x_k[0] * x_k_1[0] < 0)
			for (int i = 0; i < R; i++)
				x_k[i] *= -1.0;

		temp = 0;
		for (int i = 0; i < R; i++)
			if (x_k[i] != 0)
				temp += x_k_1[i] / x_k[i];

		lambda_k = lambda_k_1 + temp / R;

		getOrthonormalVec(x_k, x_k);

		for (int i = 0; i < R; i++)
			delta[i] = x_k[i] - x_k_1[i];

	} while (getSecNormColumn(delta) > eps);
	memcpy(X, x_k, sizeof(double)*R);
	return lambda_k;
}

int PowerMethod(double A[R][R], double V[R], double W[R], double* lambda, double EPS)
{
	double x_k[R] = { 0 };
	double x_k1[R] = { 0 };
	double l1 = 0, l0 = 0;
	double norm, norm1;
	int counter = 1;
	
	memset(V, 0, R*sizeof(double));
	memset(W, 0, R*sizeof(double));

	for (int i = 0; i < R; i++)
	{
		x_k[i] = 1;
	}

	while (4) 
	{
		l0 = l1;
		MatrixByVector(A, x_k, x_k1);


		l1 = x_k1[0] / x_k[0];
		// l1 = x_k1[0]/ x_k[0];
		
		norm1 = FindNorm(x_k1);

		// printf("%.15lf\n", l1);

		for (int i = 0; i < R; i++)
		{
			x_k1[i] /= norm1;
		//	printf("xk = %.15lf\n", x_k1[i]);
		}

		for (int i = 0; i < R; i++)
		{
			x_k[i] = x_k1[i];
		}
		
		counter++;

		if (fabs(l0 - l1) < EPS)
			break;
	}
	printf("L = %.15lf ", l1);
// 	printf("NormL: %.15lf\n", fabs(l1-100));

	//for (int i = 0; i < R; i++)
	//{
	//	printf("w = %.15lf\n", vector[i]);
	//}

	
	for (int i = 0; i < R; i++)
	{
		V[i] = x_k1[i] / pow(l1, counter);

	}
	norm = FindNorm(V);

	for (int i = 0; i < R; i++)
	{
		V[i] /= norm;
// 		printf("w = %.15lf\n", V[i]);
	}

	double temp_vect[R];
	for (int i = 0; i < R; i++)
	{
		temp_vect[i] = V[i] - W[i];
	}
// 	printf("NormW: %.15lf\n", FindNorm(temp_vect));
	
	*lambda = l1;
	return counter;
}


int main(void)
{
	double A[R][R] = { 0 };
	double V[R] = { 0 };
	int res = 0;
	double W[R];
	
	FILE* file_A = fopen("MatrixA.txt", "r");
	ReadMatrix(file_A, A);
	fclose(file_A);
	FILE* file_W = fopen("MatrixW.txt", "r");
	ReadColumn(file_W, W);
	fclose(file_W);
	/*
	for (int i = 0; i < R; i++)
	{
		for (int j = 0; j < R; j++) {
			printf("%.15lf   ", A[i][j]);
		}
		printf("\n");
	}
	*/

	
	for (double EPS = 0.1; EPS > 1e-10; EPS /= 10)
	{
		double lambda;
		res = PowerMethod(A, V, W, &lambda, EPS);
		lambda = reverseIterationsWithShift(A, lambda, V, EPS, V);
		printf("L = %.15lf\n", lambda);
		for (int i = 0; i < R; i++)
		{
			printf("w = %.15lf\n", V[i]);
		}
		
		printf("For EPS = %.15lf Iterations = %d\n", EPS, res);
		
	}
	return 0;
}
