from matplotlib import pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
from matrix.calculation_result_1 import *
from decimal import *


def diag_to_iterations():
    fig, ax = plt.subplots()

    ax.plot([10 ** i for i in range(len(result_1))], [len(result_1[i]["jacobi iter"]) for i in range(len(result_1))],
            label="Jacobi elimination method", linewidth=2)

    ax.grid(True)
    ax.set_xscale('log')
    # ax.set_yscale('log')

    plt.xlabel(r"Diagonal dominance")
    plt.ylabel("Iterations amount")
    plt.title("Matrix diagonal dominance to iterations amount")

    ax.legend(loc="upper right")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig('diag_domaince_to_iterations.png', dpi=100)


def pogr_to_iteration_plot():
    fig, ax = plt.subplots()

    ax.plot(list(range(len(result_1[0]["jacobi iter"]))),
            [np.linalg.norm(jac - result_1[0]["X"], np.inf) for jac in result_1[0]["jacobi iter"]],
            label="Jacobi elimination method", linewidth=2)

    ax.grid(True)
    # ax.set_xscale('log')
    ax.set_yscale('log')

    plt.xlabel(r"Iteration")
    plt.ylabel("Error")
    plt.title("Iteration to error")

    ax.legend(loc="upper right")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig('Error_to_iteration.png', dpi=100)


def gauss_and_jacobi_compare():
    fig, ax = plt.subplots()

    ax.plot(list(range(2, 1000)), [line["jacobi_time"] for line in result_2],
            label="Jacobi elimination method", linewidth=2)
    ax.plot(list(range(2, 1000)), [line["gauss_time"] for line in result_2],
            label="Gauss elimination method", linewidth=2)

    ax.grid(True)
    # ax.set_xscale('log')
    # ax.set_yscale('log')

    plt.xlabel(r"Matrix size")
    plt.ylabel("Calculation time, s")
    plt.title("Calculation time to matrix size")

    ax.legend(loc="upper left")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig('Comparing.png', dpi=100)
    
    # Aprox compare
    fig, ax = plt.subplots()

    param, res1 = curve_fit(lambda x, y: x**3/y, list(range(2, 1000)), [line["gauss_time"] for line in result_2])
    # print("approx coefficients: ", param)
    ans = [(lambda x, b: x**3/b)(i, param[0]) for i in list(range(2, 1000))]
    ax.plot(list(range(2, 1000)), ans, label=r'$y = \frac{x^3}{b}$, approximation', color='r')
    ax.plot(list(range(2, 1000)), [line["gauss_time"] for line in result_2],
            label="Gauss elimination method", linewidth=2)
    # ax.plot(list(range(2, 1000)), [line["gauss_time"] for line in result_2],
    #         marker='o')
    plt.title("Calculation time to matrix size")
    plt.ylabel("Calculation time, s")
    plt.xlabel("Matrix size")

    param, res1 = curve_fit(lambda x, y: x**2 / y, list(range(2, 1000)), [line["jacobi_time"] for line in result_2])
    ans = [(lambda x, y: x**2 / y)(i, param[0]) for i in list(range(2, 1000))]
    ax.plot(list(range(2, 1000)), ans, label=r'$y = \frac{x^2}{b}$, approximation')
    ax.plot(list(range(2, 1000)), [line["jacobi_time"] for line in result_2],
            label="Jacobi elimination method", linewidth=2)
    ax.grid(True)
    # ax.set_xscale('log')
    # ax.set_yscale('log')

    plt.xlabel(r"Matrix size")
    plt.ylabel("Calculation time, s")
    plt.title("Calculation time to matrix size")

    ax.legend(loc="upper left")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig('Comparing_with_approx.png', dpi=100)

#
# diag_to_iterations()
# pogr_to_iteration_plot()
gauss_and_jacobi_compare()
plt.show()
