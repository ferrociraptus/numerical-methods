#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <ctype.h>
#include <float.h>

#define FLT_ROUNDS 0

#define MATRIX_SIZE 10
#define TESTING_ITERATIONS 98
#define ITERATIONS 1000
#define EPS 1e-16

#define sign(val) ((signbit((val))) ? (-1) : (1))


typedef struct{
	long double a_matrix[MATRIX_SIZE][MATRIX_SIZE];
	long double lambda_vector[MATRIX_SIZE];
	long double lambda_calculated[MATRIX_SIZE];
	long double err_vector[MATRIX_SIZE];
	long double eps;
	long double T[MATRIX_SIZE][MATRIX_SIZE];
}DataSerializationStruct;

int cmp(const void* val1, const void* val2){
	if ((*((long double*)val1)) > (*((long double*)val2)))
		return 1;
	else if ((*((long double*)val1)) == (*((long double*)val2)))
		return 0;
	else
		return -1;
}


long double* dot_matrix_with_vector(long double matrix[MATRIX_SIZE][MATRIX_SIZE], long double vector[MATRIX_SIZE]){
	long double* result = (long double*) malloc(sizeof(long double) * MATRIX_SIZE);
	memset(result, 0, sizeof(long double)*MATRIX_SIZE);
	
	for (int row = 0; row < MATRIX_SIZE; row++)
		for (int col = 0; col < MATRIX_SIZE; col++)
			result[row] += vector[col] * matrix[row][col];
		
	return result;
}

long double* dot_matrix_with_vector_save(long double* result, long double matrix[MATRIX_SIZE][MATRIX_SIZE], long double vector[MATRIX_SIZE]){
	memset(result, 0, sizeof(long double) * MATRIX_SIZE);
	for (int row = 0; row < MATRIX_SIZE; row++)
		for (int col = 0; col < MATRIX_SIZE; col++)
			result[row] += vector[col] * matrix[row][col];
		
	return result;
}


void print_matrix(FILE* stream, const char* field_name, long double matrix[MATRIX_SIZE][MATRIX_SIZE]){
	fprintf(stream, "%s = [", field_name);
	for (unsigned row = 0; row < MATRIX_SIZE; row++){
		fprintf(stream, "[");
		for (unsigned col = 0; col < MATRIX_SIZE-1; col++)
			fprintf(stream, "%.20Lf ", matrix[row][col]);
		fprintf(stream, "%.20Lf", matrix[row][MATRIX_SIZE - 1]);
		fprintf(stream, "]");
		
		if (row < MATRIX_SIZE-1)
			fprintf(stream, ";");
	}
	fprintf(stream, "];\n");
}


void print_matrix_to_matlab_code(FILE* stream, const char* field_name, long double matrix[MATRIX_SIZE][MATRIX_SIZE]){
	fprintf(stream, "%s = [", field_name);
	for (unsigned row = 0; row < MATRIX_SIZE; row++){
		fprintf(stream, "");
		for (unsigned col = 0; col < MATRIX_SIZE-1; col++)
			fprintf(stream, "%.30Lf ", matrix[row][col]);
		fprintf(stream, "%.30Lf", matrix[row][MATRIX_SIZE - 1]);
		fprintf(stream, "");
		
		if (row < MATRIX_SIZE-1)
			fprintf(stream, ";");
	}
	fprintf(stream, "];\n");
}

void print_vector_to_matlab_code(FILE* stream, const char* field_name, long double vector[MATRIX_SIZE]){
	fprintf(stream, "%s = [", field_name);
	for (unsigned col = 0; col < MATRIX_SIZE; col++){
		fprintf(stream, "%.30Lf", vector[col]);
		if (col < MATRIX_SIZE-1)
			fprintf(stream, " ");
	}
	fprintf(stream, "];\n");
}

void print_vector(FILE* stream, const char* field_name, long double vector[MATRIX_SIZE]){
	fprintf(stream, "%s = [", field_name);
	for (unsigned col = 0; col < MATRIX_SIZE; col++){
		fprintf(stream, "\"%.10Lf\"", vector[col]);
		if (col < MATRIX_SIZE-1)
			fprintf(stream, ",");
	}
	fprintf(stream, "]\n");
}

void print_data_to_matlab_code(FILE* stream, const char* field_name, DataSerializationStruct* str){
	char buf[100] = {0};
	fprintf(stream, "%s = containers.Map();\n", field_name);
	
	sprintf(buf, "%s(\'A\')", field_name);
	print_matrix_to_matlab_code(stream, buf, str->a_matrix );
	
	sprintf(buf, "%s(\'L\')", field_name);
	print_vector_to_matlab_code(stream, buf, str->lambda_vector);
	
	sprintf(buf, "%s(\'L_calculated\')", field_name);
	print_vector_to_matlab_code(stream, buf, str->lambda_calculated);
	
	sprintf(buf, "%s(\'T\')", field_name);
	print_matrix_to_matlab_code(stream, buf, str->T);

	fprintf(stream, "%s(\'eps\') = %.20Lf\n", field_name, str->eps);
	
	
}


void add_value_to_vector(long double vector[MATRIX_SIZE], long double val){
	for (unsigned i = 0; i < MATRIX_SIZE; i++)
		vector[i] += val;
}

void matrix_from_file(FILE* file, long double matrix[MATRIX_SIZE][MATRIX_SIZE]){
	int size;
	fscanf(file, "%d", &size);
	
	char ch_buf = getc(file);
	while(!isdigit(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
	ungetc(ch_buf, file);
	
	
	for (unsigned row = 0; row < MATRIX_SIZE; row++){
		for (unsigned col = 0; col < MATRIX_SIZE; col++){
			long double val;
			fscanf(file, "%Lf", &val);
			matrix[row][col] = val;
			
			ch_buf = getc(file);
			while(isspace(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
			while((!isdigit(ch_buf) && ch_buf != '-') && ch_buf != EOF) ch_buf = getc(file);
			ungetc(ch_buf, file);
		}
	}
}

void init_values_from_file(FILE* file, DataSerializationStruct* values){
	char ch_buf = getc(file);
	while(!isdigit(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
	ungetc(ch_buf, file);
	
	
	for (unsigned row = 0; row < MATRIX_SIZE; row++){
		for (unsigned col = 0; col < MATRIX_SIZE; col++){
			long double val;
			fscanf(file, "%Lf", &val);
			values->a_matrix[row][col] = val;
			
			ch_buf = getc(file);
			while(isspace(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
			while((!isdigit(ch_buf) && ch_buf != '-') && ch_buf != EOF) ch_buf = getc(file);
			ungetc(ch_buf, file);
		}
	}
	
	ch_buf = getc(file);
	while(!isdigit(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
	ungetc(ch_buf, file);
	
	for (unsigned col = 0; col < MATRIX_SIZE; col++){
		long double val;
		fscanf(file, "%Lf", &val);
		values->lambda_vector[col] = val;
		
		ch_buf = getc(file);
		while(isspace(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
		while((!isdigit(ch_buf) && ch_buf != '-') && ch_buf != EOF) ch_buf = getc(file);
		ungetc(ch_buf, file);
	}
}


void copy_matrix(long double matrix_to[MATRIX_SIZE][MATRIX_SIZE], long double matrix_from[MATRIX_SIZE][MATRIX_SIZE]){
	for (int i = 0; i < MATRIX_SIZE; i++)
		memcpy(matrix_to[i], matrix_from[i], sizeof(long double)*MATRIX_SIZE);
}

void dot_matrix_on_matrix_save(long double result_matrix[MATRIX_SIZE][MATRIX_SIZE],long double first_matrix[MATRIX_SIZE][MATRIX_SIZE], long double second_matrix[MATRIX_SIZE][MATRIX_SIZE]){
	long double ans_matrix[MATRIX_SIZE][MATRIX_SIZE];
	memset(ans_matrix, 0, sizeof(long double) * MATRIX_SIZE * MATRIX_SIZE);
	
	for (int row = 0; row < MATRIX_SIZE; row++){
		for (int column = 0; column < MATRIX_SIZE; column++){
			for (int el = 0; el < MATRIX_SIZE; el++){
				ans_matrix[row][column] += first_matrix[row][el] * second_matrix[el][column]; 
			}
		}
	}
	
	memcpy(result_matrix, ans_matrix, sizeof(long double) * MATRIX_SIZE * MATRIX_SIZE);
}

void dot_matrix_on_matrix(long double first_matrix[MATRIX_SIZE][MATRIX_SIZE], long double second_matrix[MATRIX_SIZE][MATRIX_SIZE]){
	long double result_matrix[MATRIX_SIZE][MATRIX_SIZE];
	memset(result_matrix, 0, sizeof(long double) * MATRIX_SIZE * MATRIX_SIZE);
	
	for (int row = 0; row < MATRIX_SIZE; row++){
		for (int column = 0; column < MATRIX_SIZE; column++){
			for (int el = 0; el < MATRIX_SIZE; el++){
				result_matrix[row][column] += first_matrix[row][el] * second_matrix[column][el]; 
			}
		}
	}
	
	memcpy(first_matrix, result_matrix, sizeof(long double) * MATRIX_SIZE * MATRIX_SIZE);
}

void transpose_matrix(long double matrix[MATRIX_SIZE][MATRIX_SIZE], long double result[MATRIX_SIZE][MATRIX_SIZE]){
	long double ans[MATRIX_SIZE][MATRIX_SIZE] = {0};
	for (int i = 0; i < MATRIX_SIZE; i++){
		for (int el = i; el < MATRIX_SIZE; el++){
			ans[i][el] = matrix[el][i];
			ans[el][i] = matrix[i][el];
		}
	}
	memcpy(result, ans, sizeof(long double)*MATRIX_SIZE*MATRIX_SIZE);
}

unsigned find_self_values_my_jacobi_rotation_method(long double A[MATRIX_SIZE][MATRIX_SIZE], long double eps, long double self_values[MATRIX_SIZE], long double T[MATRIX_SIZE][MATRIX_SIZE]){
	memset(self_values, 0, sizeof(long double) * MATRIX_SIZE);
	for (int i = 0; i < MATRIX_SIZE; i++){
		memset(T[i],0,sizeof(long double)*MATRIX_SIZE);
		T[i][i] = 1;
	}
	
	int fix_i = 0, fix_j = 0;
	long double A_copy[MATRIX_SIZE][MATRIX_SIZE] = {0};
	copy_matrix(A_copy, A);
	long double max_el = A_copy[0][1];
	unsigned couter = 0;
	while(1){
		max_el = A_copy[0][1];
		fix_i = 0;
		fix_j = 1;
		
		for (int j = 0; j < MATRIX_SIZE; j++){ // row
			for (int i = 0; i < MATRIX_SIZE; i++){ // column
				if (i != j){
					if (max_el < fabsl(A_copy[i][j])){
						max_el = fabsl(A_copy[i][j]);
						fix_i = i;
						fix_j = j;
					}
				}
			}
		}

		if (fabsl(max_el) < eps)
			break;
		
		long double c,s;
		long double p = 2 * A_copy[fix_i][fix_j];
		long double q = A_copy[fix_i][fix_i] - A_copy[fix_j][fix_j];
		long double d = sqrtl(powl(p,2) + powl(q, 2));
		if (fabsl(q) <= eps){ // q == 0
			c = s = 1.0/sqrtl(2);
		}
		else{
			long double r = fabsl(q)/(2*d);
			c = sqrtl(0.5 + r);
			if (fabsl(p / q) >= 1e-10)
				s = sign(p*q) * sqrtl(0.5 - r);
			else
				s = fabsl(p)*sign(p*q)/(2*d*c);
		}
		long double iter_T[MATRIX_SIZE][MATRIX_SIZE] = {0};
		long double iter_T_transpose[MATRIX_SIZE][MATRIX_SIZE];
		for (int i = 0; i < MATRIX_SIZE; i++){
			memset(iter_T[i],0,sizeof(long double)*MATRIX_SIZE);
			iter_T[i][i] = 1;
		}

		
		iter_T[fix_i][fix_i] = c;
		iter_T[fix_j][fix_i] = s;
		iter_T[fix_j][fix_j] = c;
		iter_T[fix_i][fix_j] = -s;
		
		transpose_matrix(iter_T, iter_T_transpose);
		dot_matrix_on_matrix_save(A_copy, iter_T_transpose, A_copy);
		dot_matrix_on_matrix_save(A_copy, A_copy, iter_T);
		
// 		A_copy[fix_i][fix_i] = c*c*A_copy[fix_i][fix_i] + s*s*A_copy[fix_j][fix_j] + 2*c*s*A_copy[fix_i][fix_j];
// 		A_copy[fix_j][fix_j] = s*s*A_copy[fix_i][fix_i] + c*c*A_copy[fix_j][fix_j] - 2*c*s*A_copy[fix_i][fix_j];
// 		
		A_copy[fix_i][fix_j] = A_copy[fix_j][fix_i] = 0;
// 		for (int m = 0; m < MATRIX_SIZE; m++){
// 			if (m != fix_i && m != fix_j){
// 				A_copy[fix_i][m] = A_copy[m][fix_i] = c*A_copy[m][fix_i] + s*A_copy[m][fix_j];
// 				A_copy[fix_j][m] = A_copy[m][fix_j] = -s*A_copy[m][fix_i] + c*A_copy[m][fix_j];
// 			}
// 		}
		dot_matrix_on_matrix(T, iter_T);
		
		couter++;
	}
	for (int i = 0; i < MATRIX_SIZE; i++){
		self_values[i] = A_copy[i][i];
	}
	return couter;
}

unsigned find_self_values_my_jacobi_rotation_method_with_iterations(
	long double A[MATRIX_SIZE][MATRIX_SIZE],
	long double eps,
	long double self_values[MATRIX_SIZE],
	long double T[MATRIX_SIZE][MATRIX_SIZE],
	FILE* file,
	const char* var_name){
	
	
	fprintf(file, "%s = containers.Map();\n", var_name);
	
	memset(self_values, 0, sizeof(long double) * MATRIX_SIZE);
	for (int i = 0; i < MATRIX_SIZE; i++){
		memset(T[i],0,sizeof(long double)*MATRIX_SIZE);
		T[i][i] = 1;
	}
	
	int fix_i = 0, fix_j = 0;
	long double A_copy[MATRIX_SIZE][MATRIX_SIZE] = {0};
	copy_matrix(A_copy, A);
	long double max_el = A_copy[0][1];
	unsigned counter = 0;
	while(1){
		max_el = A_copy[0][1];
		fix_i = 0;
		fix_j = 1;
		
		for (int j = 0; j < MATRIX_SIZE; j++){ // row
			for (int i = 0; i < MATRIX_SIZE; i++){ // column
				if (i != j){
					if (max_el < fabsl(A_copy[i][j])){
						max_el = fabsl(A_copy[i][j]);
						fix_i = i;
						fix_j = j;
					}
				}
			}
		}

		if (fabsl(max_el) <= eps)
			break;
		
		long double c,s;
		long double p = 2 * A_copy[fix_i][fix_j];
		long double q = A_copy[fix_i][fix_i] - A_copy[fix_j][fix_j];
		long double d = sqrtl(powl(p,2) + powl(q, 2));
		if (fabsl(q) == eps * 10){ // q == 0
			c = s = 1.0/sqrtl(2);
		}
		else{
			long double r = fabsl(q)/(2*d);
			c = sqrtl(0.5 + r);
			if (fabsl(p / q) >= 1e-10)
				s = sign(p*q) * sqrtl(0.5 - r);
			else
				s = fabsl(p)*sign(p*q)/(2*d*c);
		}
		long double iter_T[MATRIX_SIZE][MATRIX_SIZE] = {0};
		long double iter_T_transpose[MATRIX_SIZE][MATRIX_SIZE];
		for (int i = 0; i < MATRIX_SIZE; i++){
			memset(iter_T[i],0,sizeof(long double)*MATRIX_SIZE);
			iter_T[i][i] = 1;
		}
		int flag = 0;
// 		if (c == 1){
// 			c += sqrtl(eps);
// 			s += sign(s) * sqrtl(eps);
// 			flag = 1;
// 		}
		iter_T[fix_i][fix_i] = c;
		iter_T[fix_j][fix_i] = s;
		iter_T[fix_j][fix_j] = c;
		iter_T[fix_i][fix_j] = -s;

		transpose_matrix(iter_T, iter_T_transpose);
		dot_matrix_on_matrix_save(A_copy, iter_T_transpose, A_copy);
		dot_matrix_on_matrix_save(A_copy, A_copy, iter_T);

		if (c == 1){
			A_copy[fix_i][fix_j] = A_copy[fix_j][fix_i] = DBL_EPSILON;
		}

		dot_matrix_on_matrix(T, iter_T);
		
		
		for (int i = 0; i < MATRIX_SIZE; i++){
			self_values[i] = A_copy[i][i];
		}
		
		char str[100];
		sprintf(str, "%s(\'%d\') ", var_name, counter);
// 		qsort(self_values, MATRIX_SIZE, sizeof(long double), cmp);
		print_vector_to_matlab_code(file, str, self_values);
		counter++;
		
		printf("Lambdas = [");
		for (unsigned col = 0; col < MATRIX_SIZE; col++){
		printf("\"%.15Lf\"", A_copy[col][col]);
		if (col < MATRIX_SIZE-1)
				printf(",");
		}
		printf("]\n");
	}
	
	return counter;
}


int main(){
	srand(time(NULL));
	
	FILE* matlab_code = fopen("matrix/calculation_result.m", "w");

// 	for (int i = 0; i < TESTING_ITERATIONS; i++){
	{
		DataSerializationStruct result;

		char filename[100] = {'\0'};
		char str[100] = {0};
		char str2[100] = {0};
		
		
		sprintf(filename, "matrix/matrix_%d", 60);
		FILE* file = fopen(filename, "r");
		
		sprintf(str, "result_1");
		sprintf(str2, "result_2");
		
		init_values_from_file(file, &result);

		fclose(file);
		
		int iterations = find_self_values_my_jacobi_rotation_method_with_iterations(result.a_matrix, 1e-15, result.lambda_calculated, result.T, matlab_code, "iterations");
		
		qsort(result.lambda_calculated, MATRIX_SIZE, sizeof(long double), cmp);
		
		
// 		print_matrix(stdout, "Input matrix", result.a_matrix);
// 		putchar('\n');
		print_vector(stdout, "Input lambdas" ,result.lambda_vector);
		putchar('\n');
		print_vector(stdout, "Lambdas" ,result.lambda_calculated);
		putchar('\n');
// 		print_matrix(stdout, "Self vectors", T);
		
		print_data_to_matlab_code( matlab_code, str, &result);
		fprintf( matlab_code, "%s(\'iterations_amount\') = %d;\n\n", str, iterations);
		fprintf( matlab_code, "%s(\'iterations\') = iterations;\n\n", str);
		

// 		with small epsilon

		file = fopen(filename, "r");
		init_values_from_file(file, &result);
		iterations = find_self_values_my_jacobi_rotation_method_with_iterations(result.a_matrix, 1e-8, result.lambda_calculated, result.T, matlab_code, "iterations");
// 		
		qsort(result.lambda_calculated, MATRIX_SIZE, sizeof(long double), cmp);
// 		
// 		
// 		print_matrix(stdout, "Input matrix", result.a_matrix);
// 		putchar('\n');
		print_vector(stdout, "Input lambdas" ,result.lambda_vector);
		putchar('\n');
		print_vector(stdout, "Lambdas" ,result.lambda_calculated);
		putchar('\n');
// 		print_matrix(stdout, "Self vectors", T);
// 		
		print_data_to_matlab_code( matlab_code, str2, &result);
		fprintf( matlab_code, "%s(\"iterations_amount\") = %d;\n\n", str2, iterations);
		fprintf( matlab_code, "%s(\'iterations\') = iterations;\n\n", str2);
		fclose(file);
		
	}
	fprintf(matlab_code, "result_3 = containers.Map();\n");
	long double eps = 1;
	for (int i = 0; i < 15; i++, eps /= 10){
		DataSerializationStruct result;
		result.eps = eps;

		char filename[100] = {'\0'};
		char str[100] = {0};
		
		
		sprintf(filename, "matrix/matrix_%d", 98);
		FILE* file = fopen(filename, "r");
		
		sprintf(str, "result_3(\'%d\')", i);
		
		init_values_from_file(file, &result);

		int iterations = find_self_values_my_jacobi_rotation_method(result.a_matrix, eps, result.lambda_calculated, result.T);
		
		qsort(result.lambda_calculated, MATRIX_SIZE, sizeof(long double), cmp);
		qsort(result.lambda_vector, MATRIX_SIZE, sizeof(long double), cmp);
		
// 		print_matrix(stdout, "Input matrix", result.a_matrix);
// 		putchar('\n');
		print_vector(stdout, "Input lambdas" ,result.lambda_vector);
		putchar('\n');
		print_vector(stdout, "Lambdas" ,result.lambda_calculated);
		putchar('\n');
// 		print_matrix(stdout, "Self vectors", T);
		
		print_data_to_matlab_code( matlab_code, "map", &result);
		fprintf( matlab_code, "map(\'iterations_amount\') = %d;\n\n", iterations);
		fprintf( matlab_code, "%s = map;\n\n", str);
	}

	fclose( matlab_code );
	printf("END!!!\n");
}
