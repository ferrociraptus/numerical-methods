calculation_result;

%with epsilon e-8
iter = [];
errors = [];
map = result_1('iterations');
lambdas_calc = sort(result_1('L'));
for key = [0: (map.size(:,1) - 1)]
  key;
  iter(end+1) = key + 1;
  errors(end+1) = norm(lambdas_calc - sort(map(int2str(key))));
endfor

plot(iter, errors)
grid on
hold on

xlabel 'iteration'
ylabel 'fact error'
title 'Fact error to iteration eps'