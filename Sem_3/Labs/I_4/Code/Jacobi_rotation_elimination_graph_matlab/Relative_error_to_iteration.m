calculation_result;

iter = [];
errors = [];
map = result_1('iterations');
lambdas_calc = sort(result_1('L'));
for key = [0: (map.size(:,1) - 1)]
  key;
  iter(end+1) = key + 1;
  errors(end+1) = norm(lambdas_calc - sort(map(int2str(key))))/norm(lambdas_calc);
endfor
semilogy(iter, errors);
% plot(iter, errors);
grid on
xlabel 'iteration'
ylabel 'relative error '
title 'Fact error to iteration eps=e-8'
