calculation_result;

eps = [];
vals = [];
for key = [0: 15]
  map = result_3(int2str(key));
  key;
  eps(end+1) = map('eps');
  vals(end+1) = map('iterations_amount');
endfor

semilogx(eps, vals);
grid on
xlabel 'epsilon'
ylabel 'iterations'
title 'Iterations to eps'