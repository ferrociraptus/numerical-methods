calculation_result;

%result_1 with epsilon e-8
%result_2 with epsilon e-16

iter = [];
errors = [];
for key = [0: 15]
  map = result_3(int2str(key));
  iter(end+1) = map('eps');
  lambdas_calc = sort(map('L'));
  errors(end+1) = norm(lambdas_calc - sort(map('L_calculated')));
endfor

loglog(iter, errors)
grid on

xlabel 'epsilon'
ylabel 'fact error'
title 'Fact error to epsilon'