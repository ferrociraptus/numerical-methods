 for val = [0:99]
  A=randn(10, 10);  
  R=orth( A.' ).'; % orthogonal rows
  O=bsxfun(@rdivide, R, sqrt( sum( R.^2, 2 ) ) ).*10;
  
  lambdas = ((val+1)/60 + 1)*[1:10];
  lambdas_diag = diag(lambdas);
  
  A = inverse(O)*lambdas_diag*O
  
  filename = strcat('matrix/matrix_', int2str(val));
  file = fopen(filename, 'w');
  for i=1:size(A, 1)
      fprintf(file, '%f ', A(i,:));
      fprintf(file, '\n');
  end
  fprintf(file, '\n');
  fprintf(file, '%f ', lambdas);
  fprintf(file, '\n');
  fclose(file);
endfor