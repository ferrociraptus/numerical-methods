 for val = [0:99]
  val
  lambdas = ((val+1)/60 + 1)*[1:10];
  lambdas_diag = diag(lambdas);
  A = lambdas_diag
  for i = [0:ceil(rand()*100 + 10)]
    i = ceil(mod(rand()*10, 9) + 1);
    j = i;
    while j == i
      j = ceil(mod(rand()*10, 9) + 1);
    endwhile
    c = rand()
    s = 1 - c^2
    T = eye()
    T(i,i) = c
    T(j,j) = c
    T(i,j) = s
    T(j,i) = -s
    A = T.'*A*T
  endfor
  
  filename = strcat('matrix/matrix_', int2str(val));
  file = fopen(filename, 'w');
  for i=1:size(A, 1)
      fprintf(file, '%f ', A(i,:));
      fprintf(file, '\n');
  end
  fprintf(file, '\n');
  fprintf(file, '%f ', lambdas);
  fprintf(file, '\n');
  fclose(file);
endfor