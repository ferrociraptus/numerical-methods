calculation_result;

eps = [];
errors = [];
for key = [0: 15]
  map = result_3(int2str(key));
  [EigVec, D, EigVal] = eig(map('A'))
  key;
  eps(end+1) = map('eps');
%  sort(EigVec)
   vec = map('T')
   l = map('L')
%  errors(end+1) = norm(sort(EigVec) - sort(transpose(map('T'))));
  errors(end+1) = norm(EigVec.'*map('A') - D*EigVec.');
endfor

semilogx(eps, errors);
grid on
xlabel 'epsilon'
ylabel 'relative error '
title 'Fact eig vector error to eps'