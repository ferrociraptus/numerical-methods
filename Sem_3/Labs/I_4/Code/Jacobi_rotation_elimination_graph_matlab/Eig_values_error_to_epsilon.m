calculation_result;

eps = [];
errors = [];
for key = [0: 15]
  map = result_3(int2str(key));
  key;
  eps(end+1) = map('eps');
  errors(end+1) = norm(sort(map('L')) - sort(map('L_calculated')));
endfor

loglog(eps, errors);
grid on
xlabel 'epsilon'
ylabel 'relative error '
title 'Fact eig value error to eps'