calculation_result;

eps = [];
vals = [];
for key = [0: 15]
  map = result_3(int2str(key));
  key
  eps(end+1) = map('eps');
  eigVec = transpose(map('T'));
  eigVal = diag(map('L_calculated'));
  res = 0;
  a = eigVec*map('A')
  b = eigVal*eigVec
  c = a-b
  norm(a-b)
  %vals(end + 1) = norm(eigVec*map('A') - eigVal*eigVec);
  vals(end + 1) = norm(c);
endfor

semilogx(eps, vals);
grid on
xlabel 'epsilon'
ylabel 'relative error '
title 'Fact eig vector error to eps'