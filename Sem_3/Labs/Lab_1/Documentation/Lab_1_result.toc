\babel@toc {russian}{}
\contentsline {section}{\numberline {1}Формулировка задачи}{2}%
\contentsline {section}{\numberline {2}Алгоритм метода и условие его применимости}{2}%
\contentsline {subsection}{\numberline {2.1}Метод половинного деления}{2}%
\contentsline {subsubsection}{\numberline {2.1.1}Условие применимости}{2}%
\contentsline {subsubsection}{\numberline {2.1.2}Алгоритм}{2}%
\contentsline {subsection}{\numberline {2.2}Метод Хорд и Ньютона}{2}%
\contentsline {subsubsection}{\numberline {2.2.1}Условие применимости}{2}%
\contentsline {subsubsection}{\numberline {2.2.2}Алгоритм}{2}%
\contentsline {section}{\numberline {3}Предварительный анализ задачи}{3}%
\contentsline {subsection}{\numberline {3.1}Алгебраическое уравнение}{3}%
\contentsline {subsection}{\numberline {3.2}Трансцендентное уравнение}{6}%
\contentsline {section}{\numberline {4}Проверка условий применимости}{7}%
\contentsline {subsection}{\numberline {4.1}Алгебраическое уравнение}{7}%
\contentsline {subsubsection}{\numberline {4.1.1}Проверка условий для метода половинного деления}{7}%
\contentsline {subsubsection}{\numberline {4.1.2}Проверка условий для метода Хорд и Ньютона}{7}%
\contentsline {subsection}{\numberline {4.2}Трансцендентное уравнение}{8}%
\contentsline {subsubsection}{\numberline {4.2.1}Проверка условий для метода половинного деления}{8}%
\contentsline {subsubsection}{\numberline {4.2.2}Проверка условий для метода Хорд и Ньютона}{8}%
\contentsline {section}{\numberline {5}Тестовый пример}{9}%
\contentsline {subsection}{\numberline {5.1}Метод половинного деления}{9}%
\contentsline {subsection}{\numberline {5.2}Метод Хорд и Ньютона}{10}%
\contentsline {section}{\numberline {6}Контрольные тесты}{10}%
\contentsline {section}{\numberline {7}Вычисляющая программа}{11}%
\contentsline {subsection}{\numberline {7.1}Метод половинного деления}{11}%
\contentsline {subsection}{\numberline {7.2}Метод хорд и Ньютона}{12}%
\contentsline {section}{\numberline {8}Численный анализ}{13}%
\contentsline {section}{\numberline {9}Выводы}{15}%
