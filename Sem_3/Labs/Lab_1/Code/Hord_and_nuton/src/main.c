#include <stdio.h>
#include <math.h>

double chord(double (*fun)(double), double x0, double x1, double epsilon) {
	unsigned int counter = 0;
	while (fabs(x1 - x0) > epsilon) {
		x0 = x1 - (x1 - x0) * fun(x1) / (fun(x1) - fun(x0));
		x1 = x0 - (x0 - x1) * fun(x0) / (fun(x0) - fun(x1));
		counter++;
	}
	printf("Chord operations amount: %d\n", counter);

	return x1;
}

double newton(double(*fun)(double), double(*df)(double), double xn, double eps) {
	double x1  = xn - fun(xn) / df(xn);
	double x0 = xn;

	while (fabs(x0 - x1) > eps) {
		x0 = x1;
		x1 = x1 - fun(x1) / df(x1);
	}
	
	return x1;
}

/*
 Вычисляет значение полинома
 @param x аргумент функции 
 @return значение функции
*/
double polynom_fun(double x) {
    return 3.0 * pow(x, 4) + 4.0 * pow(x, 3) + 12.0 * pow(x, 2) - 5.0;
}

/*
 Вычисляет значение первой производной полинома
 @param x аргумент функции 
 @return значение функции
*/
double polynom_fun_derivative(double x) {
    return 12.0 * pow(x, 3) + 12.0 * pow(x, 2) + 24.0 * x;
}

/*
 Вычисляет значение трансцендентной функции
 @param x аргумент функции 
 @return значение функции
*/
double ln_fun(double x){
    return log(x) + pow((x + 1), 3);
}

/*
 Вычисляет значение первой производной трансцендентной функции
 @param x аргумент функции 
 @return значение функции
*/
double ln_fun_derivative(double x){
    return 1.0/x + 3*pow((1+x), 2);
}

/*
 Вычисляет значение машинного эпсилон
 @return значение эпсилон
*/
double eps() {
    double x = 0.2e-10;

    while ((1 + x) != 1)
	x /= 2;

    return x;
}

/*
 Вычисляет значение корня функции с заданной точностью методом половинного деления
 @param fun функция
 @param x0 начальное значение интервала поиска
 @param x0 конечное значение интервала поиска
 @return корень функции
*/
double bisection(double(*fun)(double), double x0, double x1, double eps) {
    while (fabs(x1 - x0) > eps) {
		double c = (x0 + x1) / 2;
		
		if (fun(x0)*fun(c) < 0)
			x1 = c;
		else
			x0 = c;
		
		printf("bisection: %.40lf\n", (x0 + x1) / 2);
    }
    return (x0 + x1) / 2;
}

/*
 Вычисляет значение корня функции с заданной точностью методом Хорд и Ньютона
 @param fun функция
 @param x0 начальное значение интервала поиска
 @param x0 конечное значение интервала поиска
 @return корень функции
*/
double chord_and_newton(double(*fun)(double), double(*df)(double), double x0, double x1, double eps) {
    int trigger = 1;

    while (fabs(x0 - x1) > eps) {
		if (trigger) {
			x1 = x0 - (x0 - x1) * fun(x0) / (fun(x0) - fun(x1));
			trigger = !trigger;
		}
		else {
			x0 = x1;
			x1 = x1 - fun(x1) / df(x1);
			trigger = !trigger;
		}
		printf("chord_and_newton: %.40lf\n", x1);
    }
    return x1;
}

int main() {
//     printf("Chord and newton polynom result:\t%.50lf\n", chord_and_newton(polynom_fun, polynom_fun_derivative, 0.1, 1, 1.0e-17));
//     printf("Chord and newton transedintal result:\t%.50lf\n", chord_and_newton(ln_fun, ln_fun_derivative, 0.1, 1, 1.0e-17));
// 	printf("\n");
	freopen("result_2", "w", stdout);
	
	printf("@ polynomial\n");
	chord_and_newton(polynom_fun, polynom_fun_derivative, 1, 0.1, 1.0e-15);
	bisection(polynom_fun, 0.1, 1, 1.0e-15);
//     printf("Bisection polynom result:%.15lf\n", bisection(polynom_fun, 0.1, 1, 1.0e-15));
// 	bisection(polynom_fun, 0.1, 1, 1.0e-15);
	printf("@ transedintal\n");
	chord_and_newton(ln_fun, ln_fun_derivative, 1, 0.1, 1.0e-15);
//     printf("Bisection transedintal result:%.15lf\n", bisection(ln_fun, 0.1, 1, 1.0e-15));
// 	bisection(ln_fun, 0.1, 1, 1.0e-15);
	bisection(ln_fun, 0.1, 1, 1.0e-15);
	
	freopen("result", "w", stdout);
	
	double epsilon = 0.1;
	for (int i = 0; i <= 12; i ++){
		printf("\nepsilon: %.40lf\n", epsilon);
		printf("@ polynomial\n");
		chord_and_newton(polynom_fun, polynom_fun_derivative, 0, 1, epsilon);
		bisection(polynom_fun, 0.1, 1, epsilon);
		
		printf("@ transedintal\n");
		chord_and_newton(ln_fun, ln_fun_derivative, 0.1, 1, epsilon);
		bisection(ln_fun, 0.1, 1, epsilon);
		epsilon /= 10;
	}
}

