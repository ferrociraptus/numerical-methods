from matplotlib import pyplot as plt

from sympy.solvers import solve
from scipy.optimize import fsolve
import sympy as sp
import numpy as np

x = sp.Symbol('x', real=True)
poly_arr = np.arange(0, 1, 0.05)
fun_arr = np.arange(0.35, 1.5, 0.05)

poly = lambda x: 3.0 * x ** 4 + 4.0 * x ** 3 + 12.0 * x ** 2 - 5.0
poly_first_derivative = lambda x: 12.0 * x ** 3 + 12 * x ** 2 + 24*x
poly_second_derivative = lambda x: 36 * x ** 2 + 24 * x + 24

fun = lambda x: np.log(x) + (x + 1) ** 3
fun_first_derivative = lambda x: 1 / x + 3 * ((x + 1) ** 2)
fun_second_derivative = lambda x: -1 / (x ** 2) + 6 * (x + 1)
func_np = sp.lambdify(x, sp.log(x) + (x + 1) ** 3, modules=['numpy'])

poly_solving = solve(4.0 * x ** 4 + 4.0 * x ** 3 + 12.0 * x ** 2 - 5.0, x)
fun_solving = fsolve(func_np, 0.5)
print(f"Polynomial solving: {poly_solving}")
print(f"Function solving: {fun_solving}")

figure, ax = plt.subplots()

# ax.plot(poly_arr, poly(poly_arr), linewidth=2, label="3x^4 + 4x^3 + 12x^2 - 5")
# ax.plot(fun_arr, fun(fun_arr), color=(0, 0.4, 0), linewidth=2, label="ln(x) + (x+1)^3")
# print(poly_solving)
# ax.plot(poly_solving, np.zeros(len(poly_solving)), '*')
# ax.plot(fun_solving, np.zeros(len(fun_solving)), 'd')

# ax.set_facecolor((0, 0, 0))

# ax.plot(fun_arr, fun_first_derivative(fun_arr), color=(0, 0.4, 0), linewidth=2, label="f'(x)")
# ax.plot(fun_arr, fun_second_derivative(fun_arr), color=(0.5, 0.4, 0), linewidth=2, label="f''(x)")
print_solve_points = lambda solving: [ax.text(solv, 0, f"({round(solv, 3)},0)") for solv in solving]
#
ax.plot(poly_arr, poly_first_derivative(poly_arr), linewidth=2, label="f'(x)")
ax.plot(poly_arr, poly_second_derivative(poly_arr), linewidth=2, label="f''(x)")
ax.grid(True)
ax.legend(loc="upper right")
# print_solve_points(poly_solving)
# print_solve_points(fun_solving)
# ax.text(-0.671, 0, "<>", horizontalalignment="center", verticalalignment="center")

# name = input()
# if name != "no":
#     fig = plt.gcf()
#     ax.legend(loc="upper right")
#     ax.grid(True)
#     fig.savefig(name, dpi=250)
# ax.set(title='About as simple as it gets, folks')
plt.show()
