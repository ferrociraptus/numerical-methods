import numpy as np
import scipy as sci


def hilbert(n):
    x = np.arange(1, n + 1) + np.arange(0, n)[:, np.newaxis]
    return 1.0 / x


n = 10
a = np.random.random((n, n))
a = hilbert(n)
e_ex = np.ones((n, 1))
b = a.dot(e_ex)
x = a.dot(1 / b)
normal = np.abs(x - e_ex) / np.abs(x)
err = np.linalg.norm(x - e_ex) / np.linalg.norm(e_ex)

bv = 0.01 * np.random.random((n, 1))
x2 = a.dot(1 / (b + bv))

norm = np.linalg.norm(x - x2) / np.linalg.norm(x)
norm = np.linalg.norm(bv) / np.linalg.norm(b)

cond = np.linalg.cond(a)

cond_val = 54
arr = list(range(1, 101))
arr[-1] = cond_val+1
d = np.diag(np.array(arr))
# print(d.shape)
q, r = np.linalg.qr(np.random.random(d.shape))

ans = np.linalg.cond(q @ d @ q.T)
print(ans)


