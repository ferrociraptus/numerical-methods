import numpy as np
import sympy as sp
from matplotlib import pyplot as plt
from scipy.optimize import fsolve
from sympy.solvers import solve
from decimal import *

stdin = open('result_2', 'r')
input_lines = stdin.readlines()

x = sp.Symbol('x', real=True)
poly = lambda x: 3.0 * x ** 4 + 4.0 * x ** 3 + 12.0 * x ** 2 - 5.0
fun = lambda x: np.log(x) + (x + 1) ** 3
func_np = sp.lambdify(x, sp.log(x) + (x + 1) ** 3, modules=['numpy'])
poly_solving = solve(4.0 * x ** 4 + 4.0 * x ** 3 + 12.0 * x ** 2 - 5.0, x)
fun_solving = fsolve(func_np, 0.5)

x = sp.Symbol('x', real=True)
poly_arr = np.arange(-1, 1, 0.05)
fun_arr = np.arange(np.finfo(np.float32).eps, 1.5, 0.05)

poly = lambda x: 3.0 * x ** 4 + 4.0 * x ** 3 + 12.0 * x ** 2 - 5.0
fun = lambda x: np.log(x) + (x + 1) ** 3
func_np = sp.lambdify(x, sp.log(x) + (x + 1) ** 3, modules=['numpy'])
fun_abs_solve = Decimal("0.18743892873054832093338006870908429846167564392090")

poly_solving = solve(3.0 * x ** 4 + 4.0 * x ** 3 + 12.0 * x ** 2 - 5.0, x)
fun_solving = fsolve(func_np, 0.5)
poly_abs_solve = Decimal("0.57220331032003013405073943431489169597625732421875")

results = {}
postfix = ""
for line in input_lines:
    try:
        if "@" in line:
            postfix = line[1:]
        key, val = line.split(":")
        key += postfix
        if results.get(key, False):
            results[key].append(Decimal(val))
        else:
            results[key] = [Decimal(val)]
    except:
        pass


# results = {"epsilon": []}
# postfix = ""
# for line in input_lines:
#     try:
#         line = line.strip()
#         if "@" in line:
#             postfix = line[1:]
#             continue
#         key, val = line.split(":")
#         # val = float(val)
#         val = Decimal(val)
#         if "epsilon" in line:
#             for key in results.keys():
#                 if key != "epsilon":
#                     results[key].append([])
#             results["epsilon"].append(val)
#             continue
#
#         key += postfix
#         if results.get(key, False):
#             results[key][-1].append(val)
#         else:
#             results[key] = [[val]]
#     except:
#         pass

figure, ax = plt.subplots()
for key, val in results.items():
    print(f"Key: {key}")
    if "polynomial" in key:
        ax.plot(list(range(len(val))), list(map(lambda x: abs(x - Decimal(str(max(poly_solving)))), val)),
                marker='o', linewidth=2, markersize=3, label=key)
    if "transedintal" in key:
        ax.plot(list(range(len(val))), list(map(lambda x: abs(x - Decimal(str(max(fun_solving)))), val)),
                marker='o', linewidth=2, markersize=3, label=key)
ax.set_yscale('log')
# ax.set_yscale('symlog', linthresh=0.4, linscale=2)
# ax.set_xscale('symlog', linthresh=10, linscale=4)
fig = plt.gcf()
# fig.set_size_inches(18.5, 10.5)

# plt.ylim((-0.8, -0.6))
ax.legend(loc="upper right")
ax.grid(True)
# fig.set_size_inches(8, 5.5)
# fig.savefig('absolute_err.png', dpi=100)


# stdin = open('result', 'r')
# input_lines = stdin.readlines()
stdin = open('result','r')
input_lines = stdin.readlines()
results = {"epsilon": []}
postfix = ""
for line in input_lines:
    try:
        line = line.strip()
        if "@" in line:
            postfix = line[1:]
            continue
        key, val = line.split(":")
        # val = float(val)
        val = Decimal(val)
        if "epsilon" in line:
            for key in results.keys():
                if key != "epsilon":
                    results[key].append([])
            results["epsilon"].append(val)
            continue

        key += postfix
        if results.get(key, False):
            results[key][-1].append(val)
        else:
            results[key] = [[val]]
    except:
        pass

figure, ax = plt.subplots()
for key, val in results.items():
    print(f"Key: {key}")
    if "epsilon" not in key:
        ax.plot(results["epsilon"], [len(res) for res in val],
                marker='o', linewidth=2, markersize=3, label=key)
ax.set_xscale('log')
# ax.set_yscale('symlog', linthresh=0.4, linscale=2)
# ax.set_xscale('symlog', linthresh=1E-14, linscale=1)
fig = plt.gcf()
# fig.set_size_inches(18.5, 10.5)

ax.legend(loc="upper right")
ax.grid(True)
# plt.ylim((-0.8, -0.6))
fig.set_size_inches(8, 5.5)
fig.savefig('epsilon_iterations_dependency.png', dpi=100)


print(results["epsilon"])
# absolute pogr from epsilon
figure, ax = plt.subplots()
for key, val in results.items():
    # print(f"Key: {key}: {val}")
    if "polynomial" in key:
        ax.plot(results["epsilon"], [abs(res[-1]-Decimal(str(poly_solving[-1]))) for res in val],
                linewidth=2, markersize=3, marker="o", label=key)
        print("values",[res[-1]-Decimal(str(poly_solving[-1])) for res in val])
        # ax.plot(results["epsilon"], [res[-1] - poly_solving[-1] for res in val],
        #         marker='o', linewidth=2, markersize=3, label=key)
    elif "transedintal" in key:
        ax.plot(results["epsilon"], [abs(res[-1]-Decimal(str(fun_solving[-1]))) for res in val],
                linewidth=2, markersize=3, label=key, marker="o")

        # ax.plot(results["epsilon"], [res[-1] - fun_solving[-1] for res in val],
        #         marker='o', linewidth=2, markersize=3, label=key)
ax.plot(results["epsilon"], results["epsilon"], "--", linewidth=2, label="bisector")
ax.set_yscale('log')
ax.set_xscale('log')
# ax.set_yscale('symlog', linthresh=0.4, linscale=2)
# ax.set_xscale('symlog', linthresh=1E-3, linscale=5)
# plt.ylim((0, 0.00005))
fig = plt.gcf()
# fig.set_size_inches(18.5, 10.5)
fig.set_size_inches(8, 5.5)
ax.legend(loc="upper right")
ax.grid(True)
fig.savefig('absolute_error_from_epsilon_dependency.png', dpi=100)


plt.show()
