\babel@toc {russian}{}
\contentsline {section}{\numberline {1}Формулировка задачи}{2}{}%
\contentsline {section}{\numberline {2}Алгоритм метода Якоби и условие его применимости}{2}{}%
\contentsline {subsection}{\numberline {2.1}Условие применимости}{2}{}%
\contentsline {subsection}{\numberline {2.2}Алгоритм}{2}{}%
\contentsline {section}{\numberline {3}Предварительный анализ задачи}{3}{}%
\contentsline {section}{\numberline {4}Проверка условий применимости}{3}{}%
\contentsline {section}{\numberline {5}Тестовый пример применения метода Якоби}{3}{}%
\contentsline {section}{\numberline {6}Контрольные тесты}{3}{}%
\contentsline {section}{\numberline {7}Вычисляющая программа}{4}{}%
\contentsline {section}{\numberline {8}Численный анализ метода Якоби}{5}{}%
\contentsline {section}{\numberline {9}Вывод}{8}{}%
