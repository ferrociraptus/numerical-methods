from matplotlib import pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
from matrix.calculation_result_1 import *
from decimal import *


def fact_error_to_accuracy_plot():
    fig, ax = plt.subplots()

    ax.plot([10 ** -i for i in range(17)],
            [np.linalg.norm(res["X_calculated"] - res["X"]) for res in result_1],
            label="Gradient elimination method", linewidth=2)

    ax.grid(True)
    ax.set_xscale('log')
    ax.set_yscale('log')

    plt.xlabel(r"Accuracy")
    plt.ylabel("Fact error")
    plt.title("Fact error to accuracy")

    ax.legend(loc="upper right")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig('Fact_error_to_accuracy.png', dpi=100)


def discrepancy_norm_to_accuracy_plot():
    fig, ax = plt.subplots()

    ax.plot([10 ** -i for i in range(17)],
            [np.linalg.norm(res["A"] @ res["X_calculated"] - res["B"]) for res in result_1],
            label="Gradient elimination method", linewidth=2)

    ax.grid(True)
    ax.set_xscale('log')
    ax.set_yscale('log')

    plt.xlabel(r"Accuracy")
    plt.ylabel("Discrepancy norm")
    plt.title("Discrepancy norm to accuracy")

    ax.legend(loc="upper right")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig('Discrepancy_norm_to_accuracy.png', dpi=100)


def iterations_amount_to_accuracy_plot():
    fig, ax = plt.subplots()

    ax.plot([10 ** -i for i in range(17)],
            [res["iterations_amount"] for res in result_1],
            label="Gradient elimination method", linewidth=2)

    ax.grid(True)
    ax.set_xscale('log')
    # ax.set_yscale('log')

    plt.xlabel(r"Accuracy")
    plt.ylabel("Iterations amout")
    plt.title("Iterations amount to accuracy")

    ax.legend(loc="upper right")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig('Iterations_amount_to_accuracy.png', dpi=100)


discrepancy_norm_to_accuracy_plot()
fact_error_to_accuracy_plot()
iterations_amount_to_accuracy_plot()
plt.show()
