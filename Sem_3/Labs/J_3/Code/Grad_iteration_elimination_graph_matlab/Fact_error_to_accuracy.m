calculation_result;


accuracy = [];
errors = [];
for key = [0:11]
  key
  map = result_1(int2str(key));
  accuracy(end+1) = map('eps');
  map('X_calculated') 
  map('X')
  errors(end+1) = norm(map('X')-map('X_calculated'));
endfor

loglog(accuracy, errors)
grid on
xlabel 'accuracy'
ylabel 'fact error'
title 'Fact error to accuracy'