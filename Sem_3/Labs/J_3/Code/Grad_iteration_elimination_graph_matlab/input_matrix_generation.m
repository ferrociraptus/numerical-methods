

for val = [0:50]
  A=randn(10, 10).+1;  
  
  A = A*A.'
  filename = strcat('matrix/matrix_', int2str(val));
  file = fopen(filename, 'w');
  for i=1:size(A, 1)
      fprintf(file, '%f ', A(i,:));
      fprintf(file, '\n');
  end
  fprintf(file, '%f', cond(A));
  fclose(file);
endfor