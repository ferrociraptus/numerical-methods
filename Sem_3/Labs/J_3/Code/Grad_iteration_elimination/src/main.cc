#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <ctype.h>

#define MATRIX_SIZE 10
#define TESTING_ITERATIONS 15
#define ITERATIONS 1000
#define EPS 1e-16

typedef struct{
	double** matrix;
	unsigned size;
} MultysizeMatrix;

typedef struct{
	double a_matrix[MATRIX_SIZE][MATRIX_SIZE];
	double x_vector[MATRIX_SIZE];
	double x_calculated[MATRIX_SIZE];
	double b_vector[MATRIX_SIZE];
// 	double err_vector[MATRIX_SIZE];
	double eps;
}DataSerializationStruct;

typedef struct{
	MultysizeMatrix* a_matrix;
	double* x_vector;
	double* x_calculated;
	double* b_vector;
	double* err_vector;
	double cond_val;
}MultisizeDataSerializationStruct;

void fill_matrix(double matrix[MATRIX_SIZE][MATRIX_SIZE], int range_min, int range_max){
	for (int i = 0; i < MATRIX_SIZE; i ++)
		for (int k = 0; k < MATRIX_SIZE; k++)
			matrix[i][k] = rand() % (range_max + range_min) - range_min;
}

void fill_vector(double* vector, int range_min, int range_max){
	int range = abs(range_max) + abs(range_min);
	range_min = abs(range_min);
	for (int i = 0; i < MATRIX_SIZE; i ++){
		double rand_coef = (double)rand() / RAND_MAX;
		vector[i] = (rand() % range)+rand_coef - range_min;
	}
}

void multisize_fill_vector(double* vector, int size, int range_min, int range_max){
	int range = abs(range_max) + abs(range_min);
	range_min = abs(range_min);
	for (int i = 0; i < size; i ++){
		double rand_coef = (double)rand() / RAND_MAX;
		vector[i] = (rand() % 100);
	}
}

double* dot_matrix_with_vector(double matrix[MATRIX_SIZE][MATRIX_SIZE], double vector[MATRIX_SIZE]){
	double* result = (double*) malloc(sizeof(double) * MATRIX_SIZE);
	memset(result, 0, sizeof(double)*MATRIX_SIZE);
	
	for (int row = 0; row < MATRIX_SIZE; row++)
		for (int col = 0; col < MATRIX_SIZE; col++)
			result[row] += vector[col] * matrix[row][col];
		
	return result;
}

double* dot_matrix_with_vector_save(double* result, double matrix[MATRIX_SIZE][MATRIX_SIZE], double vector[MATRIX_SIZE]){
	memset(result, 0, sizeof(double) * MATRIX_SIZE);
	for (int row = 0; row < MATRIX_SIZE; row++)
		for (int col = 0; col < MATRIX_SIZE; col++)
			result[row] += vector[col] * matrix[row][col];
		
	return result;
}

double* multisize_dot_matrix_with_vector_save(double* result, MultysizeMatrix* matrix, double* vector){
	memset(result, 0, sizeof(double) * matrix->size);
	for (int row = 0; row < matrix->size; row++)
		for (int col = 0; col < matrix->size; col++)
			result[row] += vector[col] * matrix->matrix[row][col];
		
	return result;
}

void init_printing_to_python_code(FILE* stream){
	fprintf(stream, "from decimal import *\n");
	fprintf(stream, "import numpy as np\n");
}

void print_matrix_to_python_code(FILE* stream, const char* field_name, double matrix[MATRIX_SIZE][MATRIX_SIZE]){
	fprintf(stream, "%s = np.array([", field_name);
	for (unsigned row = 0; row < MATRIX_SIZE; row++){
		fprintf(stream, "[");
		for (unsigned col = 0; col < MATRIX_SIZE-1; col++)
			fprintf(stream, "Decimal(\"%.20lf\"),", matrix[row][col]);
		fprintf(stream, "Decimal(\"%.20lf\")", matrix[row][MATRIX_SIZE - 1]);
		fprintf(stream, "]");
		
		if (row < MATRIX_SIZE-1)
			fprintf(stream, ",");
	}
	fprintf(stream, "], dtype=np.dtype(Decimal))\n");
}

void print_matrix_to_matlab_code(FILE* stream, const char* field_name, double matrix[MATRIX_SIZE][MATRIX_SIZE]){
	fprintf(stream, "%s = [", field_name);
	for (unsigned row = 0; row < MATRIX_SIZE; row++){
		fprintf(stream, "");
		for (unsigned col = 0; col < MATRIX_SIZE-1; col++)
			fprintf(stream, "%.30lf ", matrix[row][col]);
		fprintf(stream, "%.30lf", matrix[row][MATRIX_SIZE - 1]);
		fprintf(stream, "");
		
		if (row < MATRIX_SIZE-1)
			fprintf(stream, ";");
	}
	fprintf(stream, "]\n");
}

void multisize_print_matrix_to_python_code(FILE* stream, const char* field_name, MultysizeMatrix* matrix){
	fprintf(stream, "%s = np.array([", field_name);
	for (unsigned row = 0; row < matrix->size; row++){
		fprintf(stream, "[");
		for (unsigned col = 0; col < matrix->size-1; col++)
			fprintf(stream, "Decimal(\"%.20lf\"),", matrix->matrix[row][col]);
		fprintf(stream, "Decimal(\"%.20lf\")", matrix->matrix[row][matrix->size - 1]);
		fprintf(stream, "]");
		
		if (row < matrix->size - 1)
			fprintf(stream, ",");
	}
	fprintf(stream, "], dtype=np.dtype(Decimal))\n");
}

void print_vector_to_python_code(FILE* stream, const char* field_name, double vector[MATRIX_SIZE]){
	fprintf(stream, "%s = np.array([", field_name);
	for (unsigned col = 0; col < MATRIX_SIZE; col++){
		fprintf(stream, "Decimal(\"%.20lf\")", vector[col]);
		if (col < MATRIX_SIZE-1)
			fprintf(stream, ",");
	}
	fprintf(stream, "], dtype=np.dtype(Decimal))\n");
}

void print_vector_to_matlab_code(FILE* stream, const char* field_name, double vector[MATRIX_SIZE]){
	fprintf(stream, "%s = [", field_name);
	for (unsigned col = 0; col < MATRIX_SIZE; col++){
		fprintf(stream, "%.30lf", vector[col]);
		if (col < MATRIX_SIZE-1)
			fprintf(stream, " ");
	}
	fprintf(stream, "]\n");
}

void multisize_print_vector_to_python_code(FILE* stream, const char* field_name, double* vector, int size){
	fprintf(stream, "%s = np.array([", field_name);
	for (unsigned col = 0; col < size-1; col++){
		fprintf(stream, "Decimal(\"%.20lf\")", vector[col]);
		if (col < size-1)
			fprintf(stream, ",");
	}
	fprintf(stream, "], dtype=np.dtype(Decimal))\n");
}

void print_vector(FILE* stream, const char* field_name, double vector[MATRIX_SIZE]){
	fprintf(stream, "%s = [", field_name);
	for (unsigned col = 0; col < MATRIX_SIZE-1; col++){
		fprintf(stream, "\"%.10lf\"", vector[col]);
		if (col < MATRIX_SIZE-1)
			fprintf(stream, ",");
	}
	fprintf(stream, "]\n");
}

void print_data_to_python_code(FILE* stream, const char* field_name, DataSerializationStruct* str){
	char buf[100] = {0};
	fprintf(stream, "%s = {}\n", field_name);
	
	sprintf(buf, "%s[\"A\"]", field_name);
	print_matrix_to_python_code(stream, buf, str->a_matrix );
	
	sprintf(buf, "%s[\"B\"]", field_name);
	print_vector_to_python_code(stream, buf, str->b_vector);
	
	sprintf(buf, "%s[\"X\"]", field_name);
	print_vector_to_python_code(stream, buf, str->x_vector);
	
	sprintf(buf, "%s[\"X_calculated\"]", field_name);
	print_vector_to_python_code(stream, buf, str->x_calculated);
	
// 	sprintf(buf, "%s[\"err\"]", field_name);
// 	print_vector_to_python_code(stream, buf, str->err_vector);
	
	fprintf(stream, "%s[\"condition_number\"] = %.20lf\n", field_name, str->eps);
}

void print_data_to_matlab_code(FILE* stream, const char* field_name, DataSerializationStruct* str){
	char buf[100] = {0};
	fprintf(stream, "%s = containers.Map()\n", field_name);
	
	sprintf(buf, "%s(\'A\')", field_name);
	print_matrix_to_matlab_code(stream, buf, str->a_matrix );
	
	sprintf(buf, "%s(\'B\')", field_name);
	print_vector_to_matlab_code(stream, buf, str->b_vector);
	
	sprintf(buf, "%s(\'X\')", field_name);
	print_vector_to_matlab_code(stream, buf, str->x_vector);
	
	sprintf(buf, "%s(\'X_calculated\')", field_name);
	print_vector_to_matlab_code(stream, buf, str->x_calculated);
	
// 	sprintf(buf, "%s(\'err\')", field_name);
// 	print_vector_to_matlab_code(stream, buf, str->err_vector);
	
	fprintf(stream, "%s(\'eps\') = %.20lf\n", field_name, str->eps);
}

void multisize_print_data_to_python_code(FILE* stream, const char* field_name, MultisizeDataSerializationStruct* str){
	char buf[100] = {0};
	fprintf(stream, "%s = {}\n", field_name);
	
	sprintf(buf, "%s[\"A\"]", field_name);
	multisize_print_matrix_to_python_code(stream, buf, str->a_matrix );
	
	sprintf(buf, "%s[\"B\"]", field_name);
	multisize_print_vector_to_python_code(stream, buf, str->b_vector, str->a_matrix->size);
	
	sprintf(buf, "%s[\"X\"]", field_name);
	multisize_print_vector_to_python_code(stream, buf, str->x_vector, str->a_matrix->size);
	
	sprintf(buf, "%s[\"X_calculated\"]", field_name);
	multisize_print_vector_to_python_code(stream, buf, str->x_calculated, str->a_matrix->size);
	
	sprintf(buf, "%s[\"err\"]", field_name);
	multisize_print_vector_to_python_code(stream, buf, str->err_vector, str->a_matrix->size);
	
	fprintf(stream, "%s[\"eps\"] = %.20lf\n", field_name, str->cond_val);
}

void add_value_to_vector(double vector[MATRIX_SIZE], double val){
	for (unsigned i = 0; i < MATRIX_SIZE; i++)
		vector[i] += val;
}

void multisize_add_value_to_vector(double* vector, int size, double val){
	for (unsigned i = 0; i < size; i++)
		vector[i] += val;
}

void matrix_from_file(FILE* file, double matrix[MATRIX_SIZE][MATRIX_SIZE]){

	char ch_buf = getc(file);
	while(!isdigit(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
	ungetc(ch_buf, file);
	
	
	for (unsigned row = 0; row < MATRIX_SIZE; row++){
		for (unsigned col = 0; col < MATRIX_SIZE; col++){
			double val;
			fscanf(file, "%lf", &val);
			matrix[row][col] = val;
			
			ch_buf = getc(file);
			while(isspace(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
// 			while((isdigit(ch_buf) || ch_buf == '.' || ch_buf == '-') && ch_buf != EOF) ch_buf = getc(file);
			while((!isdigit(ch_buf) && ch_buf != '-') && ch_buf != EOF) ch_buf = getc(file);
			ungetc(ch_buf, file);
		}
	}
}

MultysizeMatrix* multisize_matrix_from_file(FILE* file){
	MultysizeMatrix* matrix = malloc(sizeof(MultysizeMatrix));
	
	
	fscanf(file, "%d", &matrix->size);
	
	matrix->matrix = malloc(sizeof(double*) * matrix->size);

	
	for (int i = 0; i < matrix->size; i++)
		matrix->matrix[i] = malloc(sizeof(double) * matrix->size);
	
	char ch_buf = getc(file);
	while(!isdigit(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
	ungetc(ch_buf, file);
	
	for (unsigned row = 0; row < matrix->size; row++){
		for (unsigned col = 0; col < matrix->size; col++){
			double val;
			fscanf(file, "%lf", &val);
			matrix->matrix[row][col] = val;
			
			ch_buf = getc(file);
			while(isspace(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
// 			while((isdigit(ch_buf) || ch_buf == '.' || ch_buf == '-') && ch_buf != EOF) ch_buf = getc(file);
			while((!isdigit(ch_buf) && ch_buf != '-') && ch_buf != EOF) ch_buf = getc(file);
			ungetc(ch_buf, file);
		}
	}
	return matrix;
}

void destroy_multisize_matrix(MultysizeMatrix* matrix){
	for (int i = 0; i < matrix->size; i++){
		free(matrix->matrix[i]);
	}
	free(matrix->matrix);
	free(matrix);
}

MultysizeMatrix* copy_multisize_matrix(MultysizeMatrix* matrix){
	MultysizeMatrix* copy = malloc(sizeof(MultysizeMatrix));
	copy->matrix = malloc(sizeof(double*)*matrix->size);
	for (int i =0; i < matrix->size; i++){
		copy->matrix[i] = malloc(sizeof(double)*matrix->size);
		memcpy(copy->matrix[i], matrix->matrix, sizeof(double)*matrix->size);
	}
	copy->size = matrix->size;
	return copy;
}

void copy_matrix(double matrix_buf[MATRIX_SIZE][MATRIX_SIZE], double matrix[MATRIX_SIZE][MATRIX_SIZE]){
	for (int i = 0; i < MATRIX_SIZE; i++)
		memcpy(matrix_buf[i], matrix[i], sizeof(double)*MATRIX_SIZE);
}

void add_vector_to_vector(double vector_1[MATRIX_SIZE], double vector_2[MATRIX_SIZE]
){
	for (unsigned i = 0; i < MATRIX_SIZE; i++)
		vector_1[i] += vector_2[i];
}


double* gaussian_elimination_method(double matrix[MATRIX_SIZE][MATRIX_SIZE],
									double matrix_extending[MATRIX_SIZE]){
	unsigned matrix_size = MATRIX_SIZE;
	
	double matrix_copy[MATRIX_SIZE][MATRIX_SIZE];
	double matrix_extending_copy[MATRIX_SIZE];
	double* solve = (double*)malloc(sizeof(double)*MATRIX_SIZE);
	
	for (unsigned i = 0; i < matrix_size; i++){
		memcpy(matrix_copy[i], matrix[i], sizeof(double)*matrix_size);
	}
	memcpy(matrix_extending_copy, matrix_extending, sizeof(double)*matrix_size);
	
	// forward elimination
	for (unsigned iter = 0; iter < matrix_size - 1; iter++){
		for (unsigned row_num = iter + 1; row_num < matrix_size; row_num++){
			double supply_coefficient = matrix_copy[row_num][iter] / matrix_copy[iter][iter];
			for (unsigned el_num = iter; el_num < matrix_size; el_num++)
				matrix_copy[row_num][el_num] -= supply_coefficient * matrix_copy[iter][el_num];
			
			matrix_extending_copy[row_num] -= supply_coefficient * matrix_extending_copy[iter];
		}
	}
	
	// backward elimination
	for (int iter = MATRIX_SIZE - 1; iter >= 0; iter--){
		solve[iter] = matrix_extending_copy[iter];
		for (unsigned el_num = MATRIX_SIZE -1; el_num > iter; el_num--)
			solve[iter] -= solve[el_num]*matrix_copy[iter][el_num];
		solve[iter] /= matrix_copy[iter][iter];
	}
	
	return solve;
}


double* multisize_gaussian_elimination_method(MultysizeMatrix* matrix,
									double* matrix_extending){

	MultysizeMatrix* matrix_copy = copy_multisize_matrix(matrix);
	double* matrix_extending_copy = malloc(sizeof(double)*matrix->size);
	double* solve = (double*)malloc(sizeof(double)*matrix->size);
	
	memcpy(matrix_extending_copy, matrix_extending, sizeof(double)*matrix->size);
	
	// forward elimination
	for (unsigned iter = 0; iter < matrix->size - 1; iter++){
		for (unsigned row_num = iter + 1; row_num < matrix->size; row_num++){
			double supply_coefficient = matrix_copy->matrix[row_num][iter] / matrix_copy->matrix[iter][iter];
			for (unsigned el_num = iter; el_num < matrix->size; el_num++)
				matrix_copy->matrix[row_num][el_num] -= supply_coefficient * matrix_copy->matrix[iter][el_num];
			
			matrix_extending_copy[row_num] -= supply_coefficient * matrix_extending_copy[iter];
		}
	}
	
	// backward elimination
	for (int iter = matrix->size - 1; iter >= 0; iter--){
		solve[iter] = matrix_extending_copy[iter];
		for (unsigned el_num = matrix->size -1; el_num > iter; el_num--)
			solve[iter] -= solve[el_num]*matrix_copy->matrix[iter][el_num];
		solve[iter] /= matrix_copy->matrix[iter][iter];
	}
	destroy_multisize_matrix(matrix_copy);
	free(matrix_extending_copy);
	return solve;
}


double norm_oo_Vector(double vector[MATRIX_SIZE]){
    double max = fabs(vector[0]);

    for (int i = 0; i < MATRIX_SIZE; i++){
        if(fabs(vector[i])> max){
            max = fabs(vector[i]);
        }
    }
    return max;
}

double norm_oo_Matrix(double matrix[MATRIX_SIZE][MATRIX_SIZE]){
    double abs_sums[MATRIX_SIZE]={0};
    for(int i =0 ;i< MATRIX_SIZE;i++){
        for (int j =0;j < MATRIX_SIZE; j++){
            abs_sums[i] += fabs(matrix[i][j]);
        }
    }
    double max = abs_sums[0];
    for (int i =0 ; i < MATRIX_SIZE; i++){
        if(abs_sums[i]>max){
            max = abs_sums[i];
        }
    }
    return max;
}

int jacobi_elimination_method(double A[MATRIX_SIZE][MATRIX_SIZE],
		   double b[MATRIX_SIZE],
		   double x0[MATRIX_SIZE],
		   double epsilon,
		   FILE* file,
		   char* var_name){
	
    int counter = 0;
	double C[MATRIX_SIZE][MATRIX_SIZE] = {0};
	double g[MATRIX_SIZE] = {0};
	double xk[MATRIX_SIZE] = {0};
    double checkX[MATRIX_SIZE];
    double res;
	
    for (int i = 0; i < MATRIX_SIZE; i++) {
        for (int j = 0; j < MATRIX_SIZE; j++) {
            if (i == j) {
                continue;
            }
            C[i][j] =  - A[i][j] / A[i][i];
            g[i] = b[i] / A[i][i];
        }
    }

	
	fprintf(file, "%s = []\n", var_name);
    
    do{
        counter++;
        for (int i = 0; i < MATRIX_SIZE; i++) {
            res = 0;
            for (int j = 0; j < MATRIX_SIZE; j++) {
                res += C[i][j] * x0[j];
            }
            res += g[i];
            xk[i] = res;
        }
        for (int k = 0; k < MATRIX_SIZE; k++) {
            checkX[k] = xk[k] - x0[k];
        }
        for (int i = 0; i < MATRIX_SIZE; i++) {
            x0[i] = xk[i];
        }
        
		fprintf(file, "%s.append(None)\n", var_name);
		char name[50] = {0};
		sprintf(name, "%s[-1]", var_name);
		print_vector_to_python_code(file, name, xk);
        
    }while (norm_oo_Vector(checkX) > ((1 - norm_oo_Matrix(C)) / norm_oo_Matrix(C) * epsilon));
    return counter;
}


double scalar_vector_on_vector(double vec1[MATRIX_SIZE], double vec2[MATRIX_SIZE]){
	double res = 0;
	for (int i = 0; i < MATRIX_SIZE; i ++){
		res += vec1[i]*vec2[i];
	}
	return res;
}

double norm_frob_vector(double vec[MATRIX_SIZE]){
	return sqrt(scalar_vector_on_vector(vec, vec));
}

int grad_elimination_method(double A[MATRIX_SIZE][MATRIX_SIZE],
		   double B[MATRIX_SIZE],
		   double X0[MATRIX_SIZE],
		   double epsilon){
	
	double r[MATRIX_SIZE] = {0};
	double z[MATRIX_SIZE] = {0};
	double a, b;
	double buf_vec[MATRIX_SIZE];
	dot_matrix_with_vector_save(buf_vec, A, X0);
	for (int i = 0; i < MATRIX_SIZE; i++){
		r[i] = B[i] - buf_vec[i];
		z[i] = r[i];
	}
	
	int iter_count = 0;
	do{
		dot_matrix_with_vector_save(buf_vec, A, z);
		
		b = scalar_vector_on_vector(r, r);
		
		a = scalar_vector_on_vector(r, r)/scalar_vector_on_vector(buf_vec, z);
		for (int i = 0; i < MATRIX_SIZE; i++){
			X0[i] += a*z[i];
			
			r[i] -= a*buf_vec[i];
		}
		
		b = scalar_vector_on_vector(r, r)/b;
		
		for (int i = 0; i < MATRIX_SIZE; i++){
			z[i] = r[i] + b*z[i];
		}
		iter_count++;
	} while(norm_frob_vector(r)/norm_frob_vector(B) >= epsilon);
	return iter_count;
}

double multisize_norm_oo_Vector(double* vector, int size){
    double max = fabs(vector[0]);

    for (int i = 0; i < size; i++){
        if(fabs(vector[i])> max){
            max = fabs(vector[i]);
        }
    }
    return max;
}

double multisize_norm_oo_Matrix(MultysizeMatrix* matrix){
    double* abs_sums= malloc(sizeof(double)*matrix->size);
	memset(abs_sums, 0, sizeof(double)*matrix->size);
    for(int i =0 ;i< matrix->size;i++){
        for (int j =0;j < matrix->size; j++){
            abs_sums[i] += fabs(matrix->matrix[i][j]);
        }
    }
    double max = abs_sums[0];
    for (int i =0 ; i < matrix->size; i++){
        if(abs_sums[i]>max){
            max = abs_sums[i];
        }
    }
    free(abs_sums);
    return max;
}


int multisize_jacobi_elimination_method(MultysizeMatrix* A,
		   double* b,
		   double* x0,
		   double epsilon){
	
    int counter = 0;
	MultysizeMatrix* C = malloc(sizeof(MultysizeMatrix));
	C->matrix = malloc(sizeof(double*)*A->size);
	for (int i =0; i < A->size; i++){
		C->matrix[i] = malloc(sizeof(double)*A->size);
		memset(C->matrix[i], 0, sizeof(double)*A->size);
	}
	C->size = A->size;
	double* g = malloc(sizeof(double)*A->size);
	memset(g, 0, sizeof(double)*A->size);
	double* xk = malloc(sizeof(double)*A->size);
	memset(xk, 0, sizeof(double)*A->size);
	double* checkX = malloc(sizeof(double)*A->size);
	memset(checkX, 0, sizeof(double)*A->size);
    double res;
	
    for (int i = 0; i < A->size; i++) {
        for (int j = 0; j < A->size; j++) {
            if (i == j) {
                continue;
            }
            C->matrix[i][j] =  - A->matrix[i][j] / A->matrix[i][i];
            g[i] = b[i] / A->matrix[i][i];
        }
    }
    
    do{
        counter++;
        for (int i = 0; i < A->size; i++) {
            res = 0;
            for (int j = 0; j < A->size; j++) {
                res += C->matrix[i][j] * x0[j];
            }
            res += g[i];
            xk[i] = res;
        }
        for (int k = 0; k < A->size; k++) {
            checkX[k] = xk[k] - x0[k];
        }
        for (int i = 0; i < A->size; i++) {
            x0[i] = xk[i];
        }

        
    }while (multisize_norm_oo_Vector(checkX, A->size) > ((1 - multisize_norm_oo_Matrix(C)) / multisize_norm_oo_Matrix(C) * epsilon));

// 	destroy_multisize_matrix(C);
	free(g);
	free(xk);
	free(checkX);
	
    return counter;
}


double vector_norm(double vector[MATRIX_SIZE]){
	double pow_sum = 0;
	for (int i = 0; i < MATRIX_SIZE; i++)
		pow_sum += pow(vector[i], 2);
	return sqrt(pow_sum);
}


int main(){
	srand(time(NULL));
	
	FILE* matlab_code = fopen("matrix/calculation_result.m", "w");
	fprintf(matlab_code, "result_1 = containers.Map()\n");

	double eps = 0.1;
	for (int i = 0; i < 16; i++, eps /= 10){
		DataSerializationStruct result;

		char filename[1000] = {'\0'};
		char str[100] = {0};
		double solving[MATRIX_SIZE] = {0};
// 		sprintf(filename, "/home/arseniy/Documents/SPbSTU/Numeric_methods/Sem_3/Labs/J_3/Code/Grad_iteration_elimination/matrix/matrix_%d", 0);
		sprintf(filename, "matrix/matrix_%d", 0);
		FILE* file = fopen(filename, "r");
		
		sprintf(str, "result_1(\'%d\')", i);
	
		matrix_from_file(file, result.a_matrix);
		
		double X[] = {1,1,1,1,1,1,1,1,1,1};
		memcpy(result.x_vector, X, sizeof(double)*MATRIX_SIZE);
		fill_vector(result.x_vector, 0, 10);

		dot_matrix_with_vector_save(result.b_vector, result.a_matrix, result.x_vector);
		
		for(int i = 0; i < MATRIX_SIZE; i++)
			solving[i] = result.b_vector[i]/result.a_matrix[i][i];
		

		int iterations = grad_elimination_method(result.a_matrix, result.b_vector, solving, eps);
		
		
		memcpy(result.x_calculated, solving, sizeof(double)* MATRIX_SIZE);
		result.eps = eps;
		print_data_to_matlab_code(matlab_code, "map", &result);
		fprintf(matlab_code, "%s(\'iterations_amount\') = %d\n\n", "map", iterations);
		fprintf(matlab_code, "%s = %s\n", str, "map");
		print_vector(stdout, "res", solving);

		fclose(file);
	}
// 	fclose(python_code);

	printf("END!!!\n");
	fclose(matlab_code);
}
