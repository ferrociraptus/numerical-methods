#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <ctype.h>

#define MATRIX_SIZE 10
#define TESTING_ITERATIONS 15
#define ITERATIONS 1000
#define EPS 1e-16

typedef struct{
	double** matrix;
	unsigned size;
} MultysizeMatrix;

typedef struct{
	double a_matrix[MATRIX_SIZE][MATRIX_SIZE];
	double x_vector[MATRIX_SIZE];
	double x_calculated[MATRIX_SIZE];
	double b_vector[MATRIX_SIZE];
	double eps;
	double cond;
}DataSerializationStruct;


int cmp(const void* val1, const void* val2){
	if ((*((double*)val1)) > (*((double*)val2)))
		return 1;
	else if ((*((double*)val1)) == (*((double*)val2)))
		return 0;
	else
		return -1;
}

void fill_vector(double* vector, double range_min, double range_max){
	double range = fabs(range_max) + fabs(range_min);
	for (int i = 0; i < MATRIX_SIZE; i ++){
		double rand_coef = (double)rand() / RAND_MAX;
		vector[i] = (rand() % (int)range)+rand_coef + range_min;
	}
}


double* dot_matrix_with_vector_save(double* result, double matrix[MATRIX_SIZE][MATRIX_SIZE], double vector[MATRIX_SIZE]){
	memset(result, 0, sizeof(double) * MATRIX_SIZE);
	for (int row = 0; row < MATRIX_SIZE; row++)
		for (int col = 0; col < MATRIX_SIZE; col++)
			result[row] += vector[col] * matrix[row][col];
		
	return result;
}

double* multisize_dot_matrix_with_vector_save(double* result, MultysizeMatrix* matrix, double* vector){
	memset(result, 0, sizeof(double) * matrix->size);
	for (int row = 0; row < matrix->size; row++)
		for (int col = 0; col < matrix->size; col++)
			result[row] += vector[col] * matrix->matrix[row][col];
		
	return result;
}


void print_matrix_to_matlab_code(FILE* stream, const char* field_name, double matrix[MATRIX_SIZE][MATRIX_SIZE]){
	fprintf(stream, "%s = [", field_name);
	for (unsigned row = 0; row < MATRIX_SIZE; row++){
		fprintf(stream, "");
		for (unsigned col = 0; col < MATRIX_SIZE-1; col++)
			fprintf(stream, "%.30lf ", matrix[row][col]);
		fprintf(stream, "%.30lf", matrix[row][MATRIX_SIZE - 1]);
		fprintf(stream, "");
		
		if (row < MATRIX_SIZE-1)
			fprintf(stream, ";");
	}
	fprintf(stream, "]\n");
}

void print_vector_to_matlab_code(FILE* stream, const char* field_name, double vector[MATRIX_SIZE]){
	fprintf(stream, "%s = [", field_name);
	for (unsigned col = 0; col < MATRIX_SIZE; col++){
		fprintf(stream, "%.30lf", vector[col]);
		if (col < MATRIX_SIZE-1)
			fprintf(stream, " ");
	}
	fprintf(stream, "]\n");
}

void print_vector(FILE* stream, const char* field_name, double vector[MATRIX_SIZE]){
	fprintf(stream, "%s = [", field_name);
	for (unsigned col = 0; col < MATRIX_SIZE-1; col++){
		fprintf(stream, "\"%.10lf\"", vector[col]);
		if (col < MATRIX_SIZE-1)
			fprintf(stream, ",");
	}
	fprintf(stream, "]\n");
}

void print_data_to_matlab_code(FILE* stream, const char* field_name, DataSerializationStruct* str){
	char buf[100] = {0};
	fprintf(stream, "%s = containers.Map()\n", field_name);
	
	sprintf(buf, "%s(\'A\')", field_name);
	print_matrix_to_matlab_code(stream, buf, str->a_matrix );
	
	sprintf(buf, "%s(\'B\')", field_name);
	print_vector_to_matlab_code(stream, buf, str->b_vector);
	
	sprintf(buf, "%s(\'X\')", field_name);
	print_vector_to_matlab_code(stream, buf, str->x_vector);
	
	sprintf(buf, "%s(\'X_calculated\')", field_name);
	print_vector_to_matlab_code(stream, buf, str->x_calculated);

	fprintf(stream, "%s(\'eps\') = %.20lf\n", field_name, str->eps);
}


void matrix_from_file(FILE* file, DataSerializationStruct* str){

	char ch_buf = getc(file);
	while(!isdigit(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
	ungetc(ch_buf, file);
	
	
	for (unsigned row = 0; row < MATRIX_SIZE; row++){
		for (unsigned col = 0; col < MATRIX_SIZE; col++){
			double val;
			fscanf(file, "%lf", &val);
			str->a_matrix[row][col] = val;
			
			ch_buf = getc(file);
			while(isspace(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
			while((!isdigit(ch_buf) && ch_buf != '-') && ch_buf != EOF) ch_buf = getc(file);
			ungetc(ch_buf, file);
		}
	}
	while(isspace(ch_buf) && ch_buf != EOF) ch_buf = getc(file);
	while((!isdigit(ch_buf) && ch_buf != '-') && ch_buf != EOF) ch_buf = getc(file);
	ungetc(ch_buf, file);
	fscanf(file, "%lf", &str->cond);
}


double scalar_vector_on_vector(double vec1[MATRIX_SIZE], double vec2[MATRIX_SIZE]){
	double res = 0;
	for (int i = 0; i < MATRIX_SIZE; i ++){
		res += vec1[i]*vec2[i];
	}
	return res;
}

double norm_frob_vector(double vec[MATRIX_SIZE]){
	double res = 0;
	for (int i = 0; i < MATRIX_SIZE; i ++){
		res += vec[i]*vec[i];
	}
	return sqrt(res);
}

double norm_oo_vector(double vec[MATRIX_SIZE]){
	double max = -1e16;
	for (int i = 0; i < MATRIX_SIZE; i++){
		if (max < fabs(vec[i]))
			max = vec[i];
	}
	return max;
}

void vec_diff_save(double vec1[MATRIX_SIZE], double vec2[MATRIX_SIZE], double res[MATRIX_SIZE]){
	for (int i = 0; i < MATRIX_SIZE; i++){
		res[i] = vec1[i] - vec2[i];
	}
}

void abs_vec_diff_save(double vec1[MATRIX_SIZE], double vec2[MATRIX_SIZE], double res[MATRIX_SIZE]){
	for (int i = 0; i < MATRIX_SIZE; i++){
		res[i] = fabs(vec1[i]) - fabs(vec2[i]);
	}
}

void vec_copy(double from[MATRIX_SIZE], double to[MATRIX_SIZE]){
	memcpy(to, from, sizeof(double)*MATRIX_SIZE);
}

int grad_elimination_method(double A[MATRIX_SIZE][MATRIX_SIZE],
		   double B[MATRIX_SIZE],
		   double X0[MATRIX_SIZE],
		   double cond,
		   double epsilon){
	
	double r[MATRIX_SIZE] = {0};
	double z[MATRIX_SIZE] = {0};
	double x_prev[MATRIX_SIZE] = {0};
	vec_copy(X0, x_prev);
	double a, b;
	double m = (cond - 1.0) / (cond + 1.0);
	double buf_vec[MATRIX_SIZE];
	dot_matrix_with_vector_save(buf_vec, A, X0);
	for (int i = 0; i < MATRIX_SIZE; i++){
		r[i] = B[i] - buf_vec[i];
		z[i] = r[i];
	}
	double norm;
	int iter_count = 0;
	do{
		dot_matrix_with_vector_save(buf_vec, A, z);
		
		b = scalar_vector_on_vector(r, r);
		
		a = scalar_vector_on_vector(r, r)/scalar_vector_on_vector(buf_vec, z);
		for (int i = 0; i < MATRIX_SIZE; i++){
			X0[i] += a*z[i];
			
			r[i] -= a*buf_vec[i];
		}
		
		b = scalar_vector_on_vector(r, r)/b;
		
		for (int i = 0; i < MATRIX_SIZE; i++){
			z[i] = r[i] + b*z[i];
		}
		iter_count++;
		
		dot_matrix_with_vector_save(buf_vec, A, X0);
		vec_diff_save(B, buf_vec, buf_vec);
		norm = norm_oo_vector(buf_vec);
	} while(fabs(norm) >= m * epsilon);
	return iter_count;
}

void diff_vec_to_value(double vec[MATRIX_SIZE], double val){
	for (int i = 0; i < MATRIX_SIZE; i++){
		vec[i]-=val;
	}
}

int main(){
	srand(time(NULL));
	
	FILE* matlab_code = fopen("matrix/calculation_result.m", "w");
	fprintf(matlab_code, "result_1 = containers.Map()\n");

	double eps = 0.1;
	for (int i = 0; i < 12; i++, eps /= 10){
		DataSerializationStruct result;
		result.eps = eps;
		char filename[1000] = {'\0'};
		char str[100] = {0};
		double solving[MATRIX_SIZE] = {0};
		sprintf(filename, "matrix/matrix_%d", 0);
		FILE* file = fopen(filename, "r");
		
		sprintf(str, "result_1(\'%d\')", i);
	
		matrix_from_file(file, &result);
		
// 		double X[] = {0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5};
// 		diff_vec_to_value(X, eps);
// 		memcpy(result.x_vector, X, sizeof(double)*MATRIX_SIZE);
		
		
		//if you want check calculation result comment this line
		fill_vector(result.x_vector, 1, 5);

		
		dot_matrix_with_vector_save(result.b_vector, result.a_matrix, result.x_vector);
		
		for(int i = 0; i < MATRIX_SIZE; i++)
			result.x_calculated[i] = result.b_vector[i]/result.a_matrix[i][i];
		

		int iterations = grad_elimination_method(result.a_matrix, result.b_vector, result.x_calculated, result.cond, eps);
		/*
		qsort(result.x_calculated, MATRIX_SIZE, sizeof(double), cmp);
		qsort(result.x_vector, MATRIX_SIZE, sizeof(double), cmp);
		*/
		
		print_data_to_matlab_code(matlab_code, "map", &result);
		fprintf(matlab_code, "%s(\'iterations_amount\') = %d\n\n", "map", iterations);
		fprintf(matlab_code, "%s = %s\n", str, "map");
		print_vector(stdout, "res", result.x_calculated);
		fclose(file);
	}
	fclose(matlab_code);
}

