import subprocess
from decimal import Decimal


def dict_to_variables(dictionary):
    locals().update(dictionary)


def c_program_fun_wrapper(binary_path):
    def eval_c_code(*args):
        if args is None:
            args = b""
        else:
            args = (("\n".join(list(map(str, args))))+"\n").encode("utf-8")

        process = subprocess.Popen([binary_path], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        process.stdin.write(args)
        stdout, _ = process.communicate()
        stdout = stdout.decode("utf-8").splitlines()
        keys = stdout.pop(0).split()
        ans = {key: [] for key in keys}
        for line in stdout:
            for key, value in zip(keys, line.split()):
                ans[key].append(value)
        return ans

    return eval_c_code
