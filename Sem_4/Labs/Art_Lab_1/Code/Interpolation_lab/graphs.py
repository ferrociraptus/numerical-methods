from matplotlib import pyplot as plt
import numpy as np
from decimal import Decimal
from math import prod, factorial

from C_eval import c_program_fun_wrapper, dict_to_variables
import os

images_dir = "images/"

if not os.path.exists(images_dir):
    os.makedirs(images_dir)
elif not os.path.isdir(images_dir):
    os.remove(images_dir)
    os.makedirs(images_dir)


def points_amount_to_error_chebyshev():
    fig, ax = plt.subplots()

    ax.grid(True)
    ax.set_yscale('log')
    # ax.set_xscale('log')
    ax.set_yscale('log')

    errors = []

    c_fun = c_program_fun_wrapper("./main")
    c_poly_fun = c_program_fun_wrapper("./poly_fun")

    n_values = list(range(2, 101))
    for n in n_values:
        res = {key: [Decimal(el) for el in val] for key, val in c_fun(n).items()}
        X = res["X"]
        max_err = Decimal("0")
        for x, x_next in zip(X, X[1:]):
            calc_point = (x_next - x) / 2
            res = c_poly_fun(n, calc_point)
            poly_val = Decimal(res["poly_val"][0])
            fun_val = Decimal(res["func_val"][0])
            step_err = abs(poly_val - fun_val)
            if step_err > max_err:
                max_err = step_err
        errors.append(max_err)

    print(errors)

    ax.plot(n_values, errors, linewidth=2, label="Chebyshev interpolation error (Chebyshev nodes)")

    plt.xlabel(r"Nodes amount")
    plt.ylabel("Error")
    plt.title("Points amount to error (Chebyshev interpolation grid)")

    ax.legend(loc="upper right")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig(images_dir + f'points_amount_to_error_chebyshev.png', dpi=100)


def functions_and_their_interpolation(n):
    fig, ax = plt.subplots()

    ax.grid(True)
    ax.set_yscale('log')
    # ax.set_xscale('log')
    # ax.set_yscale('log')

    c_fun = c_program_fun_wrapper("./main")
    c_poly_fun = c_program_fun_wrapper("./poly_fun")

    res = c_fun(n)
    grid_x = [Decimal(val) for val in res["X"]]
    grid_y = [Decimal(val) for val in res["fun_val"]]

    x = list(np.linspace(float(grid_x[0]), float(grid_x[-1]), num=100))

    res = c_poly_fun(*([n] + x))
    poly_val = [Decimal(val) for val in res["poly_val"]]
    fun_val = [Decimal(val) for val in res["func_val"]]

    ax.plot(x, fun_val, linestyle="-", linewidth=4, label=r"$f(x) = x \log(x + 1) - /frac{1}{2}$")
    ax.plot(x, poly_val, linewidth=3, label="Chebyshev interpolation (Chebyshev nodes)")

    ax.scatter(grid_x, grid_y, marker='d', linewidths=6, color="green")

    plt.xlabel(r"X")
    plt.ylabel("Y")
    plt.title(f"Points amount to error (Chebyshev interpolation grid), {n} size grid")

    ax.legend(loc="lower right")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig(images_dir + f'functions_and_their_interpolation_n_{n}.png', dpi=100)


def fact_error(*args):
    fig, ax = plt.subplots()

    ax.grid(True)
    ax.set_yscale('log')
    # ax.set_xscale('log')
    # ax.set_yscale('log')

    c_fun = c_program_fun_wrapper("./main")
    c_poly_fun = c_program_fun_wrapper("./poly_fun")
    c_fun(4)
    for n in args:
        res = c_fun(n)
        grid_x = [Decimal(val) for val in res["X"]]
        # grid_y = [Decimal(val) for val in res["fun_val"]]

        x = list(np.linspace(float(grid_x[0]), float(grid_x[-1]), num=500))
        x.extend(grid_x)
        x.sort()

        res = c_poly_fun(*([n] + x))
        poly_val = np.array([Decimal(val) for val in res["poly_val"]], dtype=np.dtype(Decimal))
        fun_val = np.array([Decimal(val) for val in res["func_val"]], dtype=np.dtype(Decimal))

        ax.plot(x, np.abs(poly_val - fun_val), linewidth=3, label=f"Chebyshev interpolation fact error n = {n}")
        if n == 3:
            awaiting_error = lambda x: np.abs(prod(x - np.array(grid_x)) / 2)
            ax.plot(x, [awaiting_error(Decimal(xi)) for xi in x], linewidth=3,
                    linestyle='-.',
                    label=f"Chebyshev interpolation theoretical "
                          f"error n = {n}")

    # ax.scatter(grid_x, grid_y, marker='d', linewidths=6, color="green")

    plt.xlabel(r"X")
    plt.ylabel("Fact error")
    plt.title(f"Fact error in points (Chebyshev interpolation grid)")

    ax.legend(loc="lower right")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig(images_dir + f'fact_error_n_{"_".join(map(str, args))}.png', dpi=100)
    awaiting_error = lambda x: prod(x - np.array(grid_x)) / 2


def fact_error_with_3_awaiting_error():
    fig, ax = plt.subplots()
    n = 3
    ax.grid(True)
    ax.set_yscale('log')
    # ax.set_xscale('log')
    ax.set_yscale('log')

    c_fun = c_program_fun_wrapper("./main")
    c_poly_fun = c_program_fun_wrapper("./poly_fun")

    res = c_fun(n)
    grid_x = [Decimal(val) for val in res["X"]]
    awaiting_error = lambda x: np.abs(prod(x - np.array(grid_x)) / 2)
    # grid_y = [Decimal(val) for val in res["fun_val"]]

    x = list(np.linspace(float(grid_x[0]), float(grid_x[-1]), num=500))
    x.extend(grid_x)
    x.sort()

    res = c_poly_fun(*([n] + x))
    poly_val = np.array([Decimal(val) for val in res["poly_val"]], dtype=np.dtype(Decimal))
    fun_val = np.array([Decimal(val) for val in res["func_val"]], dtype=np.dtype(Decimal))

    # ax.plot(x, np.abs(poly_val - fun_val), linewidth=3, label=f"Chebyshev interpolation fact error n = {n}")
    ax.plot(x, [awaiting_error(Decimal(xi)) for xi in x], linestyle='--', linewidth=3,
            label=f"Chebyshev interpolation theoretical error n = {n}")

    # ax.scatter(grid_x, grid_y, marker='d', linewidths=6, color="green")

    plt.xlabel(r"X")
    plt.ylabel("Error")
    plt.title(f"Theoretical error in points (Chebyshev interpolation grid)")

    ax.legend(loc="lower right")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig(images_dir + f'fact_error_with_3_awaiting_error_n_3.png', dpi=100)


# functions_and_their_interpolation(3)
# functions_and_their_interpolation(5)
# functions_and_their_interpolation(10)
fact_error(3, 5, 10)
# fact_error(5)
# fact_error(10)
# points_amount_to_error_chebyshev()
# fact_error_with_3_awaiting_error()
plt.show()
