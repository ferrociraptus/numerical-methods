import numpy as np
import matplotlib.pyplot as plt
from Euler_modified_diff_method import euler_modified_2diff_method, euler_modified_2diff_method_to_eps, bisection
from collections.abc import Iterable

images_dir = "images/"
# images_dir = "./"

# fun declaration
fun = lambda x: 1 + 1 / x
diffur = lambda x, y, y1: (1 / x ** 2 + 2 * y + y1) / (x ** 2 * (x + 1))
fun_range = [1, 3]
fun_range_2 = [3, 5]

from pickle import load

with open("bisection_result_dump_1_to_6", "rb") as file:
    solve_1_to_6 = load(file)
with open("bisection_result_dump", "rb") as file:
    solve_1 = load(file)
# with open("bisection_result_2_dump", "rb") as file:
#     solve_2 = load(file)


# grid_x, solve, nodes, h, runge_err = euler_modified_2diff_method_to_eps(diffur, *fun_range, 2, -1, eps=2e-9)
# grid_x_2, solve_2, nodes_2, h_2, runge_err_2 = euler_modified_2diff_method_to_eps(diffur, *fun_range_2, 1+1/3, -1/9, eps=2e-9)

import pickle as p

with open("range_1_results_dump", "rb") as file:
    grid_x, solve, nodes, h, runge_err = p.load(file)
with open("range_2_results_dump", "rb") as file:
    grid_x_2, solve_2, nodes_2, h_2, runge_err_2 = p.load(file)


def stdout_run_status_log(fun):
    def decorator(*args, **kwargs):
        print(f"Function: {fun.__name__} started")
        res = fun(*args, **kwargs)
        print(f"Function: {fun.__name__} finished")
        return res

    return decorator


@stdout_run_status_log
def absolute_error_to_iteration(func=fun, diff=diffur, func_range=fun_range):
    fig, ax = plt.subplots()

    plt.title("Абсолютная ошибка от итерации пристрелки")

    # ax.plot(grid, fun(grid), label="true")
    # solve = euler_modified_2diff_method(diffur, grid, 6, -25)
    # grid, solve = euler_modified_2diff_method_to_eps(diff, *func_range, 6, -25, eps=1e-8)[0:2]
    steps = [i for i in range(1, len(solve_1) + 1)]
    errors = [abs(solve[1][-1] - fun(fun_range[-1])) for step, solve in solve_1]

    ax.plot(steps, errors, label=r"Модифицированный метод Эйлера $\epsilon = 1^{-7}$")
    # ax.plot(grid_x_2, abs(solve_2 - fun(grid_x_2)), label=r"Модифицированный метод Эйлера $\epsilon = 1^{-7}$")
    # solve = euler_2diff_method(diffur, grid, 6, -1 / 0.04)
    # ax.plot(grid, abs(solve[1] - y), label="usual")

    ax.set_yscale('log')
    # ax.set_xscale('log')
    ax.legend()
    ax.grid()
    plt.xlabel('номер итерации')
    plt.ylabel("абсолютная ошибка")

    fig.set_size_inches(9, 5.5)
    fig.savefig(images_dir + f'absolute_error_to_iteration_{"_".join(map(str, func_range))}.png')


@stdout_run_status_log
def absolute_error_to_eps(func=fun, diff=diffur, func_range=fun_range):
    fig, ax = plt.subplots()

    plt.title("Абсолютная ошибка от заданой точности")

    # ax.plot(grid, fun(grid), label="true")
    # solve = euler_modified_2diff_method(diffur, grid, 6, -25)
    # grid, solve = euler_modified_2diff_method_to_eps(diff, *func_range, 6, -25, eps=1e-8)[0:2]
    solve = solve_1_to_6.copy()
    solve.append(solve_1)
    it = [len(solving) for solving in solve]

    eps = [10 ** (-(i + 1)) for i in range(len(solve))]

    errors = [abs(solving[1][-1] - fun(fun_range[-1])) for step, solving in [val[-1] for val in solve]]

    ax.plot(eps, errors, label=r"Модифицированный метод Эйлера")
    ax.plot(eps, eps, "--")

    # ax.plot(grid_x_2, abs(solve_2 - fun(grid_x_2)), label=r"Модифицированный метод Эйлера $\epsilon = 1^{-7}$")
    # solve = euler_2diff_method(diffur, grid, 6, -1 / 0.04)
    # ax.plot(grid, abs(solve[1] - y), label="usual")

    ax.set_yscale('log')
    ax.set_xscale('log')
    ax.legend()
    ax.grid()
    plt.xlabel('$\epsilon$')
    plt.ylabel("абсолютная ошибка")

    fig.set_size_inches(9, 5.5)
    fig.savefig(images_dir + f'absolute_error_to_eps{"_".join(map(str, func_range))}.png')


@stdout_run_status_log
def sighting_visualization(func=fun, diff=diffur, func_range=fun_range):
    fig, ax = plt.subplots()

    plt.title("Сравнение результатов вычисления")

    # grid_x, solve, nodes, h, runge_err = euler_modified_2diff_method_to_eps(diff, *func_range, 6, -25, eps=1e-8)
    for i, (diff_2, solve) in enumerate(solve_1):
        if (i + 3) % 5 == 0 and i != 0:
            ax.plot(solve[0], solve[1], label=f"{i} итерация пристрелки, y' = {diff_2}")
    # print(len(solve_1[-1][1][0]))
    ax.plot(solve_1[1][1][0], func(solve_1[1][1][0]), "--", label=r"$y = 1 + \frac{1}{x}$", linewidth=4)

    # ax.set_yscale('log')
    # ax.set_xscale('log')

    ax.legend()
    ax.grid()

    plt.xlabel('x')
    plt.ylabel('y')

    fig.set_size_inches(9, 5.5)
    fig.savefig(images_dir + f'sighting_visualization_{"_".join(map(str, func_range))}.png')


@stdout_run_status_log
def iterations_to_eps(func=fun, diff=diffur, func_range=fun_range):
    fig, ax = plt.subplots()

    plt.title("Отношение точности к количеству итераций пристрелки")

    # grid_x, solve, nodes, h, runge_err = euler_modified_2diff_method_to_eps(diff, *func_range, 6, -25, eps=1e-8)
    # ax.plot(grid, y, label="true", linewidth=3)
    # ax.plot(*solve, label="Euler")
    solve = solve_1_to_6.copy()
    solve.append(solve_1)
    it = [len(solving) for solving in solve]

    ax.plot([10 ** (-(i + 1)) for i in range(len(it))], it)
    # ax.plot(nodes_2, runge_err_2, label=f"Ошибка Рунге {fun_range_2}")
    # ax.plot(grid, solve[1], label="usual")

    ax.set_xscale('log')
    # ax.set_yscale('log')
    ax.legend()
    ax.grid()
    plt.xlabel('$\epsilon$')
    plt.ylabel('количество узлов')

    fig.set_size_inches(9, 5.5)
    fig.savefig(images_dir + f'iterations_to_eps_{"_".join(map(str, func_range))}.png')


@stdout_run_status_log
def compare_graph_solve_and_through(func=fun, diff=diffur, func_range=fun_range):
    fig, ax = plt.subplots()

    x = list(grid_x.copy())
    x.extend(list(grid_x_2))
    x = np.array(x)[::1000]
    print(len(x))
    ax.plot(x, fun(x), label=r"$y = 1 + \frac{1}{x}$", linewidth=7, color="black")
    ax.plot(grid_x[::1000], solve[::1000], label=r"Модифицированный метод Эйлера $\epsilon = 1^{-9}$", linewidth=2,
            color="yellow")
    ax.plot(grid_x_2[::1000], solve_2[::1000], label=r"Модифицированный метод Эйлера $\epsilon = 1^{-9}$", linewidth=2,
            color="orange")
    # solve = euler_2diff_method(diffur, grid, 6, -1 / 0.04)
    # ax.plot(*solve, label="usual")

    plt.legend()
    plt.grid()

    # ax.set_yscale('log')
    # ax.set_xscale('log')

    plt.title(r"Сравнение результатов вычислений метода и точного решения")
    plt.xlabel(r'X')
    plt.ylabel(r'Y')

    fig.set_size_inches(9, 5.5)
    fig.savefig(images_dir + f'compare_graph_solve_and_through_modified_euler_{"_".join(map(str, func_range))}.png')


@stdout_run_status_log
def through_graph(func=fun, diff=diffur, func_range=fun_range):
    fig, ax = plt.subplots()

    grid_x = solve_1[-1][1][0]
    x = grid_x[::1000]
    ax.plot(x, fun(x), label=r"$y = 1 + \frac{1}{x}$   [1, 3]", color="blue")
    x = grid_x_2[::1000]
    ax.plot(x, fun(x), label=r"$y = 1 + \frac{1}{x}$   [3, 5]", color="green")
    plt.legend()
    plt.grid()

    # ax.set_yscale('log')
    # ax.set_xscale('log')

    plt.title(r"График функции $y = 1 + \frac{1}{x}$")
    plt.xlabel(r'x')
    plt.ylabel(r'y')

    fig.set_size_inches(9, 5.5)
    fig.savefig(images_dir + f'solve_graph_{"_".join(map(str, func_range))}_euler.png')


@stdout_run_status_log
def error_to_x(func=fun, diff=diffur, func_range=fun_range):
    fig, ax = plt.subplots()

    plt.title("Абсолютная ошибка на промежутке [1, 3]")

    # ax.plot(grid, fun(grid), label="true")
    # solve = euler_modified_2diff_method(diffur, grid, 6, -25)
    # grid, solve = euler_modified_2diff_method_to_eps(diff, *func_range, 6, -25, eps=1e-8)[0:2]
    grid_x = solve_1[-1][1][0]
    solve = solve_1[-1][1][1]
    ax.plot(grid_x, abs(solve - fun(grid_x)), label=r"Модифицированный метод Эйлера $\epsilon = 1^{-7}$,"
                                                    + f"\n Итерация {len(solve_1)}\n y' = {solve_1[-1][0]}")
    # ax.plot(grid_x_2, abs(solve_2 - fun(grid_x_2)), label=r"Модифицированный метод Эйлера $\epsilon = 1^{-9}$")
    # solve = euler_2diff_method(diffur, grid, 6, -1 / 0.04)
    # ax.plot(grid, abs(solve[1] - y), label="usual")

    ax.set_yscale('log')
    # ax.set_xscale('log')
    ax.legend()
    ax.grid()
    plt.xlabel('X')
    plt.ylabel("абсолютная ошибка")

    fig.set_size_inches(9, 5.5)
    fig.savefig(images_dir + f'absolute_error_modified_euler_{"_".join(map(str, func_range))}.png')


# absolute_error_to_iteration()
# absolute_error_to_eps()
# sighting_visualization()
# iterations_to_eps()
# through_graph()
error_to_x()
plt.show()
