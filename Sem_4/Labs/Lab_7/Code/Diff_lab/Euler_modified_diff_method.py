import numpy as np


# def euler_2diff_method(fun, grid_x, solve_y, diff1_solve_y):
#     grid_y = [solve_y]
#     grid_y_first_diff = [diff1_solve_y]
#     step = np.abs(grid_x[1] - grid_x[0])
#
#     for i in range(len(grid_x)):
#         grid_y.append(grid_y[i] + step * grid_y_first_diff[i])
#         grid_y_first_diff.append(grid_y_first_diff[i] + step * fun(grid_x[i], grid_y[i], grid_y_first_diff[i]))
#
#     return grid_x, np.array(grid_y)


def euler_modified_2diff_method(fun, grid_x, solve_y, diff1_solve_y):
    grid_y = [solve_y]
    grid_y_first_diff = [diff1_solve_y]
    step = np.abs(grid_x[1] - grid_x[0])
    # print(f"H = {step}")
    #
    # print(f"Y_(0) = {grid_y[-1]}")
    # print(f"Z_(0) = {grid_y_first_diff[-1]}")

    for i in range(len(grid_x) - 1):
        grid_y_half = grid_y[i] + step / 2 * grid_y_first_diff[i]
        # print(f"Y_({i} + 1/2) = {grid_y_half}")
        grid_y_first_diff_half = grid_y_first_diff[i] + step / 2 * fun(grid_x[i], grid_y[i], grid_y_first_diff[i])
        # print(f"Z_({i} + 1/2) = {grid_y_first_diff_half}")
        grid_y.append(grid_y[i] + step * grid_y_first_diff_half)
        # print(f"Y_({i+1}) = {grid_y[-1]}")
        grid_y_first_diff.append(grid_y_first_diff[i]
                                 + step * fun(grid_x[i] + step / 2, grid_y_half, grid_y_first_diff_half))
        # print(f"Z_({i+1}) = {grid_y_first_diff[-1]}")

    return grid_x, np.array(grid_y)


def euler_modified_2diff_method_to_eps(fun, a, b, solve_y, diff1_solve_y, eps=0.1):
    # euler_modified_2diff_method_to_eps.X = 0
    # euler_modified_2diff_method_to_eps.solve = 1
    # euler_modified_2diff_method_to_eps.nodes = 2
    # euler_modified_2diff_method_to_eps.h = 3
    # euler_modified_2diff_method_to_eps.runge_err = 4

    h = .1
    solve_prev = euler_modified_2diff_method(fun, np.arange(a, b, h), solve_y, diff1_solve_y)[1]
    h /= 2
    grid_x = np.arange(a, b, h)
    solve = euler_modified_2diff_method(fun, grid_x, solve_y, diff1_solve_y)[1]
    runge_err = [np.abs((solve_prev[-1] - solve[-1]) / 3)]
    nodes = [len(grid_x)]
    _h = [h]
    while runge_err[-1] > eps:
        solve_prev = solve
        h /= 2
        grid_x = np.arange(a, b, h)
        solve = euler_modified_2diff_method(fun, grid_x, solve_y, diff1_solve_y)[1]
        runge_err.append(np.abs(solve_prev[-1] - solve[-1]) / 3)
        print(f"Runge err euler method (eps {eps}): {np.abs(runge_err[-1])}")
        nodes.append(len(grid_x))
        _h.append(h)

    return grid_x, solve, nodes, _h, runge_err


def bisection(diffur, a, b, solve_borders_a, solve_borders_b, solve_y_a, solve_y_b, eps=1e-6):
    ans = []
    diff1_solve_y_1 = solve_borders_a
    # solve_1 = euler_modified_2diff_method_to_eps(diffur, a, b, solve_y_a, diff1_solve_y_1, eps=0.1)[1]
    diff1_solve_y_2 = solve_borders_b
    # solve_2 = euler_modified_2diff_method_to_eps(diffur, a, b, solve_y_a, diff1_solve_y_2, eps=0.1)[1]

    diff1_solve_y_middle = (diff1_solve_y_2 + diff1_solve_y_1) / 2
    print(f"\t y' = {diff1_solve_y_middle}")
    solve = euler_modified_2diff_method_to_eps(diffur, a, b, solve_y_a, diff1_solve_y_middle, eps=eps)
    ans.append((diff1_solve_y_middle, solve))
    while np.abs(solve_y_b - solve[1][-1]) > eps:
        if solve_y_b < solve[1][-1]:
            diff1_solve_y_2 = diff1_solve_y_middle
            diff1_solve_y_middle = (diff1_solve_y_middle + diff1_solve_y_1) / 2
        else:
            diff1_solve_y_1 = diff1_solve_y_middle
            diff1_solve_y_middle = (diff1_solve_y_middle + diff1_solve_y_2) / 2
        print(f"\t y' = {diff1_solve_y_middle}")
        solve = euler_modified_2diff_method_to_eps(diffur, a, b, solve_y_a, diff1_solve_y_middle, eps=eps)
        ans.append((diff1_solve_y_middle, solve))
    return ans


# def euler_modified_2diff_method(fun, grid_x, solve_y, diff1_solve_y):
#     grid_y = [solve_y]
#     grid_y_first_diff = [diff1_solve_y]
#     step = np.abs(grid_x[1] - grid_x[0])
#
#     for i in range(len(grid_x) - 1):
#         grid_y.append(grid_y[i] + step * grid_y_first_diff[i])
#         grid_y_first_diff.append(grid_y_first_diff[i] + step * fun(grid_x[i] + step/2, grid_y[i] + step/2*fun(grid_x[i], grid_y[i], grid_y_first_diff[i]), grid_y_first_diff[i] + step/2))
#
#     return grid_x, np.array(grid_y)
# #
# import matplotlib.pyplot as plt
#
# fig, ax = plt.subplots()
#
# plt.title("Error to nodes amount")
# h = 0.00001
# grid = np.arange(0.2, 3, h)
# grid_h2 = np.arange(0.2, 3, h / 2)
#
# fun = lambda x: 1 + 1 / x

# import numpy as np
# diffur = lambda x, y, y1: (1 / x ** 2 + 2 * y + y1) / (x ** 2 * (x + 1))
# from pickle import dump
# with open("bisection_result_dump_1_to_6", "wb") as file:
#     res = []
#     for i in range(1,7):
#         res.append(bisection(diffur, *(1, 3), *(-10, 0), 2, 1 + 1/3, eps=10**-i))
#     dump(res, file)
# with open("bisection_result_2_dump", "wb") as file:
#     dump(bisection(diffur, *(3, 5), *(-10, 0), 2, 1 + 1/3, eps=1e-7), file)

# grid = np.arange(1, 2.5, 0.5)
# print(grid)
# print(euler_modified_2diff_method(diffur, grid, 2, -0.5)[1])

# y = fun(grid)
# ax.plot(grid, fun(grid), label="true")
# solve = euler_modified_2diff_method(diffur, grid, 6, -1/0.04)
# ax.plot(*solve, label="modified")
# solve = euler_2diff_method(diffur, grid, 6, -1/0.04)
# ax.plot(*solve, label="usual")
# #
# # solve = euler_modified_2diff_method(diffur, grid, 6, -1/0.04)
# # ax.plot(grid, abs(solve[1] - y), label="mod")
# # solve = euler_2diff_method(diffur, grid, 6, -1/0.04)
# # ax.plot(grid, abs(solve[1] - y), label="usual")
# #
# #
# # runge = lambda rage, h, method, method_params: np.abs(method(**method_params, grid_x=np.arange(*rage, h))[1][-1] -
# #                                                       method(**method_params, grid_x=np.arange(*rage, h / 2))[1][-1])\
# #                                                / 3
# #
# # # h = [1]
# # # [h.append(_h / 10) for i, _h in zip(range(5), h)]
# # # rung = []
# # # for _h in h:
# # #     rung.append(runge((0.2, 3), _h, euler_modified_2diff_method, {"fun": diffur, "solve_y": 6, "diff1_solve_y": -25}))
# # # ax.plot(h, rung, label="runge usual")
# #
# # ax.set_yscale('log')
# # ax.set_xscale('log')
# ax.legend()
# ax.grid()
# # # plt.xlabel('h')
# # # plt.ylabel('err')
# #
# fig.set_size_inches(9, 5.5)
# # plt.show()
