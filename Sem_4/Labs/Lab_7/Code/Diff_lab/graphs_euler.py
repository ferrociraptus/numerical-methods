import numpy as np
import matplotlib.pyplot as plt
from Euler_modified_diff_method import euler_modified_2diff_method, euler_modified_2diff_method_to_eps
from collections.abc import Iterable

images_dir = "images/"
# images_dir = "./"

# fun declaration
fun = lambda x: 1 + 1 / x
diffur = lambda x, y, y1: (1 / x ** 2 + 2 * y + y1) / (x ** 2 * (x + 1))
fun_range = [1, 3]
fun_range_2 = [3, 5]

# grid_x, solve, nodes, h, runge_err = euler_modified_2diff_method_to_eps(diffur, *fun_range, 2, -1, eps=2e-9)
# grid_x_2, solve_2, nodes_2, h_2, runge_err_2 = euler_modified_2diff_method_to_eps(diffur, *fun_range_2, 1+1/3, -1/9, eps=2e-9)

import pickle as p

with open("range_1_results_dump", "rb") as file:
    grid_x, solve, nodes, h, runge_err = p.load(file)
with open("range_2_results_dump", "rb") as file:
    grid_x_2, solve_2, nodes_2, h_2, runge_err_2 = p.load(file)


def stdout_run_status_log(fun):
    def decorator(*args, **kwargs):
        print(f"Function: {fun.__name__} started")
        res = fun(*args, **kwargs)
        print(f"Function: {fun.__name__} finished")
        return res

    return decorator


@stdout_run_status_log
def absolute_error_modified_euler(func=fun, diff=diffur, func_range=fun_range):
    fig, ax = plt.subplots()

    plt.title("Абсолютная ошибка")

    # ax.plot(grid, fun(grid), label="true")
    # solve = euler_modified_2diff_method(diffur, grid, 6, -25)
    # grid, solve = euler_modified_2diff_method_to_eps(diff, *func_range, 6, -25, eps=1e-8)[0:2]

    ax.plot(grid_x, abs(solve - fun(grid_x)), label=r"Модифицированный метод Эйлера $\epsilon = 1^{-9}$")
    ax.plot(grid_x_2, abs(solve_2 - fun(grid_x_2)), label=r"Модифицированный метод Эйлера $\epsilon = 1^{-9}$")
    # solve = euler_2diff_method(diffur, grid, 6, -1 / 0.04)
    # ax.plot(grid, abs(solve[1] - y), label="usual")

    ax.set_yscale('log')
    # ax.set_xscale('log')
    ax.legend()
    ax.grid()
    plt.xlabel('X')
    plt.ylabel("абсолютная ошибка")

    fig.set_size_inches(9, 5.5)
    fig.savefig(images_dir + f'absolute_error_modified_euler_{"_".join(map(str, func_range))}.png')


@stdout_run_status_log
def runge_error_to_number_of_nodes_modified_euler(func=fun, diff=diffur, func_range=fun_range):
    fig, ax = plt.subplots()

    plt.title("Ошибка Рунге")

    # grid_x, solve, nodes, h, runge_err = euler_modified_2diff_method_to_eps(diff, *func_range, 6, -25, eps=1e-8)
    ax.plot(h, runge_err, label=f"Ошибка Рунге {func_range}")
    ax.plot(h_2, runge_err_2, label=f"Ошибка Рунге {fun_range_2}")

    ax.set_yscale('log')
    ax.set_xscale('log')

    ax.legend()
    ax.grid()

    plt.xlabel('h')
    plt.ylabel('ошибка')

    fig.set_size_inches(9, 5.5)
    fig.savefig(images_dir + f'runge_error_to_number_of_nodes_modified_euler_{"_".join(map(str, func_range))}.png')


@stdout_run_status_log
def number_of_nodes_of_defined_eps_modified_euler(func=fun, diff=diffur, func_range=fun_range):
    fig, ax = plt.subplots()

    plt.title("Отношение количества узлов к итоговой ошибке")

    # grid_x, solve, nodes, h, runge_err = euler_modified_2diff_method_to_eps(diff, *func_range, 6, -25, eps=1e-8)
    # ax.plot(grid, y, label="true", linewidth=3)
    # ax.plot(*solve, label="Euler")
    ax.plot(nodes, runge_err, label=f"Ошибка Рунге {func_range}")
    ax.plot(nodes_2, runge_err_2, label=f"Ошибка Рунге {fun_range_2}")
    # ax.plot(grid, solve[1], label="usual")

    ax.set_yscale('log')
    ax.set_xscale('log')
    ax.legend()
    ax.grid()
    plt.xlabel('количество узлов')
    plt.ylabel('Ошибка Рунге ')

    fig.set_size_inches(9, 5.5)
    fig.savefig(images_dir + f'number_of_nodes_of_defined_eps_modified_euler_{"_".join(map(str, func_range))}.png')


@stdout_run_status_log
def compare_graph_solve_and_through(func=fun, diff=diffur, func_range=fun_range):
    fig, ax = plt.subplots()

    x = list(grid_x.copy())
    x.extend(list(grid_x_2))
    x = np.array(x)[::1000]
    print(len(x))
    ax.plot(x, fun(x), label=r"$y = 1 + \frac{1}{x}$", linewidth=7, color="black")
    ax.plot(grid_x[::1000], solve[::1000], label=r"Модифицированный метод Эйлера $\epsilon = 1^{-9}$", linewidth=2, color="yellow")
    ax.plot(grid_x_2[::1000], solve_2[::1000], label=r"Модифицированный метод Эйлера $\epsilon = 1^{-9}$", linewidth=2, color="orange")
    # solve = euler_2diff_method(diffur, grid, 6, -1 / 0.04)
    # ax.plot(*solve, label="usual")

    plt.legend()
    plt.grid()

    # ax.set_yscale('log')
    # ax.set_xscale('log')

    plt.title(r"Сравнение результатов вычислений метода и точного решения")
    plt.xlabel(r'X')
    plt.ylabel(r'Y')

    fig.set_size_inches(9, 5.5)
    fig.savefig(images_dir + f'compare_graph_solve_and_through_modified_euler_{"_".join(map(str, func_range))}.png')


@stdout_run_status_log
def through_graph(func=fun, diff=diffur, func_range=fun_range):
    fig, ax = plt.subplots()

    x = grid_x[::1000]
    ax.plot(x, fun(x), label=r"$y = 1 + \frac{1}{x}$   [1, 3]", color="blue")
    x = grid_x_2[::1000]
    ax.plot(x, fun(x), label=r"$y = 1 + \frac{1}{x}$   [3, 5]", color="green")
    plt.legend()
    plt.grid()

    # ax.set_yscale('log')
    # ax.set_xscale('log')

    plt.title(r"График функции $y = 1 + \frac{1}{x}$")
    plt.xlabel(r'x')
    plt.ylabel(r'y')

    fig.set_size_inches(9, 5.5)
    fig.savefig(images_dir + f'solve_graph_{"_".join(map(str, func_range))}_euler.png')


absolute_error_modified_euler()
runge_error_to_number_of_nodes_modified_euler()
number_of_nodes_of_defined_eps_modified_euler()
compare_graph_solve_and_through()
through_graph()
plt.show()
