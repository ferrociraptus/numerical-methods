\selectlanguage *{russian}
\selectlanguage *{russian}
\contentsline {section}{\numberline {1}Формулировка задачи}{2}{}%
\contentsline {section}{\numberline {2}Формализация задачи}{2}{}%
\contentsline {section}{\numberline {3}Алгоритм и условия применимости}{3}{}%
\contentsline {subsection}{\numberline {3.1}Алгоритм модифицированного метода Эйлера}{3}{}%
\contentsline {subsubsection}{\numberline {3.1.1}Условия применимости}{7}{}%
\contentsline {subsection}{\numberline {3.2}Метод пристрелки}{8}{}%
\contentsline {section}{\numberline {4}Проверка условий применимости}{8}{}%
\contentsline {section}{\numberline {5}Тестовый пример}{8}{}%
\contentsline {section}{\numberline {6}Предварительный анализ задачи}{10}{}%
\contentsline {section}{\numberline {7}Численный анализ}{12}{}%
\contentsline {section}{\numberline {8}Вывод}{21}{}%
