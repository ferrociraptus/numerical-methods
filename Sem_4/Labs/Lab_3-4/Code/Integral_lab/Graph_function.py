import numpy as np
import matplotlib.pyplot as plt
from integration3_8 import integration3_8
from collections.abc import Iterable

images_dir = "images/"
# images_dir = "./"

# fun declaration
fun = lambda x: np.array([np.exp(- x_i ** 2) for x_i in (x if isinstance(x, Iterable) else [x])])
fun_range = (-3, 9)


def function_graph(func=fun, func_range=fun_range):
    fig, ax = plt.subplots()

    x = np.linspace(*(-3,3), num=1000)

    plt.title(r"Function $\epsilon^{-x^2}$")

    ax.plot(x, fun(x), label=f"Function (-3, 3)")
    x = np.linspace(*(3, 9), num=1000)
    ax.plot(x, fun(x), label=f"Function (3, 9)")

    ax.set_yscale("log")
    # ax.set_xscale("log")

    ax.grid()
    ax.legend(loc="upper right")
    plt.xlabel("X")
    plt.ylabel("Y")

    fig.set_size_inches(9, 5.5)
    fig.savefig(f"images/function_graph.png")


function_graph()
plt.show()
