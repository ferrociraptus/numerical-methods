from typing import Iterable

import numpy as np
from sympy.solvers import solve
from sympy import Symbol, diff
from threading import Thread
import queue


def lobatto(f, a, b, eps=1e-10):
    x = Symbol('x', real=True)
    a_arg = Symbol('a', real=True)
    b_arg = Symbol('b', real=True)
    _I = ((b_arg ** 3 - a_arg ** 3) / 3 * diff((x - a_arg) ** 4 * (b_arg - x) ** 4, x, 3)) / ((x - a_arg) * (x - b_arg))

    def lobatto_n4_kernel(a, b):
        n = 4

        # find roots
        # print(I)
        I_copy = _I.subs(a_arg, a).subs(b_arg, b)
        X = sorted(list(map(float, solve(I_copy))) + [a,b])
        # print(X)

        coefs = np.array([list(map(lambda x: x ** i, X)) for i in range(len(X))])
        b = np.array([(b ** (i + 1) - a ** (i + 1)) / (i + 1) for i in range(len(X))])
        A = np.linalg.solve(coefs, b)

        integral = sum(a * f(x) for a, x in zip(A, X))
        return integral

    # def integration_body(n):
    #     lines = np.linspace(a, b, num=n)
    #     threads_amount = 1
    #     threads_queue = queue.Queue()
    #     threads = []
    #     ans = []
    #     lines = [(start, end) for start, end in zip(lines, lines[1:])]
    #
    #     def run_fun():
    #         while True:
    #             line = threads_queue.get()
    #             if line is None:
    #                 return
    #             ans.append(lobatto_n4_kernel(*line))
    #             threads_queue.task_done()
    #
    #     for thread_num in range(threads_amount):
    #         threads.append(Thread(target=run_fun))
    #         threads[-1].start()
    #
    #     for line in lines:
    #         threads_queue.put(line)
    #     threads_queue.join()
    #
    #     return sum(ans)
    #     # return sum(lobatto_n4_kernel(start, end) for start, end in zip(lines, lines[1:]))
    #
    def integration_body(n):
        lines = np.linspace(a, b, num=n)
        ans = []
        lines = [(start, end) for start, end in zip(lines, lines[1:])]

        for line in lines:
            ans.append(lobatto_n4_kernel(*line))

        return sum(ans)

    incrementer = lambda i: i + (i - 1)
    n = 2
    number_of_nodes_arr = []
    I_prev = integration_body(n)
    n = incrementer(n)
    number_of_nodes_arr.append(n)
    I = integration_body(n)
    err = []
    err.append(abs(I - I_prev) / (2 ** 4 - 1))
    while (abs(I - I_prev) / (2 ** 4 - 1)) > eps:
        I_prev = I
        n = incrementer(n)
        I = integration_body(n)
        err.append(abs(I - I_prev) / (2 ** 4 - 1))
        number_of_nodes_arr.append(n)
        print(f"n:{n} Lobato err: {(abs(I - I_prev) / (2 ** 4 - 1))}")

    return I, err, number_of_nodes_arr


# fun = lambda x: np.exp(-x ** 2)
# print(lobatto(fun, 0, 1, 1e-14))
