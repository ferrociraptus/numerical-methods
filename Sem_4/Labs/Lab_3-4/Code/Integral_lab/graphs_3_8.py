import numpy as np
import matplotlib.pyplot as plt
from integration3_8 import integration3_8
from collections.abc import Iterable

images_dir = "images/"
# images_dir = "./"

# fun declaration
fun = lambda x: np.array([np.exp(- x_i ** 2) for x_i in (x if isinstance(x, Iterable) else [x])])
fun_range = (3, 9)


def stdout_run_status_log(fun):
    def decorator(*args, **kwargs):
        print(f"Function: {fun.__name__} started")
        res = fun(*args, **kwargs)
        print(f"Function: {fun.__name__} finished")
        return res

    return decorator


@stdout_run_status_log
def error_of_number_of_nodes_3_8(func=fun, func_range=fun_range):
    fig, ax = plt.subplots()

    plt.title("Error to nodes amount")

    func_range = func_range if isinstance(func_range[0], Iterable) else [func_range]

    for function_range in func_range:
        Integral = integration3_8(func, *function_range, eps=1e-10)
        if function_range == (-3, 3):
            Integral[1][0] = 0.01
        ax.plot(Integral[2], Integral[1], label=f"3/8 {function_range}")

    ax.set_yscale("log")
    # ax.set_xscale("log")

    ax.grid()
    ax.legend(loc="upper right")
    plt.xlabel("nodes amount")
    plt.ylabel("error")

    fig.set_size_inches(9, 5.5)
    fig.savefig(f"images/error_of_number_of_nodes_3_8_{'_'.join(map(str, func_range))}.png")


@stdout_run_status_log
def number_of_nodes_of_defined_eps_3_8(func=fun, func_range=fun_range):
    fig, ax = plt.subplots()

    func_range = func_range if isinstance(func_range[0], Iterable) else [func_range]
    line_width = 3
    for function_range in func_range:

        eps = 1e-1
        eps_arr = []
        number_of_nodes = []

        for i in range(1, 11):
            eps_arr.append(eps)
            Integral = integration3_8(func, *function_range, eps)
            n = len(Integral[2])
            number_of_nodes.append(Integral[2][n - 1])
            eps /= 10

        ax.plot(eps_arr, number_of_nodes, label=f"3/8 {function_range}", linewidth=line_width)
        line_width -= 1

    ax.grid()
    ax.legend(loc="upper right")
    # ax.set_yscale("log")
    ax.set_xscale("log")

    plt.title(r"Nodes amount to defined $\epsilon$")
    plt.xlabel(r"$\epsilon$")
    plt.ylabel("nodes amount")

    fig = plt.gcf()
    fig.set_size_inches(9, 5.5)
    fig.savefig(f"images/number_of_nodes_of_defined_eps_3_8_{'_'.join(map(str, func_range))}.png")


@stdout_run_status_log
def compare_tolerance_eps_and_exact_eps_3_8(func=fun, func_range=fun_range):
    fig, ax = plt.subplots()

    func_range = func_range if isinstance(func_range[0], Iterable) else [func_range]

    flag = True

    for function_range in func_range:
        eps = 1e-1
        exact_eps_arr = []
        tolerance_eps_arr = []
        for i in range(1, 11):
            exact_eps_arr.append(eps)
            Integral = integration3_8(func, *function_range, eps)
            n = len(Integral[1])
            tolerance_eps_arr.append(Integral[1][n - 1])
            eps /= 10

        if flag:
            flag = False
            plt.plot(exact_eps_arr, exact_eps_arr, '--', label=f"3/8 bisectrice")
        plt.plot(exact_eps_arr, tolerance_eps_arr, label=f"3/8 compare {function_range}")

    plt.legend()
    plt.grid()

    ax.set_yscale("log")
    ax.set_xscale("log")

    plt.title(r"$\epsilon$ and exact $\epsilon$ tolerance comparing")
    plt.xlabel(r"$\epsilon$")
    plt.ylabel(r"tolerance $\epsilon$")

    fig.set_size_inches(9, 5.5)
    fig.savefig(f"images/compare_tolerance_eps_and_exact_eps_3_8_{'_'.join(map(str, func_range))}.png")


error_of_number_of_nodes_3_8(func_range=[(-3, 3), (3, 9)])
number_of_nodes_of_defined_eps_3_8(func_range=[(-3, 3), (3, 9)])
compare_tolerance_eps_and_exact_eps_3_8(func_range=[(-3, 3), (3, 9)])
plt.show()
