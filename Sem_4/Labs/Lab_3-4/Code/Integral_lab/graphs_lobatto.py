import numpy as np
import matplotlib.pyplot as plt
from lobatto import lobatto
from collections.abc import Iterable

images_dir = "images/"
# images_dir = "./"

# fun declaration
fun = lambda x: np.array([np.exp(- x_i ** 2) for x_i in (x if isinstance(x, Iterable) else [x])], dtype=np.float64)
fun_range = (3, 9)


def stdout_run_status_log(fun):
    def decorator(*args, **kwargs):
        print(f"Function: {fun.__name__} started")
        res = fun(*args, **kwargs)
        print(f"Function: {fun.__name__} finished")
        return res

    return decorator


@stdout_run_status_log
def error_of_number_of_nodes_lobatto(func=fun, func_range=fun_range):
    fig, ax = plt.subplots()

    func_range = func_range if isinstance(func_range[0], Iterable) else [func_range]

    for function_range in func_range:
        plt.title("Error to nodes amount")
        Integral = lobatto(func, *function_range, eps=1e-10)

        ax.plot(Integral[2], Integral[1], label=f"lobatto {function_range}")

    ax.set_yscale('log')
    # ax.set_xscale('log')
    ax.legend()
    ax.grid()
    plt.xlabel('nodes amount')
    plt.ylabel('error')

    fig.set_size_inches(9, 5.5)
    fig.savefig(f"images/error_of_number_of_nodes_lobatto_{'_'.join(map(str, func_range))}.png")


@stdout_run_status_log
def number_of_nodes_of_defined_eps_lobatto(func=fun, func_range=fun_range):
    fig, ax = plt.subplots()

    func_range = func_range if isinstance(func_range[0], Iterable) else [func_range]

    for function_range in func_range:
        eps = 1e-1
        eps_arr = []
        number_of_nodes = []
        for i in range(1, 11):
            eps_arr.append(eps)
            Integral = lobatto(func, *function_range, eps)
            n = len(Integral[2])
            number_of_nodes.append(Integral[2][n - 1])
            eps /= 10

        ax.plot(eps_arr, number_of_nodes, label=f"lobatto {function_range}")

    ax.grid()

    # ax.set_yscale('log')
    ax.set_xscale('log')

    ax.legend()

    plt.title(r"Nodes amount to defined $\epsilon$")
    plt.xlabel(r'$\epsilon$')
    plt.ylabel('nodes amount')

    fig = plt.gcf()
    fig.set_size_inches(9, 5.5)
    fig.savefig(f"images/number_of_nodes_of_defined_eps_lobatto_{'_'.join(map(str, func_range))}.png")


@stdout_run_status_log
def compare_tolerance_eps_and_exact_eps_lobatto(func=fun, func_range=fun_range):
    fig, ax = plt.subplots()

    func_range = func_range if isinstance(func_range[0], Iterable) else [func_range]

    flag = True
    for function_range in func_range:
        eps = 1e-1
        exact_eps_arr = []
        tolerance_eps_arr = []
        for i in range(1, 11):
            exact_eps_arr.append(eps)
            Integral = lobatto(func, *function_range, eps)
            n = len(Integral[1])
            tolerance_eps_arr.append(Integral[1][n - 1])
            eps /= 10

        if flag:
            flag = False
            plt.plot(exact_eps_arr, exact_eps_arr, '--', label=f'bisectrice')
        plt.plot(exact_eps_arr, tolerance_eps_arr, label=f'compare {function_range}')

    plt.legend()
    plt.grid()

    ax.set_yscale('log')
    ax.set_xscale('log')

    plt.title(r"$\epsilon$ and exact $\epsilon$ tolerance comparing")
    plt.xlabel(r'$\epsilon$')
    plt.ylabel(r'tolerance $\epsilon$')

    fig.set_size_inches(9, 5.5)
    fig.savefig(f"images/compare_tolerance_eps_and_exact_eps_lobatto_{'_'.join(map(str, func_range))}.png")


error_of_number_of_nodes_lobatto(func_range=[(-3, 3), (3, 9)])
number_of_nodes_of_defined_eps_lobatto(func_range=[(-3, 3), (3, 9)])
compare_tolerance_eps_and_exact_eps_lobatto(func_range=[(-3, 3), (3, 9)])
# order_of_convergence_lobatto()
plt.show()
