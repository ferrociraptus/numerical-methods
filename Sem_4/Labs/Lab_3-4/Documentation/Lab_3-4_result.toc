\selectlanguage *{russian}
\selectlanguage *{russian}
\contentsline {section}{\numberline {1}Формулировка задачи}{2}{}%
\contentsline {section}{\numberline {2}Алгоритм метода и условие его применимости}{2}{}%
\contentsline {subsection}{\numberline {2.1}Алгоритм метода 3/8}{2}{}%
\contentsline {subsubsection}{\numberline {2.1.1}Условия применимости метода 3/8}{4}{}%
\contentsline {subsubsection}{\numberline {2.1.2}Проверка условий применимости}{4}{}%
\contentsline {subsection}{\numberline {2.2}Алгоритм метода Лобатто}{4}{}%
\contentsline {subsubsection}{\numberline {2.2.1}Условия применимости метода 3/8}{5}{}%
\contentsline {subsubsection}{\numberline {2.2.2}Проверка условий применимости}{5}{}%
\contentsline {section}{\numberline {3}Тестовый пример}{6}{}%
\contentsline {subsection}{\numberline {3.1}Метод 3/8}{6}{}%
\contentsline {subsection}{\numberline {3.2}Метод Лобатто}{6}{}%
\contentsline {section}{\numberline {4}Предварительный анализ задачи}{7}{}%
\contentsline {section}{\numberline {5}Вычисляющая программа}{8}{}%
\contentsline {section}{\numberline {6}Численный анализ}{9}{}%
\contentsline {subsection}{\numberline {6.1}Метод 3/8}{9}{}%
\contentsline {subsection}{\numberline {6.2}Метод Лобатто}{12}{}%
\contentsline {subsection}{\numberline {6.3}Сравнение методов}{15}{}%
\contentsline {section}{\numberline {7}Вывод}{16}{}%
