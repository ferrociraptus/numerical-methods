from math import cos, pi, prod
import numpy as np


def chebyshev_points(section, n):
    return np.array([0.5 * (section[0] + section[1])
                     + 0.5 * (section[1] - section[0]) * cos((2 * k - 1) * pi / (2 * n))
                     for k in range(n, 0, -1)])


def lagrange_interpolation(points_table):
    return lambda x: sum([prod([x - x_i for index, x_i in enumerate(points_table[0]) if index != i])
                          / prod([points_table[0][i] - x_i for index, x_i in enumerate(points_table[0]) if index != i])
                          * points_table[1][i]
                          for i in range(len(points_table[0]))])
