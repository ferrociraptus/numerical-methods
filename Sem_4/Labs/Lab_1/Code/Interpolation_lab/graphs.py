from matplotlib import pyplot as plt
import numpy as np
from Lagrange_interpolation import chebyshev_points, lagrange_interpolation
from decimal import Decimal
from collections.abc import Iterable

images_dir = "images/"

# fun declaration
fun = lambda x: np.array([np.exp(- x_i ** 2) for x_i in (x if isinstance(x, Iterable) else [x])])
fun_range = (-3, 3)

error_calculation = lambda f, f_l, p: max([(lambda point: abs(f(point) - f_l(point)))((x_next + x) / 2)
                                           for x, x_next in zip(p, p[1:])])


def lagrange_graph(n, func_range=fun_range):
    fig, ax = plt.subplots()

    ax.grid(True)
    # ax.set_xscale('log')
    # ax.set_yscale('log')

    # error_calculation = lambda f, f_l, p: max([(lambda p: (abs(f(p) - f_l(p)), p))((x_next + x) / 2)
    #                                            for x, x_next in zip(p, p[1:])])

    x = np.linspace(*func_range, num=500).astype(np.dtype(Decimal))
    y = fun(x)

    x_line = np.linspace(*func_range, num=n).astype(np.dtype(Decimal))
    y_line = fun(x_line)

    x_chebishka = chebyshev_points(func_range, n).astype(np.dtype(Decimal))
    y_chebishka = fun(x_chebishka)

    lagrange_chebyshev = lagrange_interpolation((x_chebishka, y_chebishka))
    lagrange_uniform = lagrange_interpolation((x_line, y_line))

    # chebyshev_error = error_calculation(fun, lagrange_chebyshev, x_chebishka)
    # uniform_error = error_calculation(fun, lagrange_uniform, x_line)
    #
    # err_x = chebyshev_error[1]
    # ax.plot((err_x, err_x), (lagrange_chebyshev(err_x), fun(err_x)), linewidth=2)
    # err_x = uniform_error[1]
    # ax.plot((err_x, err_x), (lagrange_uniform(err_x), fun(err_x)), linewidth=2)

    ax.plot(x, y, '--', linewidth=3, label="Native plot")
    ax.scatter(x_chebishka, y_chebishka, marker='d',
               color=(0, 1, 0), label="Chebyshev points", linewidths=2, s=50)
    ax.scatter(x_line, y_line, marker='d',
               color=(1, 0, 0), label="Uniform distribution points", linewidths=2, s=50)

    ax.plot(x, lagrange_chebyshev(x), linewidth=2, label="Lagrange (Chebyshev points)")
    ax.plot(x, lagrange_uniform(x), linewidth=2, label="Lagrange (uniform distribution points)")

    plt.xlabel(r"X")
    plt.ylabel("Y")
    plt.title(f"Interpolation ({n} points)")

    ax.legend(loc="upper right")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig(images_dir + f'lagrange_graph_{n}_points_{fun_range}_range.png', dpi=100)


def lagrange_graph_uniform_only(n, func_range=fun_range):
    fig, ax = plt.subplots()

    ax.grid(True)

    x = np.linspace(*func_range, num=500).astype(np.dtype(Decimal))
    y = fun(x)

    x_line = np.linspace(*func_range, num=n).astype(np.dtype(Decimal))
    y_line = fun(x_line)

    lagrange_uniform = lagrange_interpolation((x_line, y_line))

    ax.plot(x, y, '--', linewidth=3, label="Native plot")
    ax.scatter(x_line, y_line, marker='d',
               color=(1, 0, 0), label="Uniform distribution points", linewidths=2, s=50)
    ax.plot(x, lagrange_uniform(x), linewidth=2, label="Lagrange (uniform distribution points)")

    plt.xlabel(r"X")
    plt.ylabel("Y")
    plt.title(f"Uniform grid interpolation ({n} points)")

    ax.legend(loc="upper right")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig(images_dir + f'lagrange_graph_uniform_{n}_points_{func_range}_range.png', dpi=100)


def points_amount_to_error_chebyshev():
    fig, ax = plt.subplots()

    ax.grid(True)
    ax.set_yscale('log')

    errors = []

    n_values = list(range(2, 101))
    for n in n_values:
        x_chebyshka = chebyshev_points(fun_range, n).astype(np.dtype(Decimal))
        y_chebyshka = fun(x_chebyshka)

        lagrange = lagrange_interpolation((x_chebyshka, y_chebyshka))
        errors.append(error_calculation(fun, lagrange, x_chebyshka))

    ax.plot(n_values, errors, linewidth=2, label="Lagrange interpolation error (Chebyshev nodes)")

    plt.xlabel(r"Nodes amount")
    plt.ylabel("Error")
    plt.title("Points amount to error (Chebyshev interpolation grid)")

    ax.legend(loc="upper right")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig(images_dir + f'points_amount_to_error_chebyshev_{fun_range}_range.png', dpi=100)


def points_amount_to_error_uniform():
    fig, ax = plt.subplots()

    ax.grid(True)
    ax.set_yscale('log')

    errors = []

    n_values = list(range(2, 101))
    for n in n_values:
        x_line = np.linspace(*fun_range, num=n).astype(np.dtype(Decimal))
        y_line = fun(x_line)

        lagrange = lagrange_interpolation((x_line, y_line))
        errors.append(error_calculation(fun, lagrange, x_line))

    ax.plot(n_values, errors, linewidth=2, label="Lagrange interpolation error (uniform distribution points)")

    plt.xlabel(r"Nodes amount")
    plt.ylabel("Error")
    plt.title("Points amount to error (uniform interpolation grid)")

    ax.legend(loc="upper right")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig(images_dir + f'points_amount_to_error_uniform_{fun_range}_range.png', dpi=100)


def points_amount_to_error_all():
    fig, ax = plt.subplots()

    ax.grid(True)
    ax.set_yscale('log')

    errors_uniform = []
    errors_chebyshev = []

    n_values = list(range(2, 101))
    for n in n_values:
        x_line = np.linspace(*fun_range, num=n).astype(np.dtype(Decimal))
        y_line = fun(x_line)

        x_chebyshev = chebyshev_points(fun_range, n).astype(np.dtype(Decimal))
        y_chebyshev = fun(x_chebyshev)

        lagrange = lagrange_interpolation((x_line, y_line))
        lagrange_chebyshev = lagrange_interpolation((x_chebyshev, y_chebyshev))
        errors_uniform.append(error_calculation(fun, lagrange, x_line))
        errors_chebyshev.append(error_calculation(fun, lagrange_chebyshev, x_chebyshev))

    ax.plot(n_values, errors_uniform, linewidth=2, label="Lagrange interpolation error (uniform distribution)")
    ax.plot(n_values, errors_chebyshev, linewidth=2, label="Lagrange interpolation error (Chebyshev nodes)")

    plt.xlabel(r"Nodes amount")
    plt.ylabel("Error")
    plt.title("Points amount to error")

    ax.legend(loc="upper right")

    fig = plt.gcf()
    fig.set_size_inches(8, 5.5)
    fig.savefig(images_dir + f'points_amount_to_error_all_{fun_range}_range.png', dpi=100)


lagrange_graph_uniform_only(3, (0, 1))
lagrange_graph(5)
lagrange_graph(10)
lagrange_graph(15)
lagrange_graph(65)
points_amount_to_error_chebyshev()
points_amount_to_error_uniform()
points_amount_to_error_all()
plt.show()
