\selectlanguage *{russian}
\selectlanguage *{russian}
\contentsline {section}{\numberline {1}Формулировка задачи}{2}{}%
\contentsline {section}{\numberline {2}Алгоритм метода и условие его применимости}{2}{}%
\contentsline {subsection}{\numberline {2.1}Построение сеток}{2}{}%
\contentsline {subsubsection}{\numberline {2.1.1}Разбиение Чебышева}{2}{}%
\contentsline {subsubsection}{\numberline {2.1.2}Равномерное разбиение}{2}{}%
\contentsline {subsection}{\numberline {2.2}Алгоритм}{2}{}%
\contentsline {subsection}{\numberline {2.3}Условие применимости}{2}{}%
\contentsline {section}{\numberline {3}Предварительный анализ задачи}{3}{}%
\contentsline {section}{\numberline {4}Проверка условий применимости}{3}{}%
\contentsline {section}{\numberline {5}Тестовый пример}{3}{}%
\contentsline {section}{\numberline {6}Контрольные тесты}{4}{}%
\contentsline {section}{\numberline {7}Вычисляющая программа}{5}{}%
\contentsline {section}{\numberline {8}Численный анализа}{6}{}%
\contentsline {section}{\numberline {9}Вывод}{11}{}%
