from math import prod


def lagrange_interpolation(x_grid, y_grid):
    return lambda x: sum([prod([x - x_i for index, x_i in enumerate(x_grid) if index != i])
                          / prod([x_grid[i] - x_i for index, x_i in enumerate(x_grid) if index != i])
                          * y_grid[i]
                          for i in range(len(x_grid))])
