import numpy as np


def chebyshev_points(section, n):
    return np.array([0.5 * (section[0] + section[1])
                     + 0.5 * (section[1] - section[0]) * np.cos((2 * k - 1) * np.pi / (2 * n))
                     for k in range(n, 0, -1)])

