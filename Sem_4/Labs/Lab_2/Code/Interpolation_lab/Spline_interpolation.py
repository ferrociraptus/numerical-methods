from collections.abc import Iterable
from numpy import array
from decimal import Decimal

def spline_interpolation(x_grid, y_grid):
    x_grid = list([Decimal(val) for val in x_grid])
    y_grid = list([Decimal(val) for val in y_grid])

    h = lambda k: x_grid[k] - x_grid[k - 1]
    f = lambda i, j: (y_grid[j] - y_grid[i]) / h(j)

    n = len(x_grid) - 1
    c = [0 for i in range(n + 1)]
    d = [None]
    b = [None]
    deltas = [None, -h(2) / (2 * (h(1) + h(2)))]
    lambdas = [None, 3 * (f(1, 2) - f(0, 1)) / (2 * (h(1) + h(2)))]

    for k in range(3, n + 1):
        deltas.append(-h(k) / (2 * h(k - 1) + 2 * h(k) + h(k - 1) * deltas[k - 2]))
        lambdas.append((3 * f(k - 1, k) - 3 * f(k - 2, k - 1) - h(k - 1) * lambdas[k - 2])
                       / (2 * h(k - 1) + 2 * h(k) + h(k - 1) * deltas[k - 2]))

    for k in range(n, 1, -1):
        c[k - 1] = deltas[k - 1] * c[k] + lambdas[k - 1]

    a = y_grid
    for k in range(1, n + 1):
        d.append((c[k] - c[k - 1]) / (3 * h(k)))
        b.append(f(k - 1, k) + Decimal(str(2 / 3)) * h(k) * c[k] + Decimal(str(1 / 3)) * h(k) * c[k - 1])

    del deltas
    del lambdas

    def interpolation(x):
        if not isinstance(x, Iterable):
            x = [x]
        ans = []
        for val in x:
            ans_found_flag = False
            for k in range(1, n + 1):
                if x_grid[k - 1] <= val <= x_grid[k]:
                    diff = Decimal(str(val)) - x_grid[k]
                    ans.append(a[k] + b[k] * diff + c[k] * diff ** 2 + d[k] * diff ** 3)
                    ans_found_flag = True
                    break
            if not ans_found_flag:
                ans.append(None)
        return array(ans)

    return interpolation


# import numpy as np
# fun = lambda x: np.array([np.exp(- x_i ** 2) for x_i in (x if isinstance(x, Iterable) else [x])])
# x = np.array([0, 0.5, 1])
# y = fun(x)
# print(y)
# inter = spline_interpolation(x, y)
