from typing import Optional

from PIL import Image


def image_to_ascii_art(
    img_path: str, output_file: Optional[str] = "pywhatkit_asciiart"
) -> str:
    """Convert an Image to ASCII Art"""

    img = Image.open(img_path).convert("L")

    width, height = img.size
    aspect_ratio = height / width
    new_width = 80*2
    new_height = aspect_ratio * new_width * 0.55
    img = img.resize((new_width, int(new_height)))


    pixels = img.getdata()

    # chars = ["*", "S", "#", "&", "@", "$", "%", "*", "!", ":", "."]
    chars = ' .",:;!~+-xmo*#W&8@'
    new_pixels = [chars[pixel // 14] for pixel in pixels]
    # new_pixels = [chars[pixel // 25] for pixel in pixels]
    new_pixels = "".join(new_pixels)

    new_pixels_count = len(new_pixels)
    ascii_image = [
        new_pixels[index : index + new_width]
        for index in range(0, new_pixels_count, new_width)
    ]
    ascii_image = "\n".join(ascii_image)

    with open(f"{output_file}.txt", "w") as f:
        f.write(ascii_image)
    return ascii_image

def stub(func):
    def Чебурек(*args, **kwargs):
        img = image_to_ascii_art("Yakubovich.png")
        print(f"Хрен тебе, а не {func.__name__}!")
        print(img)
    return Чебурек


def bug(glob_obj):
    items = glob_obj.copy()
    new_decl = {name: stub(item) for name, item in items.items() if callable(item)}
    glob_obj.update(new_decl)
