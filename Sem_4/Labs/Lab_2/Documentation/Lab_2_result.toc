\selectlanguage *{russian}
\selectlanguage *{russian}
\contentsline {section}{\numberline {1}Формулировка задачи}{2}{}%
\contentsline {section}{\numberline {2}Алгоритм метода и условие его применимости}{2}{}%
\contentsline {subsection}{\numberline {2.1}Построение сеток}{2}{}%
\contentsline {subsubsection}{\numberline {2.1.1}Разбиение Чебышева}{2}{}%
\contentsline {subsubsection}{\numberline {2.1.2}Равномерное разбиение}{2}{}%
\contentsline {subsection}{\numberline {2.2}Алгоритм}{2}{}%
\contentsline {subsection}{\numberline {2.3}Условия применимости.}{4}{}%
\contentsline {subsection}{\numberline {2.4}Проверка условий применимости}{4}{}%
\contentsline {section}{\numberline {3}Предварительный анализ задачи}{4}{}%
\contentsline {section}{\numberline {4}Тестовый пример}{4}{}%
\contentsline {section}{\numberline {5}Контрольные тесты}{6}{}%
\contentsline {section}{\numberline {6}Численный анализ}{7}{}%
\contentsline {section}{\numberline {7}Вывод}{11}{}%
