\selectlanguage *{russian}
\selectlanguage *{russian}
\contentsline {section}{\numberline {1}Формулировка задачи}{2}{}%
\contentsline {section}{\numberline {2}Формализация задачи}{2}{}%
\contentsline {section}{\numberline {3}Алгоритм и условия применимости}{2}{}%
\contentsline {subsection}{\numberline {3.1}Итоговый алгоритм}{5}{}%
\contentsline {subsection}{\numberline {3.2}Условия применимости}{5}{}%
\contentsline {section}{\numberline {4}Проверка условий применимости}{5}{}%
\contentsline {section}{\numberline {5}Тестовый пример}{5}{}%
\contentsline {section}{\numberline {6}Предварительный анализ задачи}{6}{}%
\contentsline {section}{\numberline {7}Численный анализ}{8}{}%
\contentsline {section}{\numberline {8}Вывод}{12}{}%
